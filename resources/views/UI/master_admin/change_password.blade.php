@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">

                    <form>

                        {{--  <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="old_password" type="password">
                                <label for="name2">Old Password</label>
                            </div>
                        </div>  --}}

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="new_password" type="password">
                                <label for="name2">New Password</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="confirm_password" type="password">
                                <label for="name2">Confirm Password</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="button" id="UpdateMasterAdminPassword" name="action">Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection