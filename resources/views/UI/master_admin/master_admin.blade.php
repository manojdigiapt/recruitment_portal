@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title">Responsive Table</h5> -->
                    <a href="/add_master_admin" class="btn waves-effect waves-light pull-right" type="submit" name="action">Add Master Admin
                        <!-- <i class="material-icons right">send</i> -->
                    </a>
                    <table class="responsive-table">
                        <thead>
                            <tr>
                                <th>Master Admin Username</th>
                                {{--  <th>Password</th>  --}}
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($GetAdmin as $Admin)
                            @if($Admin->login_type != 1)
                                <tr>
                                    <td>{{$Admin->email}}</td>
                                    {{--  <td>{{$Admin->password}}</td>  --}}
                                    <td><div class="switch" style="
                                        width: 170px;
                                    ">
                                            <label>
                                                <span class="clr-green">Active</span>
                                                <input type="checkbox" onchange="ChangeMasterAdminStatus({{$Admin->id}})" @if($Admin->status == 0)
                                                    
                                                @else
                                                checked
                                                @endif>
                                                <span class="lever"></span>
                                                <span class="clr-red">In Active</span>
                                            </label>
                                        </div></td>

                                <td><a href="/edit_master_admin/{{$Admin->id}}" target="_blank">Edit</a></td>
                                </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection