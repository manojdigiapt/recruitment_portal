@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title activator">Form with placeholder<i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Get Code">more_vert</i></h5> -->
                    <form>
                        
                        <div class="row">
                            <div class="input-field col s12">
                            <input placeholder="John Doe" id="master_admin_name" value="{{$GetAdmins->email}}" type="text">
                            <input placeholder="John Doe" id="Id" value="{{$GetAdmins->id}}" type="hidden">
                            <label for="name2">Admin Username</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="Password" id="password" type="password">
                                <label for="name2">Password</label>
                            </div>
                        </div>  

                       
                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="button" id="UpdateMasterAdmin" name="action">Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection


