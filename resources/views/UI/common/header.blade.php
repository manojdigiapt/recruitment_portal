<header class="topbar">
    <!-- ============================================================== -->
    <!-- Navbar scss in header.scss -->
    <!-- ============================================================== -->
    <nav>
      <div class="nav-wrapper">
        <!-- ============================================================== -->
        <!-- Logo you can find that scss in header.scss -->
        <!-- ============================================================== -->
        <a href="javascript:void(0)" class="brand-logo logo-pad-left">
          <span class="icon">
            <img class="light-logo" src="{{URL::asset('UI/assets/images/logo_w.png')}}" alt="logos">
            <img class="dark-logo" src="{{URL::asset('UI/assets/images/logo_w.png')}}" alt="logos">
          </span>
          {{-- <span class="text">
            <img class="light-logo" src="{{URL::asset('UI/assets/images/logo_w.png')}}" alt="logos">
            <img class="dark-logo" src="{{URL::asset('UI/assets/images/logo_w.png')}}" alt="logos">
          </span> --}}
        </a>
        <!-- ============================================================== -->
        <!-- Logo you can find that scss in header.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left topbar icon scss in header.scss -->
        <!-- ============================================================== -->
        <ul class="left">
          <li class="hide-on-med-and-down">
            <a href="javascript: void(0);" class="nav-toggle">
              <span class="bars bar1"></span>
              <span class="bars bar2"></span>
              <span class="bars bar3"></span>
            </a>
          </li>
          <li class="hide-on-large-only">
            <a href="javascript: void(0);" class="sidebar-toggle">
              <span class="bars bar1"></span>
              <span class="bars bar2"></span>
              <span class="bars bar3"></span>
            </a>
          </li>
          {{-- <li class="search-box">
            <a href="javascript: void(0);"><i class="material-icons">search</i></a>
            <form class="app-search">
              <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
            </form>
          </li> --}}
        </ul>
        <!-- ============================================================== -->
        <!-- Left topbar icon scss in header.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right topbar icon scss in header.scss -->
        <!-- ============================================================== -->
        <ul class="right">
    <!-- ============================================================== -->
    <!-- Profile icon scss in header.scss -->
    <!-- ============================================================== -->
    {{-- <li><a class="dropdown-trigger" href="javascript: void(0);" data-target="user_dropdown"><img src="{{URL::asset('UI/assets/images/users/2.jpg')}}" alt="user" class="circle profile-pic"></a> --}}
      @if(Auth::guard('super_admin')->check())
        <li><a href="/SuperAdminlogout" title="Log Out">Log Out</a></li>
      @elseif(Auth::guard('master_admin')->check())
        <li><a href="/Master_admin_changepassword">Change Password</a></li>
        <li><a href="/Adminlogout" title="Log Out">Log Out</a></li>
      @elseif(Auth::guard('vendor')->check())
        <li><a href="/change_password">Change Password</a></li>
        <li><a href="/Vendorlogout" title="Log Out">Log Out</a></li>
      {{-- <li><a href="/Vendorlogout"><i class="material-icons">power_settings_new</i> Logout</a></li> --}}
      @elseif(Auth::guard('sub_admin')->check())
        <li><a href="/SubAdminlogout" title="Log Out">Log Out</a></li>
      @endif
     
    {{-- <ul id="user_dropdown" class="mailbox dropdown-content dropdown-user"> --}}
      {{-- <li>
        <div class="dw-user-box">
          <div class="u-img"><img src="{{URL::asset('UI/assets/images/users/2.jpg')}}" alt="user"></div>
          <div class="u-text">
            <h4>Steve Harvey</h4>
            <p><a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="d3a0a7b6a5b693b4beb2babffdb0bcbe">[email&#160;protected]</a></p>
            <a class="waves-effect waves-light btn-small red white-text">View Profile</a>
          </div>
        </div>
      </li>
      <li role="separator" class="divider"></li>
      <li><a href="#"><i class="material-icons">account_circle</i> My Profile</a></li>
      <li><a href="#"><i class="material-icons">account_balance_wallet</i> My Balance</a></li>
      <li><a href="#"><i class="material-icons">settings</i> Account Setting</a></li>
      <li role="separator" class="divider"></li> --}}
      {{-- @if(Auth::guard('admin')->check())
      <li><a href="/Adminlogout"><i class="material-icons">power_settings_new</i> Logout</a></li>
      @elseif(Auth::guard('vendor')->check())
      <li><a href="/change_password"><i class="material-icons">account_circle</i> Change Password</a></li>

      <li><a href="/Vendorlogout"><i class="material-icons">power_settings_new</i> Logout</a></li>
      @elseif(Auth::guard('sub_admin')->check())
      <li><a href="/SubAdminlogout"><i class="material-icons">power_settings_new</i> Logout</a></li>
      @endif --}}
    {{-- </ul> --}}
  </li>
</ul>
<!-- ============================================================== -->
<!-- Right topbar icon scss in header.scss -->
<!-- ============================================================== -->
</div>
</nav>
<!-- ============================================================== -->
<!-- Navbar scss in header.scss -->
<!-- ============================================================== -->
</header>