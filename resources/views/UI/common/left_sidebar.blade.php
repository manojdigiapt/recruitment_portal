<aside class="left-sidebar">
    <ul id="slide-out" class="sidenav">
      <li>
        <div class="user-profile" style="background-image: url({{URL::asset('UI/assets/images/user-bg.jpg')}});">
          {{-- <div class="user-name dropdown-trigger" data-target='dropdownuser'>
            <h6 class="white-text name"><i class="material-icons m-r-10">account_circle</i> <span class="hidden">Admin</span> <i class="material-icons ml-auto hidden">expand_more</i></h6>
          </div>
          <ul id='dropdownuser' class='dropdown-content'>
            <li><a href="#"><i class="material-icons">account_circle</i> My Profile</a></li>
            <li><a href="#"><i class="material-icons">account_balance_wallet</i> My Balance</a></li>
            <li><a href="#"><i class="material-icons">settings</i> Account Setting</a></li>
            <li role="separator" class="divider m-t-0"></li>
            <li><a href="#"><i class="material-icons">power_settings_new</i> Logout</a></li>
            
          </ul> --}}
        </div>
      </li>
      <li>
        @if(Auth::guard('super_admin')->check()) 
        <ul class="collapsible">
            <li class="small-cap"><span class="hide-menu">Admin</span></li>
            <li>
              <a href="/dashboard" class="collapsible-header"><i class="material-icons">dashboard</i><span class="hide-menu"> Dashboard</span></a>
            </li>
  
            <li>
              <a href="/MasterAdmin" class="collapsible-header"><i class="material-icons">account_circle</i><span class="hide-menu"> Master Admin</span></a>
            </li>
  
            <li>
              <a href="/customers" class="collapsible-header"><i class="material-icons">account_box</i><span class="hide-menu"> Customers</span></a>
            </li>
      
            <li>
              <a href="/jobs" class="collapsible-header"><i class="material-icons">work</i><span class="hide-menu"> Jobs</span></a>
            </li>
      
            <li>
              <a href="/vendor" class="collapsible-header"><i class="material-icons">supervised_user_circle</i><span class="hide-menu"> Vendor</span></a>
            </li>
  
            <li>
                <a href="/admin_list" class="collapsible-header"><i class="material-icons">account_circle</i><span class="hide-menu"> Admin</span></a>
              </li>
  
            <li>
              <a href="/AdminCandidateList" class="collapsible-header"><i class="material-icons">info</i><span class="hide-menu"> Shortlisted Candidates</span></a>
          </li>
            <!-- end -->
          </ul>

        @elseif(Auth::guard('master_admin')->check()) 
        <ul class="collapsible">
          <li class="small-cap"><span class="hide-menu">Admin</span></li>
          <li>
            <a href="/dashboard" class="collapsible-header"><i class="material-icons">dashboard</i><span class="hide-menu"> Dashboard</span></a>
          </li>


          <li>
            <a href="/customers" class="collapsible-header"><i class="material-icons">account_box</i><span class="hide-menu"> Customers</span></a>
          </li>
    
          <li>
            <a href="/jobs" class="collapsible-header"><i class="material-icons">work</i><span class="hide-menu"> Jobs</span></a>
          </li>
    
          <li>
            <a href="/vendor" class="collapsible-header"><i class="material-icons">supervised_user_circle</i><span class="hide-menu"> Vendor</span></a>
          </li>

          <li>
              <a href="/admin_list" class="collapsible-header"><i class="material-icons">account_circle</i><span class="hide-menu"> Admin</span></a>
            </li>

          <li>
            <a href="/AdminCandidateList" class="collapsible-header"><i class="material-icons">info</i><span class="hide-menu"> Shortlisted Candidates</span></a>
        </li>
          <!-- end -->
        </ul>
        @elseif(Auth::guard('vendor')->check())
        <ul class="collapsible">
            <li class="small-cap"><span class="hide-menu">Vendor</span></li>
            <li>
              <a href="/candidate_list" class="collapsible-header"><i class="material-icons">dashboard</i><span class="hide-menu"> Master Tracker</span></a>
            </li>

            <li>
                <a href="/jobs_list" class="collapsible-header"><i class="material-icons">work</i><span class="hide-menu">All Jobs</span></a>
            </li>

            {{-- <li>
              <a href="/Assigninterviews" class="collapsible-header"><i class="material-icons">dashboard</i><span class="hide-menu"> Candidates Tracker</span></a>
            </li> --}}

            <li>
                <a href="/Assigninterviews" class="collapsible-header"><i class="material-icons">info</i><span class="hide-menu"> Shorlisted Candidates</span></a>
            </li>
            <!-- end -->
            </ul>
          @elseif(Auth::guard('sub_admin')->check())
        <ul class="collapsible">
            <li class="small-cap"><span class="hide-menu">Vendor</span></li>
            <li>
              <a href="/Adminjobs" class="collapsible-header"><i class="material-icons">work</i><span class="hide-menu"> Jobs list</span></a>
            </li>

            <li>
              <a href="/AdminShortlistedCandidateList" class="collapsible-header"><i class="material-icons">info</i><span class="hide-menu"> Shorlisted Candidates</span></a>
          </li>

          <li>
            <a href="/AdminRejectedCandidateList" class="collapsible-header"><i class="material-icons">remove_circle</i><span class="hide-menu"> Rejected Candidates</span></a>
        </li>

            <!-- end -->
          </ul>
        @endif
      </li>
    </ul>
    </aside>