@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title activator">Form with placeholder<i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Get Code">more_vert</i></h5> -->
                    <form>
                        {{-- <div class="row">
                            <div class="input-field col s12 m6 l12">
                                    <select id="CustomerId">
                                        <option selected>Select customer</option>
                                            <option value="1">Name</option>
                                    </select>
                                    <label>Select customer</label>
                            </div>
                        </div> --}}

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="John Doe" value="{{$GetCandidates->name}}" id="name" type="text">
                                <input placeholder="John Doe" value="{{$GetCandidates->id}}" id="Id" type="hidden">
                                <label for="name2">Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="john@domainname.com" value="{{$GetCandidates->email}}" id="email" type="email">
                                <label for="email2">Email</label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="mobile" value="{{$GetCandidates->mobile}}" type="text" maxlength="10">
                                <label for="email2">Mobile</label>
                            </div>
                        </div>

                        <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="" id="skills" value="{{$GetCandidates->skills}}" type="text">
                                    <label for="email2">Skills</label>
                                </div>
                            </div>

                            <div class="row">
                                    <div class="input-field col s12">
                                        <input placeholder="" id="current_company" type="text" value="{{$GetCandidates->current_company}}">
                                        <label for="email2">Current Company</label>
                                    </div>
                                </div>
        
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input placeholder="" id="current_ctc" value="{{$GetCandidates->current_ctc}}" type="text">
                                        <label for="email2">Current CTC</label>
                                    </div>
                                </div>
        
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input placeholder="" id="notice_period" type="text" value="{{$GetCandidates->notice_period}}" maxlength="2">
                                        <label for="email2">Notice Period (days)</label>
                                    </div>
                                </div>


                        <div class="row">
                            <div class=" col s3">
                                <p>Update Resume</p>
                            </div>

                            <div class="input-field col s5">
                                <input placeholder="" id="resume" type="file" style="display:none;" accept="application/pdf">
                                <label for="resume">Click here to update file <br/><span style="color:red">{{$GetCandidates->resume}}</span></label>
                            </div>

                            <div class="input-field col s4">
                                    <label for="name2">Preview</label>
                                    <br>
                                    <br>
                                    @php
                                        if(isset($GetCandidates->resume)){
                                            echo "<iframe src='/resume/".$GetCandidates->resume."#toolbar=0' width='100%'' height='400px'>
                                        </iframe>";
                                        }
                                    @endphp
                                                        </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="button" id="UpdateCandidates" name="action">Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Code<i class="material-icons right">close</i></span>
                                                    <pre class="pre-scroll">                                                            <code class="language-markup">
                                                                &lt;form&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;John Doe&quot; id=&quot;name2&quot; type=&quot;text&quot;&gt;
                                                                            &lt;label for=&quot;name2&quot;&gt;Name&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;<a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="553f3a3d3b15313a38343c3b3b3438307b363a38">[email&#160;protected]</a>&quot; id=&quot;email2&quot; type=&quot;email&quot;&gt;
                                                                            &lt;label for=&quot;email2&quot;&gt;Email&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;YourPassword&quot; id=&quot;password2&quot; type=&quot;password&quot;&gt;
                                                                            &lt;label for=&quot;password2&quot;&gt;Password&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;textarea placeholder=&quot;Oh WoW! Let me check this one too.&quot; id=&quot;message2&quot; class=&quot;materialize-textarea&quot;&gt;&lt;/textarea&gt;
                                                                            &lt;label for=&quot;message2&quot;&gt;Message&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;button class=&quot;btn cyan waves-effect waves-light right&quot; type=&quot;submit&quot; name=&quot;action&quot;&gt;Submit
                                                                            &lt;/button&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                &lt;/form&gt;
                                                            </code>
                    </pre>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection