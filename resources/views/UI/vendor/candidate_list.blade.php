@extends('UI.base')

@section('Content')
<div class="container-fluid">
        

    
    <div class="row">
        <div class="col s12">
                    

            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title">Responsive Table</h5> -->
                    @if(Auth::guard('vendor')->check())
                    <a href="#JobDetails" class="waves-effect waves-light btn pull-right modal-trigger" name="action">Add To Jobs
                    </a> 
                    @endif

                    @if(Auth::guard('vendor')->check())
                     <a href="/add_candidate" class="btn waves-effect waves-light pull-right mar-right25" type="submit" name="action">Add Candidate
                    </a> 
                    @endif

                    {{--  <div class="row">
                        <div class="input-field col s4 m6 l4">
                                <select id="cname">
                                    <option selected disabled>Select Date</option>
                                    <option value="">Last Hour</option>
                                    <option value="">Last 7 Days</option>
                                    <option value="">Last Month</option>
                                    <option value="">Last 6 Month</option>
                                </select>
                                <label>Select Date</label>
                        </div>
                    </div>  --}}

                    <table class="responsive-table" id="CandidateDataTable">
                        <thead>
                            <tr>
                                {{-- <th>Customer Name</th> --}}
                                @if(Auth::guard('vendor')->check())
                                <th><label>
                                        <input type="checkbox" id="CheckAll" value="1" id="round"/>
                                        <span></span>
                                    </label></th>
                                    @endif
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Primary Skills</th>
                                <th>Current Company</th>
                                <th>Current CTC</th>
                                <th>Notice Period</th>
                                <th>Resume</th>
                                <th>Created date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody id="CandidateDetails">
                            @foreach($GetCandidates as $Candidates)
                            <tr>
                                    @if(Auth::guard('vendor')->check())
                                <td><p>
                                        <label>
                                            <input type="checkbox" class="CheckCandidate" name="Candidates[]" value="{{$Candidates->id}}" id=""/>
                                            <span></span>
                                        </label>
                                    </p></td>
                                    @endif
                                <td>{{$Candidates->name}}</td>
                                <td>{{$Candidates->email}}</td>
                                <td>{{$Candidates->mobile}}</td>
                                <td>{{$Candidates->skills}}</td>
                                <td>{{$Candidates->currenct_company}}</td>
                                <td>{{$Candidates->current_ctc}}</td>
                                <td>{{$Candidates->notice_period}} Days</td>
                                <td><a href='resume/{{$Candidates->resume}}' target='_blank'>View Resume</a></td>

                                <td>{{date('M d Y', strtotime($Candidates->created_at))}}</td>
                                
                                @if(Auth::guard('vendor')->check())
                                    <td><a href="/edit_candidate/{{$Candidates->id}}" target="_blank">Edit</a></td>
                                @elseif(Auth::guard('super_admin')->check())
                                    <td><a href="/CandidateDetails/{{$Candidates->id}}" target="_blank">View Details</a></td>
                                @elseif(Auth::guard('master_admin')->check())
                                    <td><a href="/CandidateDetails/{{$Candidates->id}}" target="_blank">View Details</a></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<div id="JobDetails" class="modal jobs-modal-width">
        <div class="modal-content">
            <h4 class="pull-left">Add Job Details</h4>
            <a href="javascript:void(0);" class="pull-right modal-close"><i class="material-icons dp48 candidate-close">close</i></a>
            <form>
                <div class="row">
                    <table class="responsive-table" id="JobsData">
                        <thead>
                            <tr>
                                <th>
                                    <label>
                                        <input type="checkbox" id="CheckAllJobs" value="1" id="round"/>
                                        <span></span>
                                    </label>
                                </th>
                                <th>Customer Name</th>
                                <th>Job Title</th>
                                <th>Qualification</th>
                                <th>Experience</th>
                                <th>Location</th>
                                <th>Primary Skills</th>
                                <th>Notice Period</th>
                                <th>Budget</th>
                                <th>Gender</th>
                            </tr>
                        </thead>

                        <tbody id="JobDetailsTable">
                            @foreach($GetJobs as $Jobs)
                            <tr>
                                <td><p>
                                    <label>
                                        <input type="checkbox" class="CheckJobs" name="Jobs[]" value="{{$Jobs->id}}" id=""/>
                                        <span></span>
                                    </label>
                                </p></td>
                                <td><input type="hidden" name="" id="CustomerId" value="{{$Jobs->customer_id}}" id=""/> {{$Jobs->name}}</td>
                                <td>{{$Jobs->job_title}}</td>
                                <td>{{$Jobs->qualification}}</td>
                                <td>{{$Jobs->experience_from}} - {{$Jobs->experience_to}}</td>
                                <td>{{$Jobs->location}}</td>
                                <td>{{$Jobs->details}}</td>
                                <td>{{$Jobs->notice_period}}</td>
                                <td>{{$Jobs->budget_from}} - {{$Jobs->budget_to}}</td>
                                <td>@if($Jobs->gender == 1)
                                Any
                                @elseif($Jobs->gender == 2)
                                    Male
                                @elseif($Jobs->gender == 3)
                                    Female
                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <a href="javascript:void(0);" id="SubmitJobsList" class="btn waves-effect waves-light pull-right mar-top20" type="button" name="action">Submit
                    </a> 
                </div>
            </form>
        </div>
        {{-- <div class="modal-footer">
                <a href="#" class="waves-effect waves-green btn-flat">Agree</a>
            </div> --}}
        </div>

@endsection


@section('JSScript')
    <script>
        $(document).ready( function () {
            // $('#CandidateDataTable').DataTable();
            $('#CandidateDataTable').dataTable( {
    language: {
        searchPlaceholder: "Type Name, Email, Mobile, Skills"
    }
} );
        } );

        $(document).ready( function () {
            // $('#CandidateDataTable').DataTable();
            $('#JobsData').dataTable( {
    language: {
        searchPlaceholder: "Type Keywords"
    }
} );
        } );

    </script>
@endsection