@extends('UI.base')
@section('Content')
<div class="container-fluid">
        <div class="row">
            
            <div class="col s12">
                <div class="row filter-bg-mar">
                        <h5 class="filter-head">Filter</h5>
        
                                <div class="input-field col s12 m6">
                                    <select id="customer_name" onchange="CheckCustomerJobsData()">
                                        <option selected>Select Customer</option>
                                        @foreach($GetCustomers as $Customers)
                                            <option value="{{$Customers->id}}">{{$Customers->name}}</option>
                                        @endforeach
                                    </select>
                                    <label>Select Customer</label>
                                </div>
        
                                <div class="input-field col s12 m6">
                                    <select id="job_name" onchange="CheckJobsListData()">
                                        <option selected>Select Jobs</option>
                                        @foreach($GetJobsList as $Jobs)
                                            <option value="{{$Jobs->id}}">{{$Jobs->job_title}}</option>
                                        @endforeach
                                    </select>
                                    <label>Select Jobs</label>
                                </div>
                    </div>

                <div class="card">
                    <div class="card-content">
                        {{-- <h5 class="card-title">Jobs</h5> --}}
                        <div class="row">
                            <div class="col s12 m12" id="JobsData">
                                <!-- Basic Card -->
                                @foreach($GetJobs as $Jobs)
                                <div class="card blue-grey darken-1 bg-white-shadow">
                                    <div class="card-content job-card-pad-bottom job_pad_bottom">
                                        <p class="pull-right job-status">@if($Jobs->status == 0) <span class="clr-green">Active</span> @elseif($Jobs->status == 1) <span class="clr-red">In Active</span> @endif</p>

                                        <span class="card-title">Job Title: {{$Jobs->job_title}}</span>
                                        
                                        <p><b>Customer Name:</b> {{$Jobs->name}}</p>
                                        <div class="col m4">
                                            <p>Qualification: {{$Jobs->qualification}}</p>
                                        </div>
                                        <div class="col m4">
                                        <p>Total Experience: {{$Jobs->experience_from}} Years - {{$Jobs->experience_to}} Months</p>
                                        </div>
                                        <div class="col m4">
                                            <p>Annual CTC Budget: {{$Jobs->budget_from}} </p>
                                        </div>
                                        <div class="col m4">
                                            <p>Primary Skills: {{$Jobs->details}}</p>
                                        </div>
                                        <div class="col m4">
                                            <p>Job Location: {{$Jobs->location}}</p>
                                        </div>
                                        <div class="col m4">
                                            <p>Notice period: {{$Jobs->notice_period}} Days</p>
                                        </div>

                                        {{-- <div class="col m4">
                                            <p>Job Status: @if($Jobs->status == 0) <span class="clr-green">Active</span> @elseif($Jobs->status == 1) <span class="clr-red">In Active</span> @endif</p>
                                        </div> --}}

                                        <div class="col m12 pad-top15">
                                            <p><b>Job Description:</b> {{$Jobs->job_description}}</p>
                                        </div>
                                        

                                        
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="modal1" class="modal">
            <div class="modal-content">
                <h4>Add Candidate Details</h4>
                <form>
                        <div class="row">
                            @foreach($GetCandidates as $Candidates)
                            <div class="col s4">
                                <p>
                                    <label>
                                        <input type="checkbox" value="{{$Candidates->id}}" id="round1"/>
                                        <span>{{$Candidates->name}}</span>
                                    </label>
                                </p>
                            </div>
                            @endforeach
                        </div>
                    </form>
            </div>
            {{-- <div class="modal-footer">
                    <a href="#" class="waves-effect waves-green btn-flat">Agree</a>
                </div> --}}
            </div>


            <div id="UserList" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                  
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title">Users list</h4>
                        </div>
                        <div class="modal-body">
                            <table class="responsive-table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Resume</th>
                                    </tr>
                                </thead>
                            <tbody id="UserDetails">
                                
                            </tbody>
                        </table>
                        </div>
                        {{-- <div class="modal-footer">
                          <button type="button" data-dismiss="modal" class="btn btn-default close">Close</button>
                        </div> --}}
                      </div>
                  
                    </div>
                  </div>
@endsection