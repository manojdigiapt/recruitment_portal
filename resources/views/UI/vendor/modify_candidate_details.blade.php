@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title activator">Form with placeholder<i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Get Code">more_vert</i></h5> -->
                    <form>
                        

                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Job Title" value="{{$GetCandidateDetails->JobName}}" id="job_title" type="text" disabled>

                                <input placeholder="Job Name" value="{{$GetCandidateDetails->Id}}" id="Id" type="hidden">
                                <label for="name2">Job Title</label>
                            </div>
                            <div class="input-field col s6">
                            <input placeholder="Candidate Name" value="{{$GetCandidateDetails->name}}" id="name" type="text" disabled>
                                <label for="name2">Candidate Name</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Email" value="{{$GetCandidateDetails->email}}" id="email" type="text" disabled>
                                <label for="name2">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <input placeholder="Mobile" value="{{$GetCandidateDetails->mobile}}" id="mobile" type="text" maxlength="10" disabled>
                                <label for="name2">Mobile</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                                    <input placeholder="Company Name" value="{{$GetCandidateDetails->current_company}}" id="company_name" type="text" disabled>
                                    <label for="name2">Company Name</label>
                                </div>
                            <div class="input-field col s6">
                                    <input placeholder="CTC" id="ctc" value="{{$GetCandidateDetails->current_ctc}}" type="text" disabled>
                                    <label for="name2">CTC</label>
                                </div>
                        </div>

                        <div class="row">
                            
                            <div class="input-field col s6">
                                    <input placeholder="Eg: 20 days" id="mode" value="{{$GetCandidateDetails->notice_period}}" type="text" disabled>
                                    <label for="name2">Notice Period</label>
                                </div>
                        </div>


                        {{-- <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="John Doe" id="schedule" value="{{$GetCandidateDetails->interview_schedule}}" type="date">
                                <label for="name2">Interview Date</label>
                            </div>
                            <div class="input-field col s6">
                                    <input placeholder="Feedback" value="{{$GetCandidateDetails->feedback}}" id="feedback" type="text">
                                    <label for="name2">Feedback</label>
                                </div>
                        </div> --}}
                        
                        <div class="row rounds-border" id="Round1Row">
                            {{-- <div class="input-field col s6">
                                <select id="interview_status">
                                    <option value="" disabled selected>Interview Status</option>
                                    <option value="1">Pending</option>
                                    <option value="2">Completed</option>
                                </select>
                                <label>Interview Status</label>
                            </div> --}}
                            <h5>Round 1</h5>
                            <div class="col s3">
                                    {{--  <input placeholder="Interview Status" value="{{$GetCandidateDetails->interview_stages}}" id="stage" type="text">  --}}
                                {{-- <label for="name2">Round 1</label> --}}
                                
                                <br>    
                                <p>
                                    <label>
                                        <input type="checkbox" value="1" id="round1" @if($GetCandidateRoundsDetails->round1 == 1) 
                                        checked @endif/>
                                        <span>Round 1</span>
                                    </label>
                                </p>
                            </div>
                            <div class="col s2">
                                    {{--  <input placeholder="Interview Status" value="{{$GetCandidateRoundsDetails->interview_stages}}" id="stage" type="text">  --}}
                                <label for="name2">Interview Status</label>
                                <p>
                                    <label>
                                        <input type="radio" class="round1" name="round1" value="1" id="stage1" @if($GetCandidateRoundsDetails->interview_stage_1 == 1) 
                                        checked @endif/>
                                        <span>Rejected</span>
                                    </label>
                                </p>
                            </div>
                            <div class="col s2">
                                <br>
                                <p>
                                    <label>
                                        <input type="radio" class="round1" name="round1" value="2" id="stage2" @if($GetCandidateRoundsDetails->interview_stage_1 == 2) 
                                        checked @endif disabled/>
                                        <span>Shortlisted</span>
                                    </label>
                                </p>
                            </div>
                            <div class="col s2">
                                <br>
                                <p>
                                    <label>
                                        <input type="radio" class="round1" name="round1" value="3" id="stage3" @if($GetCandidateRoundsDetails->interview_stage_1 == 3) 
                                        checked @endif/>
                                        <span>On Hold</span>
                                    </label>
                                </p>
                            </div>
                            <div class="col s2">
                                <br>
                                <p>
                                    <label>
                                        <input type="radio" class="round1" name="round1" value="4" id="stage4" @if($GetCandidateRoundsDetails->interview_stage_1 == 4) 
                                        checked @endif/>
                                        <span>Selected</span>
                                    </label>
                                </p>

                            </div>

                            <div class="col s1">
                                <button class="btn btn-sm cyan waves-effect waves-light right" type="button" id="ClearRound1" name="action">Clear
                                    </button>
                            </div>

                            <div class="input-field col s3">
                                <input placeholder="John Doe" id="schedule1" value="{{$GetCandidateRoundsDetails->round1_date}}" type="date">
                                <label for="name2">Interview Date</label>
                            </div>
                            <div class="input-field col s3">
                                <select id="interview_mode1">
                                    <option selected>Interview Mode</option>
                                    <option value="1" @if($GetCandidateRoundsDetails->interview_mode1 == 1) selected @endif>Face To Face</option>
                                    <option value="2" @if($GetCandidateRoundsDetails->interview_mode1 == 2) selected @endif>Telephonic</option>
                                    <option value="3" @if($GetCandidateRoundsDetails->interview_mode1 == 3) selected @endif>Skype</option>
                                    <option value="4" @if($GetCandidateRoundsDetails->interview_mode1 == 4) selected @endif>Online Test</option>
                                </select>
                                <label>Interview Mode</label>
                            </div>
                            <div class="input-field col s6">
                                <input placeholder="Feedback" value="{{$GetCandidateRoundsDetails->feedback1}}" id="feedback1" type="text">
                                <label for="name2">Feedback</label>
                            </div>
                        </div>
                        <br>

                        <div class="row rounds-border round-row-disable" id="Round2Row">
                                {{-- <div class="input-field col s6">
                                    <select id="interview_status">
                                        <option value="" disabled selected>Interview Status</option>
                                        <option value="1">Pending</option>
                                        <option value="2">Completed</option>
                                    </select>
                                    <label>Interview Status</label>
                                </div> --}}
                                <h5>Round 2</h5>
                                <div class="col s3">
                                        {{--  <input placeholder="Interview Status" value="{{$GetCandidateRoundsDetails->interview_stages}}" id="stage" type="text">  --}}
                                    {{-- <label for="name2">Rounds</label> --}}
                                    <p>
                                        <label>
                                            <input type="checkbox" value="1" id="round2" @if($GetCandidateRoundsDetails->round2 == 1) 
                                            checked @endif/>
                                            <span>Round 2</span>
                                        </label>
                                    </p>
                                </div>
                                <div class="col s2">
                                        {{--  <input placeholder="Interview Status" value="{{$GetCandidateRoundsDetails->interview_stages}}" id="stage" type="text">  --}}
                                    <label for="name2">Interview Status</label>
                                    <p>
                                        <label>
                                            <input type="radio" name="round2" value="1" id="stage5" @if($GetCandidateRoundsDetails->interview_stage_2 == 1) 
                                            checked @endif/>
                                            <span>Rejected</span>
                                        </label>
                                    </p>
                                </div>
                                <div class="col s2">
                                    <br>
                                    <p>
                                        <label>
                                            <input type="radio" name="round2" value="2" id="stage6" @if($GetCandidateRoundsDetails->interview_stage_2 == 2) 
                                            checked @endif disabled/>
                                            <span>Shortlisted</span>
                                        </label>
                                    </p>
                                </div>
                                <div class="col s2">
                                    <br>
                                    <p>
                                        <label>
                                            <input type="radio" name="round2" value="3" id="stage7" @if($GetCandidateRoundsDetails->interview_stage_2 == 3) 
                                            checked @endif/>
                                            <span>On Hold</span>
                                        </label>
                                    </p>
                                </div>
                                <div class="col s2">
                                    <br>
                                    <p>
                                        <label>
                                            <input type="radio" name="round2" value="4" id="stage8" @if($GetCandidateRoundsDetails->interview_stage_2 == 4) 
                                            checked @endif/>
                                            <span>Selected</span>
                                        </label>
                                    </p>
                                </div>

                                <div class="col s1">
                                        <button class="btn btn-sm cyan waves-effect waves-light right" type="button" id="ClearRound2" name="action">Clear
                                            </button>
                                    </div>
    
                                <div class="input-field col s3">
                                    <input placeholder="John Doe" id="schedule2" value="{{$GetCandidateRoundsDetails->round2_date}}" type="date">
                                    <label for="name2">Interview Date</label>
                                </div>
                                <div class="input-field col s3">
                                    <select id="interview_mode2">
                                        <option >Interview Mode</option>
                                        <option value="1" @if($GetCandidateRoundsDetails->interview_mode2 == 1) selected @endif>Face To Face</option>
                                        <option value="2" @if($GetCandidateRoundsDetails->interview_mode2 == 2) selected @endif>Telephonic</option>
                                        <option value="3" @if($GetCandidateRoundsDetails->interview_mode2 == 3) selected @endif>Skype</option>
                                        <option value="4" @if($GetCandidateRoundsDetails->interview_mode2 == 4) selected @endif>Online Test</option>
                                    </select>
                                    <label>Interview Mode</label>
                                </div>
                                <div class="input-field col s6">
                                    <input placeholder="Feedback" value="{{$GetCandidateRoundsDetails->feedback2}}" id="feedback2" type="text">
                                    <label for="name2">Feedback</label>
                                </div>
                            </div>
                            <br>

                            <div class="row rounds-border round-row-disable  " id="Round3Row">
                                    {{-- <div class="input-field col s6">
                                        <select id="interview_status">
                                            <option value="" disabled selected>Interview Status</option>
                                            <option value="1">Pending</option>
                                            <option value="2">Completed</option>
                                        </select>
                                        <label>Interview Status</label>
                                    </div> --}}
                                    <h5>Round 3</h5>
                                    <div class="col s3">
                                            {{--  <input placeholder="Interview Status" value="{{$GetCandidateRoundsDetails->interview_stages}}" id="stage" type="text">  --}}
                                        {{-- <label for="name2">Rounds</label> --}}
                                        <p>
                                            <label>
                                                <input type="checkbox" value="1" id="round3" @if($GetCandidateRoundsDetails->round3 == 1) 
                                                checked @endif/>
                                                <span>Round 3</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="col s2">
                                            {{--  <input placeholder="Interview Status" value="{{$GetCandidateRoundsDetails->interview_stages}}" id="stage" type="text">  --}}
                                        <label for="name2">Interview Status</label>
                                        <p>
                                            <label>
                                                <input type="radio" name="round3" value="1" id="stage9" @if($GetCandidateRoundsDetails->interview_stage_3 == 1) 
                                                checked @endif/>
                                                <span>Rejected</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="col s2">
                                        <br>
                                        <p>
                                            <label>
                                                <input type="radio" name="round3" value="2" id="stage10" @if($GetCandidateRoundsDetails->interview_stage_3 == 2) 
                                                checked @endif disabled/>
                                                <span>Shortlisted</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="col s2">
                                        <br>
                                        <p>
                                            <label>
                                                <input type="radio" name="round3" value="3" id="stage11" @if($GetCandidateRoundsDetails->interview_stage_3 == 3) 
                                                checked @endif/>
                                                <span>On Hold</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="col s2">
                                        <br>
                                        <p>
                                            <label>
                                                <input type="radio" name="round3" value="4" id="stage12" @if($GetCandidateRoundsDetails->interview_stage_4 == 4) 
                                                checked @endif/>
                                                <span>Selected</span>
                                            </label>
                                        </p>
                                    </div>

                                    <div class="col s1">
                                            <button class="btn btn-sm cyan waves-effect waves-light right" type="button" id="ClearRound3" name="action">Clear
                                                </button>
                                        </div>
        
                                    <div class="input-field col s3">
                                        <input placeholder="John Doe" id="schedule3" value="{{$GetCandidateRoundsDetails->round3_date}}" type="date">
                                        <label for="name2">Interview Date</label>
                                    </div>
                                    <div class="input-field col s3">
                                        <select id="interview_mode3">
                                            <option >Interview Mode</option>
                                            <option value="1" @if($GetCandidateRoundsDetails->interview_mode3 == 1) selected @endif>Face To Face</option>
                                            <option value="2" @if($GetCandidateRoundsDetails->interview_mode3 == 2) selected @endif>Telephonic</option>
                                            <option value="3" @if($GetCandidateRoundsDetails->interview_mode3 == 3) selected @endif>Skype</option>
                                            <option value="4" @if($GetCandidateRoundsDetails->interview_mode3 == 4) selected @endif>Online Test</option>
                                        </select>
                                        <label>Interview Mode</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input placeholder="Feedback" value="{{$GetCandidateRoundsDetails->feedback3}}" id="feedback3" type="text">
                                        <label for="name2">Feedback</label>
                                    </div>
                                </div>
                                <br>

                                <div class="row rounds-border round-row-disable " id="Round4Row">
                                        {{-- <div class="input-field col s6">
                                            <select id="interview_status">
                                                <option value="" disabled selected>Interview Status</option>
                                                <option value="1">Pending</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <label>Interview Status</label>
                                        </div> --}}
                                        <h5>Round 4</h5>
                                        <div class="col s3">
                                                {{--  <input placeholder="Interview Status" value="{{$GetCandidateRoundsDetails->interview_stages}}" id="stage" type="text">  --}}
                                            {{-- <label for="name2">Rounds</label> --}}
                                            <p>
                                                <label>
                                                    <input type="checkbox" value="1" id="round4" @if($GetCandidateRoundsDetails->round4 == 1) 
                                                    checked @endif/>
                                                    <span>Round 4</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col s2">
                                                {{--  <input placeholder="Interview Status" value="{{$GetCandidateRoundsDetails->interview_stages}}" id="stage" type="text">  --}}
                                            <label for="name2">Interview Status</label>
                                            <p>
                                                <label>
                                                    <input type="radio" name="round4" value="1" id="stage13" @if($GetCandidateRoundsDetails->interview_stage_4 == 1) 
                                                    checked @endif/>
                                                    <span>Rejected</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col s2">
                                            <br>
                                            <p>
                                                <label>
                                                    <input type="radio" name="round4" value="2" id="stage14" @if($GetCandidateRoundsDetails->interview_stage_4 == 2) 
                                                    checked @endif disabled/>
                                                    <span>Shortlisted</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col s2">
                                            <br>
                                            <p>
                                                <label>
                                                    <input type="radio" name="round4" value="3" id="stage15" @if($GetCandidateRoundsDetails->interview_stage_4 == 3) 
                                                    checked @endif/>
                                                    <span>On Hold</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col s2">
                                            <br>
                                            <p>
                                                <label>
                                                    <input type="radio" name="round4" value="4" id="stage16" @if($GetCandidateRoundsDetails->interview_stage_4 == 4) 
                                                    checked @endif/>
                                                    <span>Selected</span>
                                                </label>
                                            </p>
                                        </div>

                                        <div class="col s1">
                                            <button class="btn btn-sm cyan waves-effect waves-light right" type="button" id="ClearRound4" name="action">Clear
                                                </button>
                                        </div>
            
                                        <div class="input-field col s3">
                                            <input placeholder="John Doe" id="schedule4" value="{{$GetCandidateRoundsDetails->round4_date}}" type="date">
                                            <label for="name2">Interview Date</label>
                                        </div>
                                        <div class="input-field col s3">
                                            <select id="interview_mode4">
                                                <option >Interview Mode</option>
                                                <option value="1" @if($GetCandidateRoundsDetails->interview_mode4 == 1) selected @endif>Face To Face</option>
                                                <option value="2" @if($GetCandidateRoundsDetails->interview_mode4 == 2) selected @endif>Telephonic</option>
                                                <option value="3" @if($GetCandidateRoundsDetails->interview_mode4 == 3) selected @endif>Skype</option>
                                                <option value="4" @if($GetCandidateRoundsDetails->interview_mode4 == 4) selected @endif>Online Test</option>
                                            </select>
                                            <label>Interview Mode</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input placeholder="Feedback" value="{{$GetCandidateRoundsDetails->feedback4}}" id="feedback4" type="text">
                                            <label for="name2">Feedback</label>
                                        </div>
                                    </div>
                                    <br>


                        {{-- <div class="row">
                            <div class="input-field col s6">
                                    <input placeholder="Company Name" value="{{$GetCandidateDetails->company_name}}" id="company_name" type="text">
                                    <label for="name2">Company Name</label>
                                </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="CTC" id="ctc" value="{{$GetCandidateDetails->ctc}}" type="text">
                                <label for="name2">CTC</label>
                            </div>
                            <div class="input-field col s6">
                                    <input placeholder="Mode" id="mode" value="{{$GetCandidateDetails->mode}}" type="text">
                                    <label for="name2">Mode</label>
                                </div>
                        </div> --}}


                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="button" id="AddCandidateDetails" name="action">Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection