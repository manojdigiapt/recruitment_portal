@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title">Responsive Table</h5> -->
                    <a href="/add_customers" class="btn waves-effect waves-light pull-right" type="submit" name="action">Add Customer
                        <!-- <i class="material-icons right">send</i> -->
                    </a>
                    <table class="responsive-table" id="CustomersData">
                        <thead>
                            <tr>
                                <th>Customer Name</th>
                                <th>Email</th>
                                <th>Contact Person Name</th>
                                <th>Mobile</th>
                                <th>Customer Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($GetCustomers as $Customers)
                            <tr>
                                <td>{{$Customers->name}}</td>
                                <td>{{$Customers->email}}</td>
                                <td>{{$Customers->contact}}</td>
                                <td>{{$Customers->mobile}}</td>
                                <td>{{$Customers->address}}</td>
                                <td><div class="switch" style="
                                    width: 170px;
                                ">
                                    <label>
                                        <span class="clr-green">Active</span>
                                        <input type="checkbox" onchange="ChangeStatus({{$Customers->id}})" @if($Customers->status == 1)
                                            checked
                                        @else
                                        
                                        @endif>
                                        <span class="lever"></span>
                                        <span class="clr-red">In Active</span>
                                    </label>
                                </div></td>
                                <td><a href="/edit_customers/{{$Customers->id}}" target="_blank">Edit</a></td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('JSScript')
    <script>
         $(document).ready( function () {
            // $('#CandidateDataTable').DataTable();
            $('#CustomersData').dataTable( {
    language: {
        searchPlaceholder: "Type Name, Email, Mobile"
    }
} );
        } );

    </script>
@endsection