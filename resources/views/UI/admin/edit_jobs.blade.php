@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title activator">Form with placeholder<i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Get Code">more_vert</i></h5> -->
                    <form>
                        <div class="row">
                            <div class="input-field col s12 m6 l12">
                                    <select id="cname">
                                        @foreach($GetCustomers as $Customers)
                                            <option @if($GetJobs['customer_id'] == $Customers->id) selected @else @endif value="{{$Customers->id}}">{{$Customers->name}}</option>
                                        @endforeach
                                    </select>
                                    <label>Select customer</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m6 l12">
                                <select id="vendor_id">
                                    @foreach($GetVendors as $Vendor)
                                        <option @if($GetJobs['vendor_id'] == $Vendor->id) selected @else @endif value="{{$Vendor->id}}">{{$Vendor->name}}</option>
                                    @endforeach
                                </select>
                                <label>Select vendor</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                            <input placeholder="" id="title" value="{{$GetJobs['job_title']}}" type="text">
                            <input placeholder="" id="Id" value="{{$GetJobs['id']}}" type="hidden">
                            <label for="name2">Job Title</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" value="{{$GetJobs['qualification']}}" id="qualification" type="text">
                                <label for="name2">Qualification</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s4">
                                <input placeholder="" id="exp_from" value="{{$GetJobs['experience_from']}}" type="text" maxlength="2">
                                <label for="name2">Experience</label>
                            </div>
                            <div class="input-field col s2">
                                <p class="pad-top15">Years</p>
                            </div>
                            <div class="input-field col s4">
                                    <input placeholder="" value="{{$GetJobs['experience_to']}}" id="exp_to" type="text" maxlength="2">
                                    {{-- <label for="name2">Experience    To</label> --}}
                            </div>
                            <div class="input-field col s2">
                                <p class="pad-top15">Months</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="location" value="{{$GetJobs['location']}}" type="text">
                                <label for="name2">Job Location</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" value="{{$GetJobs['notice_period']}}" id="notice_period" type="text" maxlength="2">
                                <label for="name2">Notice period (days)</label>
                            </div>
                        </div>

                        <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="" value="{{$GetJobs['budget_from']}}" id="budget_from" type="text">
                                    <label for="name2">Annual CTC Budget (In Lacs)</label>
                                </div>
                                {{-- <div class="input-field col s6">
                                        <input placeholder="" value="{{$GetJobs['budget_to']}}" id="budget_to" type="text">
                                        <label for="name2">Annual CTC Budget    To</label>
                                    </div> --}}
                            </div>
                            
                            <div class="row">
                                <div class="input-field col s12 m6 l12">
                                        <select id="gender">
                                            @if($GetJobs['gender'] == 1)
                                            <option value="1" selected>Any</option>
                                            <option value="2">Male</option>
                                            <option value="3">Female</option>
                                            @elseif($GetJobs['gender'] == 2)
                                            <option value="1" >Any</option>
                                            <option value="2" selected>Male</option>
                                            <option value="3">Female</option>
                                            @else
                                            <option value="1" >Any</option>
                                            <option value="2" >Male</option>
                                            <option value="3" selected>Female</option>
                                            @endif
                                            
                                        </select>
                                        <label>Select gender</label>
                                </div>
                            </div>

                            <div class="row">
                                    <div class="input-field col s12">
                                        <textarea placeholder="" id="information" class="materialize-textarea">{{$GetJobs['details']}}</textarea>
                                        <label for="message2">Primary Skills</label>
                                    </div>
                                </div>

                                <div class="row">
                                        <div class="input-field col s12">
                                            <textarea placeholder="" id="description" class="materialize-textarea">{{$GetJobs['job_description']}}</textarea>
                                            <label for="message2">Job Description</label>
                                        </div>
                                    </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="button" id="UpdateJobs" name="action">Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Code<i class="material-icons right">close</i></span>
                                                    <pre class="pre-scroll">                                                            <code class="language-markup">
                                                                &lt;form&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;&quot; id=&quot;name2&quot; type=&quot;text&quot;&gt;
                                                                            &lt;label for=&quot;name2&quot;&gt;Name&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;<a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="553f3a3d3b15313a38343c3b3b3438307b363a38">[email&#160;protected]</a>&quot; id=&quot;email2&quot; type=&quot;email&quot;&gt;
                                                                            &lt;label for=&quot;email2&quot;&gt;Email&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;YourPassword&quot; id=&quot;password2&quot; type=&quot;password&quot;&gt;
                                                                            &lt;label for=&quot;password2&quot;&gt;Password&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;textarea placeholder=&quot;Oh WoW! Let me check this one too.&quot; id=&quot;message2&quot; class=&quot;materialize-textarea&quot;&gt;&lt;/textarea&gt;
                                                                            &lt;label for=&quot;message2&quot;&gt;Message&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;button class=&quot;btn cyan waves-effect waves-light right&quot; type=&quot;submit&quot; name=&quot;action&quot;&gt;Submit
                                                                            &lt;/button&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                &lt;/form&gt;
                                                            </code>
                    </pre>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection