@extends('UI.base')

@section('Content')
<div class="container-fluid">
        

    
    <div class="row">
        <div class="col s12">
            <div class="row filter-bg-mar">
                <h5 class="filter-head">Filter</h5>

                        <div class="input-field col s12 m6">
                            <select id="customer_name" onchange="FilterCustomerJobsData()">
                                <option selected>Select Customer</option>
                                @foreach($GetCustomers as $Customers)
                                    <option value="{{$Customers->id}}">{{$Customers->name}}</option>
                                @endforeach
                            </select>
                            <label>Select Customer</label>
                        </div>

                        <div class="input-field col s12 m6">
                            <select id="job_name" onchange="FilterJobsListData()">
                                <option selected>Select Jobs</option>
                                @foreach($GetJobs as $Jobs)
                                    <option value="{{$Jobs->id}}">{{$Jobs->job_title}}</option>
                                @endforeach
                            </select>
                            <label>Select Jobs</label>
                        </div>
        
                        {{-- <div class="col s4 pad-top15">
                                <select id="final_status" onchange="CheckCandidateFinalStatusFilters()">
                                        <option value="" disabled selected>Final Status</option>
                                        <option value="0">All</option>
                                        <option value="1">Selected</option>
                                        <option value="2">Rejected</option>
                                    </select>
                                    <label>Final Status</label>
                            </div> --}}
        
                            {{-- <div class="col s4 pad-top15">
                                    <select id="feedback" onchange="CheckCandidateFeedbackFilters()">
                                            <option value="" disabled selected>Select Feedback</option>
                                            <option value="1">Freelance consultant</option>
                                            <option value="2">Proprietorship</option>
                                            <option value="3">Partnership Firm</option>
                                            <option value="4">LLP</option>
                                            <option value="5">Pvt Ltd Company</option>
                                            <option value="6">Others</option>
                                        </select>
                                        <label>Select Feedback</label>
                                </div> --}}
            </div>
                

            <div class="row filter-bg-mar" style="display:none;" id="FilterRounds">
                    <h5 class="filter-head">Filter Rounds</h5>
    
                            <div class="input-field col s12 m6">
                                @if(Auth::guard('vendor')->check())
                                    <select id="interview_status" onchange="CheckCandidateInterviewVendorFilters()">
                                @elseif(Auth::guard('super_admin')->check())
                                    <select id="interview_status" onchange="CheckCandidateInterviewAdminFilters()">
                                @elseif(Auth::guard('master_admin')->check())
                                    <select id="interview_status" onchange="CheckCandidateInterviewAdminFilters()">
                                @endif
                                    <option value="0">All</option>
                                    <optgroup label="Round 1">
                                        <option value="1">Rejected</option>
                                        <option value="2">Shortlisted</option>
                                        <option value="3">On Hold</option>
                                        <option value="4">Selected</option>
                                    </optgroup>
                                    <optgroup label="Round 2">
                                        <option value="5">Rejected</option>
                                        <option value="6">Shortlisted</option>
                                        <option value="7">On Hold</option>
                                        <option value="8">Selected</option>
                                    </optgroup>
    
                                    <optgroup label="Round 3">
                                        <option value="9">Rejected</option>
                                        <option value="10">Shortlisted</option>
                                        <option value="11">On Hold</option>
                                        <option value="12">Selected</option>
                                    </optgroup>
    
                                    <optgroup label="Round 4">
                                        <option value="13">Rejected</option>
                                        <option value="14">Shortlisted</option>
                                        <option value="15">On Hold</option>
                                        <option value="16">Selected</option>
                                    </optgroup>
                                </select>
                                <label>Rounds</label>
                            </div>
            
                </div>
                    

            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title">Responsive Table</h5> -->
                    {{--  <a href="/add_customers" class="btn waves-effect waves-light pull-right" type="submit" name="action">Add Customer
                    </a>  --}}
                    <table class="responsive-table" id="CandidateData">
                        <thead>
                            <tr>
                                {{-- <th>Customer Name</th> --}}
                                <th>Candidate Name</th>
                                <th>Email</th>
                                <th>Job Title</th>
                                <th>Round 1</th>
                                <th>Round 2</th>
                                <th>Round 3</th>
                                <th>Round 4</th>
                                <th>Shortlisted Status</th>
                                {{-- <th>Email</th>
                                <th>Mobile</th> --}}
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody id="CandidateDetails">
                            {{-- <tr></tr> --}}
                            @foreach($GetCandidates as $Candidates)
                            <tr>
                                <td>{{$Candidates->name}}</td>
                                <td>{{$Candidates->email}}</td>
                                <td>{{$Candidates->job_title}}</td>
                                {{-- <td>{{$Candidates->mobile}}</td> --}}
                                <td>@if($Candidates->Round1 == 1)
                                        Rejected
                                    @elseif($Candidates->Round1 == 2)
                                        Shortlisted
                                    @elseif($Candidates->Round1 == 3)
                                        On Hold
                                    @elseif($Candidates->Round1 == 4)
                                        Selected
                                    @endif</td>
                                <td>@if($Candidates->Round2 == 1)
                                    Rejected
                                @elseif($Candidates->Round2 == 2)
                                    Shortlisted
                                @elseif($Candidates->Round2 == 3)
                                    On Hold
                                @elseif($Candidates->Round2 == 4)
                                    Selected
                                @endif</td>
                                <td>@if($Candidates->Round3 == 1)
                                    Rejected
                                @elseif($Candidates->Round3 == 2)
                                    Shortlisted
                                @elseif($Candidates->Round3 == 3)
                                    On Hold
                                @elseif($Candidates->Round3 == 4)
                                    Selected
                                @endif</td>
                                <td>@if($Candidates->Round4 == 1)
                                    Rejected
                                @elseif($Candidates->Round4 == 2)
                                    Shortlisted
                                @elseif($Candidates->Round4 == 3)
                                    On Hold
                                @elseif($Candidates->Round4 == 4)
                                    Selected
                                @endif</td>
                                
                                <td> @if($Candidates->shortlisted_status == 2)

                                    <span class="clr-red">Rejected</span>
                                    @elseif($Candidates->shortlisted_status == 1)
                                    <span class="clr-green">Shortlisted</span>
                                    @endif
                                </td>

                            <td>
                                @if(Auth::guard('vendor')->check())
                                    <a href="/ModifyCandidateDetails/{{$Candidates->Id}}" target="_blank">Modify Status</a></td>
                                @elseif(Auth::guard('super_admin')->check())
                                    @if($Candidates->shortlisted_status != 2)
                                        <a href="/CandidateDetails/{{$Candidates->Id}}" target="_blank">View Details</a></td>
                                    @endif
                                @elseif(Auth::guard('master_admin')->check())
                                    @if($Candidates->shortlisted_status != 2)
                                        <a href="/CandidateDetails/{{$Candidates->Id}}" target="_blank">View Details</a></td>
                                    @endif
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('JSScript')
    <script>
         $(document).ready( function () {
            // $('#CandidateDataTable').DataTable();
            $('#CandidateData').dataTable( {
    language: {
        searchPlaceholder: "Type Keywords"
    }
} );
        } );

    </script>
@endsection