@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title activator">Form with placeholder<i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Get Code">more_vert</i></h5> -->
                    <form>
                        <div class="row">
                            <div class="input-field col s12 m6 l12">
                                    <select id="vendor_type">
                                        <option selected>Select Vendor Type</option>
                                        <option value="1">Freelance consultant</option>
                                        <option value="2">Proprietorship</option>
                                        <option value="3">Partnership Firm</option>
                                        <option value="4">LLP</option>
                                        <option value="5">Pvt Ltd Company</option>
                                        <option value="6">Others</option>
                                    </select>
                                    <label>Vendor Type</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m6 l12">
                                    <select id="service_category">
                                        <option selected>Service Category</option>
                                        <option value="1">Technical Hiring</option>
                                        <option value="2">Non Technical Hiring</option>
                                    </select>
                                    <label>Service Category</label>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="cname" type="text">
                                <label for="name2">Vendor Name</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="Password" id="password" type="password">
                                <label for="name2">Password</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="address" type="text">
                                <label for="name2">Address</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="contact" type="text">
                                <label for="name2">Contact Person Name</label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="email" type="text">
                                <label for="name2">Contact Person Email</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="mobile" type="text" maxlength="10">
                                <label for="name2">Contact Person Mobile</label>
                            </div>
                        </div>

                        <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="" id="pan" type="text">
                                    <label for="name2">PAN Number</label>
                                </div>
                            </div>

                            <div class="row">
                                    <div class="input-field col s12">
                                        <input placeholder="" id="gst" type="text">
                                        <label for="name2">GST Number</label>
                                    </div>
                                </div>

                                <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="Eg: 3 Years" id="tenure" type="text">
                                            <label for="name2">Contract tenure (months or years)</label>
                                        </div>
                                    </div>

                            

                            <div class="row">
                                    <div class="input-field col s12">
                                        <textarea placeholder="" id="payment" class="materialize-textarea"></textarea>
                                        <label for="message2">Payment terms</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s3">
                                        <label for="name2">PAN Copy (Pdf, Jpg, Jpeg, Png)</label>
                                    </div>

                                    <div class="input-field col s4">
                                        <input placeholder="" id="pan_copy" type="file" accept="image/jpg,image/jpeg,image/png,application/pdf">
                                    </div>

                                    <div class="col s5">
                                        <img id="pan_img" src="#" alt="your image" class="customers-agreement-width vendor-pre-image-width" style="display:none;"/>

                                        <iframe id="Pan_viewer" frameborder="0" scrolling="no" width="100%" height="300" style="display:none;"></iframe>
                                    </div>

                                </div>

                                <br>

                                    <div class="row">
                                            <div class="input-field col s3">
                                                <label for="name2">GST Certificate (Pdf, Jpg, Jpeg, Png)</label>
                                            </div>
        
                                            <div class="input-field col s4">
                                                <input placeholder="" id="gst_certificate" type="file" accept="image/jpg,image/jpeg,image/png,application/pdf">
                                            </div>

                                            <div class="col s5">
                                                <img id="gst_img" src="#" alt="your image" class="customers-agreement-width vendor-pre-image-width"  style="display:none;"/>

                                                <iframe id="Gst_viewer" frameborder="0" scrolling="no" width="100%" height="300" style="display:none;"></iframe>
                                            </div>
                                        </div>

                                        <br>

                                        <div class="row">
                                                <div class="input-field col s3">
                                                    <label for="name2">Company Incorporation Certificate (Pdf, Jpg, Jpeg, Png)</label>
                                                </div>
            
                                                <div class="input-field col s4">
                                                    <input placeholder="" id="company_certificate" type="file" accept="image/jpg,image/jpeg,image/png,application/pdf">
                                                </div>

                                                <div class="col s5">
                                                    <img id="company_img" src="#" alt="your image" class="customers-agreement-width vendor-pre-image-width" style="display:none;"/>

                                                    <iframe id="Company_viewer" frameborder="0" scrolling="no" width="100%" height="300" style="display:none;"></iframe>
                                                </div>
                                            </div>

                                            <br>

                                            <div class="row">
                                                    <div class="input-field col s3">
                                                        <label for="name2">Upload agreement (Pdf, Jpg, Jpeg, Png)</label>
                                                    </div>
                
                                                    <div class="input-field col s4">
                                                        <input placeholder="" id="agreement" type="file" accept="image/jpg,image/jpeg,image/png,application/pdf">
                                                    </div>

                                                    <div class="col s5">
                                                        <img id="agreement_img" src="#" alt="your image" class="customers-agreement-width vendor-pre-image-width" style="display:none;" />

                                                        <iframe id="Agreement_viewer" frameborder="0" scrolling="no" width="100%" height="300" style="display:none;"></iframe>
                                                    </div>
                                                </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="button" id="AddVendor" name="action">Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Code<i class="material-icons right">close</i></span>
                                                    <pre class="pre-scroll">                                                            <code class="language-markup">
                                                                &lt;form&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;&quot; id=&quot;name2&quot; type=&quot;text&quot;&gt;
                                                                            &lt;label for=&quot;name2&quot;&gt;Name&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;<a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="553f3a3d3b15313a38343c3b3b3438307b363a38">[email&#160;protected]</a>&quot; id=&quot;email2&quot; type=&quot;email&quot;&gt;
                                                                            &lt;label for=&quot;email2&quot;&gt;Email&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;YourPassword&quot; id=&quot;password2&quot; type=&quot;password&quot;&gt;
                                                                            &lt;label for=&quot;password2&quot;&gt;Password&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;textarea placeholder=&quot;Oh WoW! Let me check this one too.&quot; id=&quot;message2&quot; class=&quot;materialize-textarea&quot;&gt;&lt;/textarea&gt;
                                                                            &lt;label for=&quot;message2&quot;&gt;Message&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;button class=&quot;btn cyan waves-effect waves-light right&quot; type=&quot;submit&quot; name=&quot;action&quot;&gt;Submit
                                                                            &lt;/button&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                &lt;/form&gt;
                                                            </code>
                    </pre>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


