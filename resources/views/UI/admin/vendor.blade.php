@extends('UI.base')



@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title">Responsive Table</h5> -->
                    <a href="/add_vendor" class="btn waves-effect waves-light pull-right" type="submit" name="action">Add Vendor
                        <!-- <i class="material-icons right">send</i> -->
                    </a>
                    <table class="responsive-table" id="VendorData">
                        <thead>
                            <tr>
                                {{--  <th>Id</th>  --}}
                                <th>Service Category</th>
                                <th>Vendor Name</th>
                                <th>Vendor Address</th>
                                <th>Contact Person Name</th>
                                <th>Contact Person Email</th>
                                <th>Contact Person Mobile</th>
                                <th>PAN NO</th>
                                <th>GST</th>
                                <th>Contract Tenure</th>
                                <th>Payment terms</th>
                                <th>Created Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($GetVendors as $Vendors)
                            <tr>
                                {{--  <td>{{$Vendors->id}}</td>  --}}
                                <td>@if($Vendors->service_category == 1)
                                        Technical Hiring
                                    @elseif($Vendors->service_category == 2)
                                        Non Technical Hiring
                                    @endif
                                </td>
                                <td>{{$Vendors->name}}</td>
                                <td>{{$Vendors->address}}</td>
                                <td>{{$Vendors->contact}}</td>
                                <td>{{$Vendors->email}}</td>
                                <td>{{$Vendors->mobile}}</td>
                                <td>{{$Vendors->pan_no}}</td>
                                <td>{{$Vendors->gst}}</td>
                                <td>{{$Vendors->contract_tenure}}</td>
                                <td>{{$Vendors->payment_terms}}</td>
                                <td>{{date('d M Y', strtotime($Vendors->created_at))}}</td>
                                <td><div class="switch" style="
                                    width: 170px;
                                ">
                                        <label>
                                            <span class="clr-green">Active</span>
                                            <input type="checkbox" onchange="ChangeVendorStatus({{$Vendors->id}})" @if($Vendors->status == 1)
                                                checked
                                            @else
                                            
                                            @endif>
                                            <span class="lever"></span>
                                            <span class="clr-red">In Active</span>
                                        </label>
                                    </div></td>

                            <td><a href="/edit_vendor/{{$Vendors->id}}" target="_blank">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('JSScript')
    <script>
         $(document).ready( function () {
            // $('#CandidateDataTable').DataTable();
            $('#VendorData').dataTable( {
                order: [
                    [10, 'desc']
                ],
    language: {
        searchPlaceholder: "Type Keywords"
    }
} );
        } );

    </script>
@endsection