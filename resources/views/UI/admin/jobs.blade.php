@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title">Responsive Table</h5> -->
                    <a href="/add_jobs" class="btn waves-effect waves-light pull-right" type="submit" name="action">Add Jobs
                        <!-- <i class="material-icons right">send</i> -->
                    </a>
                    <div>
                        <table class="responsive-table" id="JobsData">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Vendor Name</th>
                                    <th>Job Title</th>
                                    <th>Qualification</th>
                                    <th>Total Experience</th>
                                    <th>Job Location</th>
                                    <th>Notice Period</th>
                                    <th>Annual CTC Budget</th>
                                    <th>Gender</th>
                                    <th>Primary Skills</th>
                                    
                                    {{-- <th>Job Location</th> --}}
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($GetJobs as $Jobs)
                                <tr>
                                    <td>{{$Jobs->name}}</td>
                                    <td>{{$Jobs->VendorName}}</td>
                                    <td>{{$Jobs->job_title}}</td>
                                    <td>{{$Jobs->qualification}}</td>
                                    <td>{{$Jobs->experience_from}} Years - {{$Jobs->experience_to}} Months</td>
                                    <td>{{$Jobs->location}}</td>
                                    <td>{{$Jobs->notice_period}}</td>
                                    <td>{{$Jobs->budget_from}} </td>
                                    
                                    <td>@if($Jobs->gender == 1)
                                        Any
                                    @elseif($Jobs->gender == 2)
                                        Male
                                @elseif($Jobs->gender == 3)
                                        Female
                            @endif</td>
                                    <td>{{$Jobs->details}}</td>
                                    {{-- <td>{{$Jobs->location}}</td> --}}
                                    <td><div class="switch">
                                            <label>
                                                <span class="clr-green">Active</span>
                                                <input type="checkbox" onchange="ChangeJobStatus({{$Jobs->id}})" @if($Jobs->status == 1)
                                                    checked
                                                @else
                                                
                                                @endif>
                                                <span class="lever"></span>
                                                <span class="clr-red"> In Active</span>
                                            </label>
                                        </div></td>

                                <td><a href="/edit_jobs/{{$Jobs->id}}" target="_blank">Edit</a> </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('JSScript')
    <script>
         $(document).ready( function () {
            // $('#CandidateDataTable').DataTable();
            $('#JobsData').dataTable( {
    language: {
        searchPlaceholder: "Type Keywords"
    }
} );
        } );

    </script>
@endsection