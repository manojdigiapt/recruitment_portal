@extends('UI.base')

@section('Content')
<div class="container-fluid">
        

    
    <div class="row">
        <div class="col s12">
                    

            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title">Responsive Table</h5> -->
                    @if(Auth::guard('vendor')->check())
                     <a href="/add_candidate" class="btn waves-effect waves-light pull-right" type="submit" name="action">Add Candidate
                    </a> 
                    @endif

                    {{--  <div class="row">
                        <div class="input-field col s4 m6 l4">
                                <select id="cname">
                                    <option selected disabled>Select Date</option>
                                    <option value="">Last Hour</option>
                                    <option value="">Last 7 Days</option>
                                    <option value="">Last Month</option>
                                    <option value="">Last 6 Month</option>
                                </select>
                                <label>Select Date</label>
                        </div>
                    </div>  --}}

                    <table class="responsive-table" id="CandidateDataTable">
                        <thead>
                            <tr>
                                {{-- <th>Customer Name</th> --}}
                                @if(Auth::guard('vendor')->check())
                                <th><label>
                                        <input type="checkbox" id="CheckAll" value="1" id="round"/>
                                        <span></span>
                                    </label></th>
                                    @endif
                                <th>Candidate Name</th>
                                <th>Email</th>
                                <th>Job Title</th>
                                <th>Round 1</th>
                                <th>Round 2</th>
                                <th>Round 3</th>
                                <th>Round 4</th>
                                <th>Shortlisted Status</th>
                                @if(!(request()->is('AdminRejectedCandidateList')) ? 'active' : '')
                                <th>Action</th>
                                @endif
                            </tr>
                        </thead>
                        
                        <tbody id="CandidateDetails">
                            @foreach($GetCandidates as $Candidates)
                                <tr>
                                    <td>{{$Candidates->name}}</td>
                                    <td>{{$Candidates->email}}</td>
                                    <td>{{$Candidates->job_title}}</td>
                                    {{-- <td>{{$Candidates->mobile}}</td> --}}
                                    <td>@if($Candidates->Round1 == 1)
                                            Rejected
                                        @elseif($Candidates->Round1 == 2)
                                            Shortlisted
                                        @elseif($Candidates->Round1 == 3)
                                            On Hold
                                        @elseif($Candidates->Round1 == 4)
                                            Selected
                                        @endif</td>
                                    <td>@if($Candidates->Round2 == 1)
                                        Rejected
                                    @elseif($Candidates->Round2 == 2)
                                        Shortlisted
                                    @elseif($Candidates->Round2 == 3)
                                        On Hold
                                    @elseif($Candidates->Round2 == 4)
                                        Selected
                                    @endif</td>
                                    <td>@if($Candidates->Round3 == 1)
                                        Rejected
                                    @elseif($Candidates->Round3 == 2)
                                        Shortlisted
                                    @elseif($Candidates->Round3 == 3)
                                        On Hold
                                    @elseif($Candidates->Round3 == 4)
                                        Selected
                                    @endif</td>
                                    <td>@if($Candidates->Round4 == 1)
                                        Rejected
                                    @elseif($Candidates->Round4 == 2)
                                        Shortlisted
                                    @elseif($Candidates->Round4 == 3)
                                        On Hold
                                    @elseif($Candidates->Round4 == 4)
                                        Selected
                                    @endif</td>
                                
                                <td> @if($Candidates->shortlisted_status == 2)

                                    <span class="clr-red">Rejected</span>
                                    @elseif($Candidates->shortlisted_status == 1)
                                    <span class="clr-green">Shortlisted</span>
                                    @endif
                                </td>
                                @if(!(request()->is('AdminRejectedCandidateList')) ? 'active' : '')
                                    @if(Auth::guard('vendor')->check())
                                        <td><a href="/edit_candidate/{{$Candidates->id}}" target="_blank">Edit</a></td>
                                    @elseif(Auth::guard('super_admin')->check())
                                        <td><a href="/CandidateDetails/{{$Candidates->Id}}" target="_blank">View Details</a></td>
                                    @elseif(Auth::guard('master_admin')->check())
                                        <td><a href="/CandidateDetails/{{$Candidates->Id}}" target="_blank">View Details</a></td>
                                    @elseif(Auth::guard('sub_admin')->check())
                                        <td><a href="/CandidateDetails/{{$Candidates->Id}}" target="_blank">View Details</a></td>
                                    @endif
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if(Auth::guard('vendor')->check())
                    <a href="#JobDetails" class="waves-effect waves-light btn pull-right modal-trigger mar-top20" name="action">Add To Jobs
                    </a> 
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


<div id="JobDetails" class="modal">
        <div class="modal-content">
            <h4>Add Job Details</h4>
            <form>
                <div class="row">
                    <table class="responsive-table">
                        <thead>
                            <tr>
                                <th><label>
                                        <input type="checkbox" id="CheckAllJobs" value="1" id="round"/>
                                        <span></span>
                                    </label></th>
                                <th>Job Name</th>
                                <th>Qualification</th>
                                <th>Experience</th>
                                <th>Location</th>
                            </tr>
                        </thead>

                        <tbody id="JobDetailsTable">
                            @foreach($GetJobs as $Jobs)
                            <tr>
                                <td><p>
                                    <label>
                                        <input type="checkbox" class="CheckJobs" name="Jobs[]" value="{{$Jobs->id}}" id=""/>
                                        <span></span>
                                    </label>
                                </p></td>
                                <td><input type="hidden" name="" id="CustomerId" value="{{$Jobs->customer_id}}" id=""/> {{$Jobs->job_title}}</td>
                                <td>{{$Jobs->qualification}}</td>
                                <td>{{$Jobs->experience_from}} - {{$Jobs->experience_to}}</td>
                                <td>{{$Jobs->location}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <a href="javascript:void(0);" id="SubmitJobsList" class="btn waves-effect waves-light pull-right mar-top20" type="button" name="action">Submit
                    </a> 
                </div>
            </form>
        </div>
        {{-- <div class="modal-footer">
                <a href="#" class="waves-effect waves-green btn-flat">Agree</a>
            </div> --}}
        </div>

@endsection


@section('JSScript')
    <script>
        $(document).ready( function () {
            // $('#CandidateDataTable').DataTable();
            $('#CandidateDataTable').dataTable( {
    language: {
        searchPlaceholder: "Type Name, Email, Mobile, Skills"
    }
} );
        } );

    </script>
@endsection