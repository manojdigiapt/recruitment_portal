@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title activator">Form with placeholder<i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Get Code">more_vert</i></h5> -->
                    <form>
                        <div class="row">
                            <div class="input-field col s12 m6 l12">
                                <select id="vendor_type">
                                    <option selected>Vendor Type</option>
                                    <option value="1" @if($GetVendor->type == 1) selected @endif>Freelance consultant</option>
                                    <option value="2" @if($GetVendor->type == 2) selected @endif>Proprietorship</option>
                                    <option value="3" @if($GetVendor->type == 3) selected @endif>Partnership Firm</option>
                                    <option value="4" @if($GetVendor->type == 4) selected @endif>LLP</option>
                                    <option value="5" @if($GetVendor->type == 5) selected @endif>Pvt Ltd Company</option>
                                    <option value="6" @if($GetVendor->type == 6) selected @endif>Others</option>
                                </select>
                                <label>Vendor Type</label>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="input-field col s12 m6 l12">
                                    <select id="service_category">
                                        <option selected>Select Category</option>
                                        <option value="1" @if($GetVendor->service_category == 1) selected @endif>Technical Hiring</option>
                                        <option value="2" @if($GetVendor->service_category == 2) selected @endif>Non Technical Hiring</option>
                                    </select>
                                    <label>Service Category</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                            <input placeholder="" id="cname" value="{{$GetVendor->name}}" type="text">
                            
                            <input placeholder="" id="doc_id" value="{{$GetVendor->DocId}}" type="hidden">
                            <input placeholder="" id="Id" value="{{$GetVendor->id}}" type="hidden">
                            <label for="name2">Vendor Name</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="Password" value="@if(Auth::guard('super_admin')->check())@elseif(Auth::guard('master_admin')->check()) {{$GetVendor->password}} @endif" id="password" type="password" @if(Auth::guard('super_admin')->check()) @elseif(Auth::guard('master_admin')->check()) disabled @endif>
                                <label for="name2">Password</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" value="{{$GetVendor->address}}" id="address" type="text">
                                <label for="name2">Address</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" value="{{$GetVendor->contact}}" id="contact" type="text">
                                <label for="name2">Contact Person</label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" value="{{$GetVendor->email}}" id="email" type="text">
                                <label for="name2">Email</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" value="{{$GetVendor->mobile}}" id="mobile" type="text" maxlength="10">
                                <label for="name2">Mobile</label>
                            </div>
                        </div>

                        <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="" value="{{$GetVendor->pan_no}}" id="pan" type="text">
                                    <label for="name2">PAN Number</label>
                                </div>
                            </div>

                            <div class="row">
                                    <div class="input-field col s12">
                                        <input placeholder="" value="{{$GetVendor->gst}}" id="gst" type="text">
                                        <label for="name2">GST Number</label>
                                    </div>
                                </div>

                                <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="" value="{{$GetVendor->contract_tenure}}" id="tenure" type="text">
                                            <label for="name2">Contract tenure (months or years)</label>
                                        </div>
                                    </div>

                            

                            <div class="row">
                                    <div class="input-field col s12">
                                        <textarea placeholder="" id="payment" class="materialize-textarea">{{$GetVendor->payment_terms}}</textarea>
                                        <label for="message2">Payment terms</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s3">
                                        <label for="name2">PAN Copy (Pdf, Jpg, Jpeg, Png)</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input placeholder="" id="pan_copy" type="file" style="display:none;" accept="image/jpg,image/jpeg,image/png,application/pdf">

                                        <a class="waves-effect waves-light btn" id="click_pan_copy"><i class="material-icons right">file_upload</i>Click here to upload</a>
                                        
                                         <br/><span style="color:red">{{$GetVendor->pan_card}}</span>
                                    </div>

                                    <div class="input-field col s3">
                                        <label for="name2">Preview</label>
                                        <br>
                                        @php
                                if(isset($GetVendor->pan_card)){
                                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                                $type = finfo_file($finfo, "pan_copy/".$GetVendor->pan_card);
                                
                                if (isset($type) && in_array($type, array("image/png", "image/jpeg", "image/gif"))) {
                                    echo "<img src='/pan_copy/".$GetVendor->pan_card."'' class ='customers-agreement-width'>";
                                } else {
                                    echo "<iframe src='/pan_copy/".$GetVendor->pan_card."#toolbar=0' width='100%'' height='300px'>
    </iframe>";
                                }
                            }
                            @endphp
                                        {{--  <img src="{{URL::asset("pan_copy")}}/{{$GetVendor->pan_card}}" class ="vendor-preview-width" alt="">  --}}
                                    </div>
                                </div>

                                

                                    <div class="row">
                                            <div class="input-field col s3">
                                                <label for="name2">GST Certificate (Pdf, Jpg, Jpeg, Png)</label>
                                            </div>
        
                                            <div class="input-field col s6">
                                                    <input placeholder="" id="gst_certificate" type="file" style="display:none;" accept="image/jpg,image/jpeg,image/png,application/pdf">
                                                    
                                                    <a class="waves-effect waves-light btn" id="click_gst_certificate"><i class="material-icons right">file_upload</i>Click here to upload</a>

                                                    <br/><span style="color:red">{{$GetVendor->gst_certificate}}</span>
                                                </div>
                                                <div class="input-field col s3">
                                                    <label for="name2">Preview</label>
                                                    <br>
                                                    @php
                                if(isset($GetVendor->gst_certificate)){
                                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                                $type = finfo_file($finfo, "gst_certificate/".$GetVendor->gst_certificate);
                                
                                if (isset($type) && in_array($type, array("image/png", "image/jpeg", "image/gif"))) {
                                    echo "<img src='/gst_certificate/".$GetVendor->gst_certificate."'' class ='customers-agreement-width'>";
                                } else {
                                    echo "<iframe src='/gst_certificate/".$GetVendor->gst_certificate."#toolbar=0' width='100%'' height='300px'>
    </iframe>";
                                }
                            }
                            @endphp
                                                </div>
                                        </div>

                                        <div class="row">
                                                <div class="input-field col s3">
                                                    <label for="name2">Company Incorporation Certificate (Pdf, Jpg, Jpeg, Png)</label>
                                                </div>
            
                                                <div class="input-field col s6">
                                                        <input placeholder="" id="company_certificate" type="file" style="display:none; " accept="image/jpg,image/jpeg,image/png,application/pdf">

                                                        <a class="waves-effect waves-light btn" id="click_company_certificate"><i class="material-icons right">file_upload</i>Click here to upload</a>

                                                         <br/><span style="color:red">{{$GetVendor->incorporation_certificate}}</span>
                                                    </div>
                                                <div class="input-field col s3">
                                                    <label for="name2">Preview</label>
                                                    <br>
                                                    <br>
                                                    @php
                                if(isset($GetVendor->incorporation_certificate)){
                                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                                $type = finfo_file($finfo, "company_certificate/".$GetVendor->incorporation_certificate);
                                
                                if (isset($type) && in_array($type, array("image/png", "image/jpeg", "image/gif"))) {
                                    echo "<img src='/company_certificate/".$GetVendor->incorporation_certificate."'' class ='customers-agreement-width'>";
                                } else {
                                    echo "<iframe src='/company_certificate/".$GetVendor->incorporation_certificate."#toolbar=0' width='100%'' height='300px'>
    </iframe>";
                                }
                            }
                            @endphp
                                                    {{--  <img src="{{URL::asset("company_certificate")}}/{{$GetVendor->incorporation_certificate}}" class ="vendor-preview-width" alt="">  --}}
                                                </div>
                                            </div>

                                            <div class="row">
                                                    <div class="input-field col s3">
                                                        <label for="name2">Upload agreement (Pdf, Jpg, Jpeg, Png)</label>
                                                    </div>
                
                                                    <div class="input-field col s6">
                                                            <input placeholder="" id="agreement" type="file" style="display:none;" accept="image/jpg,image/jpeg,image/png,application/pdf">

                                                            <a class="waves-effect waves-light btn" id="click_agreement"><i class="material-icons right">file_upload</i>Click here to upload</a>
                                                            
                                                            <br/><span style="color:red">{{$GetVendor->agreement}}</span>
                                                    </div>

                                                    <div class="input-field col s3">
                                                        <label for="name2">Preview</label>
                                                        <br>
                                                        <br>
                                                        @php
                                if(isset($GetVendor->agreement)){
                                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                                $type = finfo_file($finfo, "agreement/".$GetVendor->agreement);
                                
                                if (isset($type) && in_array($type, array("image/png", "image/jpeg", "image/gif"))) {
                                    echo "<img src='/agreement/".$GetVendor->agreement."'' class ='customers-agreement-width'>";
                                } else {
                                    echo "<iframe src='/agreement/".$GetVendor->agreement."#toolbar=0' width='100%'' height='300px'>
    </iframe>";
                                }
                            }
                            @endphp

                                                        {{--  <h3><a href="/agreement/{{$GetVendor->agreement}}" target="_blank">click here</a></h3>  --}}
                                                        
                                                    </div>

                                                </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="button" id="UpdateVendor" name="action">Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Code<i class="material-icons right">close</i></span>
                                                    <pre class="pre-scroll">                                                            <code class="language-markup">
                                                                &lt;form&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;&quot; id=&quot;name2&quot; type=&quot;text&quot;&gt;
                                                                            &lt;label for=&quot;name2&quot;&gt;Name&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;<a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="553f3a3d3b15313a38343c3b3b3438307b363a38">[email&#160;protected]</a>&quot; id=&quot;email2&quot; type=&quot;email&quot;&gt;
                                                                            &lt;label for=&quot;email2&quot;&gt;Email&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;YourPassword&quot; id=&quot;password2&quot; type=&quot;password&quot;&gt;
                                                                            &lt;label for=&quot;password2&quot;&gt;Password&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;textarea placeholder=&quot;Oh WoW! Let me check this one too.&quot; id=&quot;message2&quot; class=&quot;materialize-textarea&quot;&gt;&lt;/textarea&gt;
                                                                            &lt;label for=&quot;message2&quot;&gt;Message&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;button class=&quot;btn cyan waves-effect waves-light right&quot; type=&quot;submit&quot; name=&quot;action&quot;&gt;Submit
                                                                            &lt;/button&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                &lt;/form&gt;
                                                            </code>
                    </pre>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection