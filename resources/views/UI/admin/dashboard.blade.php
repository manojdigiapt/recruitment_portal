@extends('UI.base')
@section('Content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Sales Summery -->
    <!-- ============================================================== -->
    <div class="row">
      <!-- col -->
      <div class="col m6 s12 l3">
        <div class="card">
          <div class="p-15">
            <div class="d-flex no-block align-items-center">
              <div class="m-r-10 blue-text text-accent-4">
                <i class="material-icons display-5">account_box</i>
              </div>
              <div>
                <span>Total Customers</span>
              <h4 class="font-medium m-b-0">{{count($Customers)}}</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- col -->
      <!-- col -->
      <div class="col m6 s12 l3">
        <div class="card">
          <div class="p-15">
            <div class="d-flex no-block align-items-center">
              <div class="m-r-10 light-blue-text">
                <i class="material-icons display-5">work</i>
              </div>
              <div>
                <span>Total Jobs</span>
                <h4 class="font-medium m-b-0">{{count($Jobs)}}</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- col -->
      <!-- col -->
      <div class="col m6 s12 l3">
        <div class="card">
          <div class="p-15">
            <div class="d-flex no-block align-items-center">
              <div class="m-r-10 orange-text text-darken-2 text-darken-1">
                <i class="material-icons display-5">supervised_user_circle</i>
              </div>
              <div>
                <span>Total Vendor</span>
                <h4 class="font-medium m-b-0">{{count($Vendor)}}</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- col -->
      <!-- col -->
      <div class="col m6 s12 l3">
        <div class="card">
          <div class="p-15">
            <div class="d-flex no-block align-items-center">
              <div class="m-r-10 deep-purple-text text-accent-2">
                <i class="material-icons display-5">account_circle</i>
              </div>
              <div>
                <span>Total Candidates</span>
                <h4 class="font-medium m-b-0">{{count($Candidates)}}</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- col -->
    </div>
    <!-- ============================================================== -->
    <!-- Sales Summery -->
    <!-- ============================================================== -->
    {{-- <div class="row">
      <div class="col s12 l6">
        <div class="card">
          <div class="card-content">
            <div class="d-flex align-items-center">
              <div>
                <h5 class="card-title">Sales Summary</h5>
                <h6 class="card-subtitle">Overview of Latest Month</h6>
              </div>
              <div class="ml-auto">
                <div class="input-field dl support-select">
                  <select>
                    <option value="0" selected>10 Mar - 10 Apr</option>
                    <option value="1">10 Apr - 10 May</option>
                    <option value="2">10 May - 10 Jun</option>
                    <option value="3">10 Jun - 10 Jul</option>
                  </select>
                </div>
              </div>
            </div>
            <!-- Sales Summery -->
            <div class="p-t-20">
              <div class="row">
                <div class="col m12">
                  <div class="campaign ct-charts" style="height: 275px;"></div>
                </div>
                <div class="center-align">
                  <ul class="list-inline font-12 dl m-r-10">
                    <li class="light-blue-text"><i class="fa fa-circle"></i> Earnings</li>
                    <li class="blue-text text-accent-4"><i class="fa fa-circle"></i> Sales</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col s12 l6">
        <div class="row">
          <div class="col s12">
            <div class="card order-widget">
              <div class="card-content">
                <div class="row">
                  <!-- column -->
                  <div class="col s12 m8">
                    <h5 class="card-title">Order Status</h5>
                    <h6 class="card-subtitle m-b-0">Total Earnings of the Month</h6>
                    <ul class="order list-inline m-t-20">
                      <li class="b-r order-category">
                        <i class="fa fa-circle m-r-5 light-blue-text font-10"></i>
                        <h4 class="font-bold font-20 m-b-0 m-t-5">5489</h4>
                        <h6 class="font-14 font-light">Success</h6>
                      </li>
                      <li class="b-r order-category">
                        <i class="fa fa-circle m-r-5 blue-text font-10"></i>
                        <h4 class="font-bold font-20 m-b-0 m-t-5">954</h4>
                        <h6 class="font-14 font-light">Pending</h6>
                      </li>
                      <li class="order-category">
                        <i class="fa fa-circle m-r-5 orange-text text-darken-2 font-10"></i>
                        <h4 class="font-bold font-20 m-b-0 m-t-5">736</h4>
                        <h6 class="font-14 font-light">Failed</h6>
                      </li>
                    </ul>
                  </div>
                  <!-- column -->
                  <div class="col s12 m4">
                    <div id="visitor" class="m-t-20 chart-align" style="height:150px; width: 120px;"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- column -->
          <div class="col m12 s12 l6">
            <div class="card light-blue">
              <div class="card-content">
                <h5 class="card-title white-text">Revenue Statistics</h5>
                <h4 class="font-medium white-text m-b-0"><i class="ti-arrow-up"></i>$351 <span class="font-12 white-text op-5">Jan 10  - Jan  20</span></h4>
                <div class="m-t-20 center-align">
                  <div id="revenue"></div>
                </div>
              </div>
            </div>
          </div>
          <!-- column -->
          <div class="col m12 s12 l6">
            <div class="card blue accent-4">
              <div class="card-content">
                <h5 class="card-title white-text">Page Views</h5>
                <div class="d-flex no-block align-items-center">
                  <div>
                    <h4 class="white-text m-b-0"><i class="ti-arrow-up"></i> 6548</h4>
                  </div>
                  <div class="ml-auto">
                    <ul class="m-b-0">
                      <li class="white-text font-12"><i class="fa fa-circle m-r-5 white-text font-12 op-3"></i> Visit </li>
                      <li class="white-text font-12"><i class="fa fa-circle m-r-5 white-text text-accent-4 font-12"></i> Page Views </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="m-t-20" id="views"></div>
            </div>
          </div>
        </div>
      </div>
    </div> --}}

    <div class="row">
      <div class="col s12 l12">
        <div class="card">
          <div class="card-content">
            <div class="d-flex align-items-center">
              <div>
                <h5 class="card-title">Customers</h5>
                {{--  <h6 class="card-subtitle">Sales on products we have</h6>  --}}
              </div>
            </div>
            <div class="table-responsive m-b-20 m-t-15">
              <table class="">
                <thead>
                  <tr>
                    <th>Customer Name</th>
                    <th>Email</th>
                    <th>Contact Person Name</th>
                    <th>Mobile</th>
                    <th>Customer Address</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($GetLastRegisteredCustomers as $Customers)
                  <tr>
                    <td>{{$Customers->name}}</td>
                    <td>{{$Customers->email}}</td>
                    <td>{{$Customers->contact}}</td>
                    <td>{{$Customers->mobile}}</td>
                    <td>{{$Customers->address}}</td>
                    <td><div class="switch" style="
                        width: 170px;
                    ">
                        <label>
                            <span class="clr-green">Active</span>
                            <input type="checkbox" onchange="ChangeStatus({{$Customers->id}})" @if($Customers->status == 1)
                                checked
                            @else
                            
                            @endif>
                            <span class="lever"></span>
                            <span class="clr-red">In Active</span>
                        </label>
                    </div></td>
                    
                </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <a href="/customers" target="_blank"><i class="fa fa-angle-right"></i> View All Customers</a>
          </div>
        </div>
      </div>
      <div class="col s12 l12">
        <div class="card">
          <div class="card-content">
            <div class="d-flex align-items-center">
              <div>
                <h5 class="card-title">Jobs </h5>
              </div>
              {{--  <div class="ml-auto">
                <div class="input-field dl support-select">
                  <select>
                    <option value="0" selected>10 Mar - 10 Apr</option>
                    <option value="1">10 Apr - 10 May</option>
                    <option value="2">10 May - 10 Jun</option>
                    <option value="3">10 Jun - 10 Jul</option>
                  </select>
                </div>
              </div>  --}}
            </div>
            <div class="table-responsive m-b-20">
              <table class="">
                <thead>
                  <tr>
                    <th>Customer Name</th>
                    <th>Vendor Name</th>
                    <th>Job Title</th>
                    <th>Qualification</th>
                    <th>Total Experience</th>
                    <th>Job Location</th>
                    <th>Notice Period</th>
                    <th>Annual CTC Budget</th>
                    <th>Gender</th>
                    <th>Primary Skills</th>
                    
                    {{-- <th>Job Location</th> --}}
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($GetLastAddedJobs as $Jobs)
                    <tr>
                      <td>{{$Jobs->name}}</td>
                      <td>{{$Jobs->VendorName}}</td>
                      <td>{{$Jobs->job_title}}</td>
                      <td>{{$Jobs->qualification}}</td>
                      <td>{{$Jobs->experience_from}} Years - {{$Jobs->experience_to}} Months</td>
                      <td>{{$Jobs->location}}</td>
                      <td>{{$Jobs->notice_period}}</td>
                      <td>{{$Jobs->budget_from}}</td>
                      
                      <td>@if($Jobs->gender == 1)
                          Any
                      @elseif($Jobs->gender == 2)
                          Male
                  @elseif($Jobs->gender == 3)
                          Female
              @endif</td>
                      <td>{{$Jobs->details}}</td>
                      {{-- <td>{{$Jobs->location}}</td> --}}
                      <td><div class="switch">
                              <label>
                                  <span class="clr-green">Active</span>
                                  <input type="checkbox" onchange="ChangeJobStatus({{$Jobs->id}})" @if($Jobs->status == 1)
                                      checked
                                  @else
                                  
                                  @endif>
                                  <span class="lever"></span>
                                  <span class="clr-red"> In Active</span>
                              </label>
                          </div></td>
                  </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <a href="/jobs" target="_blank"><i class="fa fa-angle-right"></i> View All Jobs</a>
          </div>
        </div></div>
      </div>

      <div class="row">
          <div class="col s12 l12">
            <div class="card">
              <div class="card-content">
                <div class="d-flex align-items-center">
                  <div>
                    <h5 class="card-title">Vendors</h5>
                    {{--  <h6 class="card-subtitle">Sales on products we have</h6>  --}}
                  </div>
                </div>
                <div class="table-responsive m-b-20 m-t-15">
                  <table class="">
                    <thead>
                      <tr>
                        <th>Service Category</th>
                        <th>Vendor Name</th>
                        <th>Vendor Address</th>
                        <th>Contact Person Name</th>
                        <th>Contact Person Email</th>
                        <th>Contact Person Mobile</th>
                        <th>PAN NO</th>
                        <th>GST</th>
                        <th>Contract Tenure</th>
                        <th>Payment terms</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($GetVendors as $Vendors)
                      <tr>
                        <td>@if($Vendors->service_category == 1)
                                Technical Hiring
                            @elseif($Vendors->service_category == 2)
                                Non Technical Hiring
                            @endif
                        </td>
                        <td>{{$Vendors->name}}</td>
                        <td>{{$Vendors->address}}</td>
                        <td>{{$Vendors->contact}}</td>
                        <td>{{$Vendors->email}}</td>
                        <td>{{$Vendors->mobile}}</td>
                        <td>{{$Vendors->pan_no}}</td>
                        <td>{{$Vendors->gst}}</td>
                        <td>{{$Vendors->contract_tenure}}</td>
                        <td>{{$Vendors->payment_terms}}</td>
                        <td><div class="switch" style="
                            width: 170px;
                        ">
                                <label>
                                    <span class="clr-green">Active</span>
                                    <input type="checkbox" onchange="ChangeVendorStatus({{$Vendors->id}})" @if($Vendors->status == 1)
                                        checked
                                    @else
                                    
                                    @endif>
                                    <span class="lever"></span>
                                    <span class="clr-red">In Active</span>
                                </label>
                            </div></td>

                    </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <a href="/vendor" target="_blank"><i class="fa fa-angle-right"></i> View All Vendors</a>
              </div>
            </div>
          </div>
          <div class="col s12 l12">
            <div class="card">
              <div class="card-content">
                <div class="d-flex align-items-center">
                  <div>
                    <h5 class="card-title">Shortlisted Candidates </h5>
                  </div>
                  {{--  <div class="ml-auto">
                    <div class="input-field dl support-select">
                      <select>
                        <option value="0" selected>10 Mar - 10 Apr</option>
                        <option value="1">10 Apr - 10 May</option>
                        <option value="2">10 May - 10 Jun</option>
                        <option value="3">10 Jun - 10 Jul</option>
                      </select>
                    </div>
                  </div>  --}}
                </div>
                
               <div class="table-responsive m-b-20">
                <table class="responsive-table" id="CandidateDataTable">
                  <thead>
                      <tr>
                        <th>Candidate Name</th>
                        <th>Email</th>
                        <th>Job Title</th>
                        <th>Round 1</th>
                        <th>Round 2</th>
                        <th>Round 3</th>
                        <th>Round 4</th>
                        <th>Shortlisted Status</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  
                  <tbody id="CandidateDetails">
                      @foreach($GetCandidates as $Candidates)
                      <tr>
                        <tr>
                          <td>{{$Candidates->name}}</td>
                          <td>{{$Candidates->email}}</td>
                          <td>{{$Candidates->job_title}}</td>
                          {{-- <td>{{$Candidates->mobile}}</td> --}}
                          <td>@if($Candidates->Round1 == 1)
                                  Rejected
                              @elseif($Candidates->Round1 == 2)
                                  Shortlisted
                              @elseif($Candidates->Round1 == 3)
                                  On Hold
                              @elseif($Candidates->Round1 == 4)
                                  Selected
                              @endif</td>
                          <td>@if($Candidates->Round2 == 1)
                              Rejected
                          @elseif($Candidates->Round2 == 2)
                              Shortlisted
                          @elseif($Candidates->Round2 == 3)
                              On Hold
                          @elseif($Candidates->Round2 == 4)
                              Selected
                          @endif</td>
                          <td>@if($Candidates->Round3 == 1)
                              Rejected
                          @elseif($Candidates->Round3 == 2)
                              Shortlisted
                          @elseif($Candidates->Round3 == 3)
                              On Hold
                          @elseif($Candidates->Round3 == 4)
                              Selected
                          @endif</td>
                          <td>@if($Candidates->Round4 == 1)
                              Rejected
                          @elseif($Candidates->Round4 == 2)
                              Shortlisted
                          @elseif($Candidates->Round4 == 3)
                              On Hold
                          @elseif($Candidates->Round4 == 4)
                              Selected
                          @endif</td>
                      
                      <td> @if($Candidates->shortlisted_status == 2)

                          <span class="clr-red">Rejected</span>
                          @elseif($Candidates->shortlisted_status == 1)
                          <span class="clr-green">Shortlisted</span>
                          @endif
                      </td>
                          
                      {{-- @if(Auth::guard('vendor')->check())
                                    <td><a href="/edit_candidate/{{$Candidates->id}}" target="_blank">Edit</a></td>
                                @elseif(Auth::guard('admin')->check())
                                    <td><a href="/CandidateDetails/{{$Candidates->Id}}" target="_blank">View Details</a></td>
                                @elseif(Auth::guard('sub_admin')->check())
                                    <td><a href="/CandidateDetails/{{$Candidates->Id}}" target="_blank">View Details</a></td>
                                @endif --}}
                        <td>
                          @if(Auth::guard('vendor')->check())
                              <a href="/ModifyCandidateDetails/{{$Candidates->Id}}" target="_blank">Modify Status</a></td>
                          @elseif(Auth::guard('super_admin')->check())
                              @if($Candidates->shortlisted_status != 2)
                                  <a href="/CandidateDetails/{{$Candidates->Id}}" target="_blank">View Details</a></td>
                              @endif
                          @elseif(Auth::guard('master_admin')->check())
                              @if($Candidates->shortlisted_status != 2)
                                  <a href="/CandidateDetails/{{$Candidates->Id}}" target="_blank">View Details</a></td>
                              @endif
                          @endif
                      </tr>
                      @endforeach
                  </tbody>
              </table>
                </div>  
                <a href="/AdminCandidateList" target="_blank"><i class="fa fa-angle-right"></i> View All Shortlisted Candidates</a>
              </div>
            </div></div>
          </div>

      
    <!-- Two Tables -->
    {{-- <div class="row">
      <div class="col s12 l6">
        <div class="card">
          <div class="card-content">
            <div class="d-flex align-items-center">
              <div>
                <h5 class="card-title">Recent Sales</h5>
                <h6 class="card-subtitle">Sales on products we have</h6>
              </div>
              <div class="ml-auto">
                <div class="input-field dl support-select">
                  <select>
                    <option value="0" selected>10 Mar - 10 Apr</option>
                    <option value="1">10 Apr - 10 May</option>
                    <option value="2">10 May - 10 Jun</option>
                    <option value="3">10 Jun - 10 Jul</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="table-responsive m-b-20 m-t-15">
              <table class="">
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Price</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <p class="">Elite Admin</p>
                    </td>
                    <td><span class="label label-info">Sale</span></td>
                    <td>May 23, 2018</td>
                    <td class="blue-grey-text text-darken-4 font-medium">$96K</td>
                  </tr>
                  <tr>
                    <td>
                      <p class="">Real Homes WP Theme</p>
                    </td>
                    <td><span class="label cyan">Extended</span></td>
                    <td>May 23, 2018</td>
                    <td class="blue-grey-text text-darken-4 font-medium">$85K</td>
                  </tr>
                  <tr>
                    <td>
                      <p class="">MedicalPro WP Theme</p>
                    </td>
                    <td><span class="label label-primary">Multiple</span></td>
                    <td>May 23, 2018</td>
                    <td class="blue-grey-text text-darken-4 font-medium">$81K</td>
                  </tr>
                  <tr>
                    <td>
                      <p class="">HostinPress Html</p>
                    </td>
                    <td><span class="label label-warning">Tax</span></td>
                    <td>May 23, 2018</td>
                    <td class="blue-grey-text text-darken-4 font-medium">-$30K</td>
                  </tr>
                  <tr>
                    <td>
                      <p class="">Materialx Admin</p>
                    </td>
                    <td><span class="label label-info">Sale</span></td>
                    <td>May 23, 2018</td>
                    <td class="blue-grey-text text-darken-4 font-medium">$80K</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <a href="javascript: void(0)"><i class="fas fa-angle-right"></i> View Complete Report</a>
          </div>
        </div>
      </div>
      <div class="col s12 l6">
        <div class="card">
          <div class="card-content">
            <div class="d-flex align-items-center">
              <div>
                <h5 class="card-title">Sales Executives</h5>
                <h6 class="card-subtitle">Sales on products we have</h6>
              </div>
              <div class="ml-auto">
                <div class="input-field dl support-select">
                  <select>
                    <option value="0" selected>10 Mar - 10 Apr</option>
                    <option value="1">10 Apr - 10 May</option>
                    <option value="2">10 May - 10 Jun</option>
                    <option value="3">10 Jun - 10 Jul</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="table-responsive m-b-20">
              <table class="">
                <thead>
                  <tr>
                    <th>Executives</th>
                    <th>Progress</th>
                    <th>Sales</th>
                    <th>Earned</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="d-flex no-block align-items-center">
                        <div class="m-r-10">
                          <img src="{{URL::asset('UI/assets/images/users/d1.jpg')}}" alt="user" class="circle" width="45" />
                        </div>
                        <div class="">
                          <h5 class="m-b-0 font-16 font-medium">Hanna Gover</h5><span><a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="d8b0bfb7aebdaa98bfb5b9b1b4f6bbb7b5">[email&#160;protected]</a></span>
                        </div>
                      </div>
                    </td>
                    <td class="green-text"><i class="fa fa-arrow-up"></i> 23%</td>
                    <td>2356</td>
                    <td class="blue-grey-text  text-darken-4 font-medium">$96K</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="d-flex no-block align-items-center">
                        <div class="m-r-10"><img src="{{URL::asset('UI/assets/images/users/d2.jpg')}}" alt="user" class="circle" width="45" /></div>
                        <div class="">
                          <h5 class="m-b-0 font-16 font-medium">Daniel Kristeen</h5><span><a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="abe0d9c2d8dfcecec5ebccc6cac2c785c8c4c6">[email&#160;protected]</a></span>
                        </div>
                      </div>
                    </td>
                    <td class="green-text"><i class="fa fa-arrow-up"></i> 10%</td>
                    <td>2198</td>
                    <td class="blue-grey-text  text-darken-4 font-medium">$85K</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="d-flex no-block align-items-center">
                        <div class="m-r-10"><img src="{{URL::asset('UI/assets/images/users/d3.jpg')}}" alt="user" class="circle" width="45" /></div>
                        <div class="">
                          <h5 class="m-b-0 font-16 font-medium">Julian Josephs</h5><span><a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="e9a3869a8c99819aa98e84888085c78a8684">[email&#160;protected]</a></span>
                        </div>
                      </div>
                    </td>
                    <td class="orange-text"><i class="fa fa-arrow-up"></i> 45%</td>
                    <td>2198</td>
                    <td class="blue-grey-text  text-darken-4 font-medium">$45k</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="d-flex no-block align-items-center">
                        <div class="m-r-10"><img src="{{URL::asset('UI/assets/images/users/2.jpg')}}" alt="user" class="circle" width="45" /></div>
                        <div class="">
                          <h5 class="m-b-0 font-16 font-medium">Jan Petrovic</h5><span><a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="2149464e57445361464c40484d0f424e4c">[email&#160;protected]</a></span>
                        </div>
                      </div>
                    </td>
                    <td class="green-text"><i class="fa fa-arrow-up"></i> 45%</td>
                    <td>2198</td>
                    <td class="blue-grey-text  text-darken-4 font-medium">$85K</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <a href="javascript: void(0)"><i class="fas fa-angle-right"></i> View Complete Report</a>
          </div>
        </div></div>
      </div>
      <!-- Charts  -->
      {{--  <div class="row">
        <div class="col m12 s12 l8">
          <div class="card">
            <div class="card-content">
              <div class="d-flex align-items-center">
                <div>
                  <h5 class="card-title">Sales Overview</h5>
                  <h6 class="card-subtitle">Sales on products we have</h6>
                </div>
                <div class="ml-auto">
                  <ul class="list-inline font-12 dl m-r-10">
                    <li class="blue-text text-accent-4"><i class="fa fa-circle"></i> Ample</li>
                    <li class="light-blue-text"><i class="fa fa-circle"></i> Pixel Admin</li>
                  </ul>
                </div>
              </div>
              <div class="product-sales" style="height:335px; width:100%;"></div>
            </div>
          </div>
        </div>
        <div class="col m12 s12 l4">
          <div class="card">
            <div class="card-content">
              <div class="center-align">
                <i class="material-icons display-5 light-blue-text db">insert_chart</i>
                <span class="display-4 db blue-grey-text font-medium text-darken-3">368</span>
                <span>Active Visitors on Site</span>
                <!-- Progress -->
                <div class="progress m-t-30">
                  <div class="determinate yellow" style="width:100%"></div>
                  <div class="determinate orange" style="width: 40%"></div>
                  <div class="determinate blue accent-4" style="width:12%"></div>
                </div>
                <!-- Progress -->
                <!-- row -->
                <div class="row m-t-30 m-b-20">
                  <!-- column -->
                  <div class="col s4 b-r left-align">
                  <h4 class="m-b-0 font-medium">60%</h4>Desktop</div>
                  <!-- column -->
                  <div class="col s4 b-r center-align">
                  <h4 class="m-b-0 font-medium">28%</h4>Mobile</div>
                  <!-- column -->
                  <div class="col s4 right-align">
                  <h4 class="m-b-0 font-medium">12%</h4>Tablet</div>
                </div>
                <a class="waves-effect waves-light m-t-20 btn-large blue accent-4 m-b-20">View More Details</a>
              </div>
            </div>
          </div>
        </div>
      </div>  --}}
      <!-- Comments & Chats -->
      {{--  <div class="row">
        <div class="col s12 m12 l6">
          <div class="card">
            <div class="card-content">
              <h5 class="card-title">Recent Comments</h5>
              <div class="comment-widgets scrollable" style="height:560px;">
                <!-- Comment Row -->
                <div class="d-flex flex-row comment-row">
                  <div class="p-2"><img src="{{URL::asset('UI/assets/images/users/1.jpg')}}" alt="user" width="50" class="circle"></div>
                  <div class="comment-text w-100">
                    <h6 class="font-medium">James Anderson</h6>
                    <span class="m-b-15 db">Lorem Ipsum is simply dummy text of the printing and type setting industry. </span>
                    <div class="comment-footer">
                      <span class="text-muted right">April 14, 2016</span> <span class="label label-info">Pending</span> <span class="action-icons">
                      <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                      <a href="javascript:void(0)"><i class="ti-check"></i></a>
                      <a href="javascript:void(0)"><i class="ti-heart"></i></a>
                    </span> </div>
                  </div>
                </div>
                <!-- Comment Row -->
                <div class="d-flex flex-row comment-row">
                  <div class="p-2"><img src="{{URL::asset('UI/assets/images/users/4.jpg')}}" alt="user" width="50" class="circle"></div>
                  <div class="comment-text active w-100">
                    <h6 class="font-medium">Michael Jorden</h6>
                    <span class="m-b-15 db">Lorem Ipsum is simply dummy text of the printing and type setting industry. </span>
                    <div class="comment-footer ">
                      <span class="text-muted right">April 14, 2016</span>
                      <span class="label label-success">Approved</span>
                      <span class="action-icons active">
                        <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                        <a href="javascript:void(0)"><i class="icon-close"></i></a>
                        <a href="javascript:void(0)"><i class="ti-heart text-danger"></i></a>
                      </span>
                    </div>
                  </div>
                </div>
                <!-- Comment Row -->
                <div class="d-flex flex-row comment-row">
                  <div class="p-2"><img src="{{URL::asset('UI/assets/images/users/5.jpg')}}" alt="user" width="50" class="circle"></div>
                  <div class="comment-text w-100">
                    <h6 class="font-medium">Johnathan Doeting</h6>
                    <span class="m-b-15 db">Lorem Ipsum is simply dummy text of the printing and type setting industry. </span>
                    <div class="comment-footer">
                      <span class="text-muted right">April 14, 2016</span>
                      <span class="label label-warning">Rejected</span>
                      <span class="action-icons">
                        <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                        <a href="javascript:void(0)"><i class="ti-check"></i></a>
                        <a href="javascript:void(0)"><i class="ti-heart"></i></a>
                      </span>
                    </div>
                  </div>
                </div>
                <!-- Comment Row -->
                <div class="d-flex flex-row comment-row">
                  <div class="p-2"><img src="{{URL::asset('UI/assets/images/users/1.jpg')}}" alt="user" width="50" class="circle"></div>
                  <div class="comment-text w-100">
                    <h6 class="font-medium">James Anderson</h6>
                    <span class="m-b-15 db">Lorem Ipsum is simply dummy text of the printing and type setting industry. </span>
                    <div class="comment-footer">
                      <span class="text-muted right">April 14, 2016</span> <span class="label label-info">Pending</span> <span class="action-icons">
                      <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                      <a href="javascript:void(0)"><i class="ti-check"></i></a>
                      <a href="javascript:void(0)"><i class="ti-heart"></i></a>
                    </span> </div>
                  </div>
                </div>
                <!-- Comment Row -->
                <!-- Comment Row -->
                <div class="d-flex flex-row comment-row">
                  <div class="p-2"><img src="{{URL::asset('UI/assets/images/users/4.jpg')}}" alt="user" width="50" class="circle"></div>
                  <div class="comment-text active w-100">
                    <h6 class="font-medium">Michael Jorden</h6>
                    <span class="m-b-15 db">Lorem Ipsum is simply dummy text of the printing and type setting industry. </span>
                    <div class="comment-footer ">
                      <span class="text-muted right">April 14, 2016</span>
                      <span class="label label-success">Approved</span>
                      <span class="action-icons active">
                        <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                        <a href="javascript:void(0)"><i class="icon-close"></i></a>
                        <a href="javascript:void(0)"><i class="ti-heart text-danger"></i></a>
                      </span>
                    </div>
                  </div>
                </div>
                <!-- Comment Row -->
              </div>
            </div>
          </div>
        </div>
        <div class="col s12 m12 l6">
          <div class="card">
            <div class="card-content">
              <h5 class="card-title">Recent Chats</h5>
              <div class="chat-box scrollable" style="height:480px;">
                <!--chat Row -->
                <ul class="chat-list">
                  <!--chat Row -->
                  <li>
                    <div class="chat-img"><img src="{{URL::asset('UI/assets/images/users/1.jpg')}}" alt="user"></div>
                    <div class="chat-content">
                      <h6 class="font-medium">James Anderson</h6>
                      <div class="box bg-light-info">Lorem Ipsum is simply dummy text of the printing &amp; type setting industry.</div>
                    </div>
                    <div class="chat-time">10:56 am</div>
                  </li>
                  <!--chat Row -->
                  <li>
                    <div class="chat-img"><img src="{{URL::asset('UI/assets/images/users/2.jpg')}}" alt="user"></div>
                    <div class="chat-content">
                      <h6 class="font-medium">Bianca Doe</h6>
                      <div class="box bg-light-info">It’s Great opportunity to work.</div>
                    </div>
                    <div class="chat-time">10:57 am</div>
                  </li>
                  <!--chat Row -->
                  <li class="odd">
                    <div class="chat-content">
                      <div class="box bg-light-inverse">I would love to join the team.</div>
                      <br>
                    </div>
                  </li>
                  <!--chat Row -->
                  <li class="odd">
                    <div class="chat-content">
                      <div class="box bg-light-inverse">Whats budget of the new project.</div>
                      <br>
                    </div>
                    <div class="chat-time">10:59 am</div>
                  </li>
                  <!--chat Row -->
                  <li>
                    <div class="chat-img"><img src="{{URL::asset('UI/assets/images/users/3.jpg')}}" alt="user"></div>
                    <div class="chat-content">
                      <h6 class="font-medium">Angelina Rhodes</h6>
                      <div class="box bg-light-info">Well we have good budget for the project</div>
                    </div>
                    <div class="chat-time">11:00 am</div>
                  </li>
                  <!--chat Row -->
                </ul>
              </div>
            </div>
            <div class="card-action">
              <div class="row">
                <div class="col s8">
                  <div class="input-field m-t-0 m-b-0">
                    <textarea id="textarea1" class="materialize-textarea b-0"></textarea>
                  </div>
                </div>
                <div class="col s4">
                  <a class="btn-floating btn-large cyan pulse right"><i class="fas fa-paper-plane"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  
    </div> --}}
    <!-- ============================================================== -->
    <!-- Container fluid scss in scafholding.scss -->
    <!-- ============================================================== -->
    
@endsection