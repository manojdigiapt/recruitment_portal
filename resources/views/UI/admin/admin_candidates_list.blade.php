@extends('UI.base')

@section('Content')
<div class="container-fluid">
        

    
    <div class="row">
        <div class="col s12">
                    

            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title">Responsive Table</h5> -->

                    {{--  <div class="row">
                        <div class="input-field col s4 m6 l4">
                                <select id="cname">
                                    <option selected disabled>Select Date</option>
                                    <option value="">Last Hour</option>
                                    <option value="">Last 7 Days</option>
                                    <option value="">Last Month</option>
                                    <option value="">Last 6 Month</option>
                                </select>
                                <label>Select Date</label>
                        </div>
                    </div>  --}}

                    <table class="responsive-table" id="CandidateDataTable">
                        <thead>
                            <tr>
                                {{-- <th>Customer Name</th> --}}
                                
                                <th>Candidate Name</th>
                                <th>Current Company</th>
                                <th>Current CTC</th>
                                <th>Notice Period</th>
                                <th>Primary Skills</th>
                                <th>View Resume</th>
                                <th>Shortlisted Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody id="CandidateDetails">
                            @foreach($GetCandidates as $Candidates)
                            <tr>
                                <td>{{$Candidates->name}}</td>
                                <td>{{$Candidates->current_company}}</td>
                                <td>{{$Candidates->current_ctc}}</td>
                                <td>{{$Candidates->notice_period}}</td>
                                <td>{{$Candidates->skills}}</td>
                                <td><a href='resume/{{$Candidates->resume}}' target='_blank'>View Resume</a></td>
                                <td>@if($Candidates->shortlisted_status == 1)
                                    <span class="clr-green">Shortlisted</span>
                                    @elseif($Candidates->shortlisted_status == 2)
                                    <span class="clr-red">Rejected</span>
                                    @endif</td>
                                <td>
                                    {{-- @if($Candidates->shortlisted_status == 1)
                                        <button class="btn waves-effect waves-light red" type="button" id="ShortlistBtn" onclick="ChangeShortlistedStatus({{$Candidates->Id}})" name="action">Remove Shortlist
                                        <i class="material-icons right">send</i>
                                        </button>
                                    @else
                                        <button class="btn waves-effect waves-light" type="button" id="ShortlistBtn" onclick="ChangeShortlistedStatus({{$Candidates->Id}})" name="action">Shortlist
                                            <i class="material-icons right">send</i>
                                            </button>
                                    @endif --}}
                                
                                    <p>
                                        <label>
                                            <input class="with-gap" onclick="ChangeShortlistedStatus({{$Candidates->Id}})" name="{{$Candidates->Id}}" type="radio" @if($Candidates->shortlisted_status == 1) checked @endif/>
                                            <span>Shortlist</span>
                                        </label>
                                    </p>
                                    <p>
                                        <label>
                                            <input class="with-gap" onclick="ChangeRejectedStatus({{$Candidates->Id}})" name="{{$Candidates->Id}}" type="radio" @if($Candidates->shortlisted_status == 2) checked @endif/>
                                            <span>Reject</span>
                                        </label>
                                    </p>
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection


@section('JSScript')
    <script>
         $(document).ready( function () {
            // $('#CandidateDataTable').DataTable();
            $('#CandidateDataTable').dataTable( {
    language: {
        searchPlaceholder: "Type Name, Email, Mobile, Skills"
    }
} );
        } );

    </script>
@endsection