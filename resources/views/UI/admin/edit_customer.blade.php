@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title activator">Form with placeholder<i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Get Code">more_vert</i></h5> -->
                    <form>
                        <div class="row">
                            <div class="input-field col s12">
                            <input placeholder="John Doe" id="customer_name" value="{{$GetCustomers->name}}" type="text">
                            <input placeholder="John Doe" id="Id" value="{{$GetCustomers->id}}" type="hidden">
                            <label for="name2">Customer Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="john@domainname.com" value="{{$GetCustomers->email}}" id="email" type="email">
                                <label for="email2">Email</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="contact_name" value="{{$GetCustomers->contact}}" type="text">
                                <label for="email2">Contact Person Name</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="mobile" value="{{$GetCustomers->mobile}}" type="text" maxlength="10">
                                <label for="email2">Mobile</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <textarea placeholder="" id="address" class="materialize-textarea">{{$GetCustomers->address}}</textarea>
                                <label for="message2">Customer Address</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class=" col s6">
                                <input placeholder="" id="agreement" type="file">
                                <br>
                                <label for="email2">Customer Agreement</label>
                            </div>
                            <div class="input-field col s6">
                                <p>Preview</p>
                            @php
                                if(isset($GetCustomers->agreement)){
                                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                                $type = finfo_file($finfo, "agreement_files/".$GetCustomers->agreement);
                                
                                if (isset($type) && in_array($type, array("image/png", "image/jpeg", "image/gif"))) {
                                    echo "<img src='/agreement_files/".$GetCustomers->agreement."'' class ='customers-agreement-width'>";
                                } else {
                                    echo "<iframe src='/agreement_files/".$GetCustomers->agreement."#toolbar=0' width='100%'' height='300px'>
    </iframe>";
                                }
                            }
                            @endphp
                            {{--  <img src="{{URL::asset("agreement_files")}}/{{$GetCustomers->agreement}}" class ="customers-agreement-width" alt="">  --}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="button" id="UpdateCustomers" name="action">Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection