@extends('UI.base')

@section('Content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title activator">Form with placeholder<i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Get Code">more_vert</i></h5> -->
                    <form>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="customer_name" type="text">
                                <label for="name2">Customer Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="email" type="email">
                                <label for="email2">Email</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="contact_name" type="text">
                                <label for="email2">Contact Person Name</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="" id="mobile" type="text" maxlength="10">
                                <label for="email2">Mobile</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <textarea placeholder="" id="address" class="materialize-textarea"></textarea>
                                <label for="message2">Customer Address</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s3">
                                <label for="name2">Upload Customer agreement (Pdf, Jpg, Jpeg, Png)</label>
                            </div>

                            <div class="input-field col s4">
                                <input placeholder="" id="agreement" type="file" accept="image/jpg,image/jpeg,image/png,application/pdf">
                            </div>

                            <div class="col s5">
                                <img id="agreement_img" src="#" alt="your image" class="customers-agreement-width vendor-pre-image-width" style="display:none;" />

                                <iframe id="Agreement_viewer" frameborder="0" scrolling="no" width="100%" height="300" style="display:none;"></iframe>
                            </div>
                        </div>

                        {{-- <div class="row">
                            <div class=" col s12">
                                <input placeholder="" id="agreement" type="file">
                                <br>
                                <label for="email2">Customer Agreement</label>
                            </div>
                        </div> --}}

                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="button" id="AddCustomers" name="action">Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Code<i class="material-icons right">close</i></span>
                                                    <pre class="pre-scroll">                                                            <code class="language-markup">
                                                                &lt;form&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;&quot; id=&quot;name2&quot; type=&quot;text&quot;&gt;
                                                                            &lt;label for=&quot;name2&quot;&gt;Name&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;<a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="553f3a3d3b15313a38343c3b3b3438307b363a38">[email&#160;protected]</a>&quot; id=&quot;email2&quot; type=&quot;email&quot;&gt;
                                                                            &lt;label for=&quot;email2&quot;&gt;Email&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;input placeholder=&quot;YourPassword&quot; id=&quot;password2&quot; type=&quot;password&quot;&gt;
                                                                            &lt;label for=&quot;password2&quot;&gt;Password&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;textarea placeholder=&quot;Oh WoW! Let me check this one too.&quot; id=&quot;message2&quot; class=&quot;materialize-textarea&quot;&gt;&lt;/textarea&gt;
                                                                            &lt;label for=&quot;message2&quot;&gt;Message&lt;/label&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                    &lt;div class=&quot;row&quot;&gt;
                                                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                                                            &lt;button class=&quot;btn cyan waves-effect waves-light right&quot; type=&quot;submit&quot; name=&quot;action&quot;&gt;Submit
                                                                            &lt;/button&gt;
                                                                        &lt;/div&gt;
                                                                    &lt;/div&gt;
                                                                &lt;/form&gt;
                                                            </code>
                    </pre>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection