@extends('UI.base')

@section('Content')
<div class="container-fluid">
    

    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <!-- <h5 class="card-title activator">Form with placeholder<i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Get Code">more_vert</i></h5> -->
                    <form>
                        

                        <div class="row pad-bottom15">
                            <div class="col s6">
                                
                                <label for="name2">Job Name</label>

                                <h5>{{$GetCandidateDetails->JobName}}</h5>
                            </div>
                            <div class=" col s6">
                                <label for="name2">Candidate Name</label>

                                <h5>{{$GetCandidateDetails->name}}</h5>
                            </div>
                        </div>

                        <div class="row pad-bottom15">
                            <div class="col s6">
                                <label for="name2">Email</label>

                                <h5>{{$GetCandidateDetails->email}}</h5>
                            </div>
                            <div class="col s6">
                                <label for="name2">Mobile</label>

                                <h5>{{$GetCandidateDetails->mobile}}</h5>
                            </div>
                        </div>

                        <div class="row pad-bottom15">
                                <div class=" col s6">
                                        <label for="name2">Company Name</label>
    
                                        <h5>{{$GetCandidateDetails->current_company}}</h5>
                                    </div>
                                <div class="col s6">
                                        <label for="name2">CTC</label>
        
                                        <h5>{{$GetCandidateDetails->current_ctc}}</h5>
                                    </div>
                            </div>
    
                            <div class="row">
                                
                                <div class="col s6">
                                        <label for="name2">Notice Period</label>
    
                                        <h5>{{$GetCandidateDetails->notice_period}} Days</h5>
                                    </div>
                            </div>
                            <br>
                            <h4>Interview Rounds</h4>

                        @if(isset($GetCandidateRoundsDetails))
                        <table class="responsive-table">
                                <thead>
                                    <tr>
                                        <th>Rounds</th>
                                        <th>Interview Status</th>
                                        <th>Interview Mode</th>
                                        <th>Date</th>
                                        <th>Feedback</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>@if($GetCandidateRoundsDetails->round1 == 1)
                                                Round 1
                                                @endif</td>
                                        <td>@if($GetCandidateRoundsDetails->interview_stage_1 == 1)
                                                Rejected
                                                @elseif($GetCandidateRoundsDetails->interview_stage_1 == 2)
                                                Shortlisted
                                                @elseif($GetCandidateRoundsDetails->interview_stage_1 == 3)
                                                On Hold
                                                @elseif($GetCandidateRoundsDetails->interview_stage_1 == 4)
                                                Selected
                                                @endif</td>
                                        
                                        <td>
                                            @if($GetCandidateRoundsDetails->interview_mode1 == 1)
                                            Face To Face
                                        @elseif($GetCandidateRoundsDetails->interview_mode1 == 2)
                                        Telephonic
                                        @elseif($GetCandidateRoundsDetails->interview_mode1 == 3)
                                        Skype
                                        @elseif($GetCandidateRoundsDetails->interview_mode1 == 4)
                                        Online Test
                                        @endif</td>
                                        <td>@if($GetCandidateRoundsDetails->round1_date)
                                                {{$GetCandidateRoundsDetails->round1_date}}
                                                @endif</td>

                                        <td>@if($GetCandidateRoundsDetails->feedback1)
                                                {{$GetCandidateRoundsDetails->feedback1}}
                                                @endif</td>
                                    </tr>

                                    <tr>
                                    <td>@if($GetCandidateRoundsDetails->round2 == 1)
                                            Round 2
                                            @endif</td>
                                    <td>@if($GetCandidateRoundsDetails->interview_stage_2 == 1)
                                            Rejected
                                            @elseif($GetCandidateRoundsDetails->interview_stage_2 == 2)
                                            Shortlisted
                                            @elseif($GetCandidateRoundsDetails->interview_stage_2 == 3)
                                            On Hold
                                            @elseif($GetCandidateRoundsDetails->interview_stage_2 == 4)
                                            Selected
                                            @endif</td>

                                        <td>
                                                @if($GetCandidateRoundsDetails->interview_mode2 == 1)
                                                Face To Face
                                            @elseif($GetCandidateRoundsDetails->interview_mode2 == 2)
                                            Telephonic
                                            @elseif($GetCandidateRoundsDetails->interview_mode2 == 3)
                                            Skype
                                            @elseif($GetCandidateRoundsDetails->interview_mode2 == 4)
                                            Online Test
                                            @endif</td>
                                            
                                    <td>@if($GetCandidateRoundsDetails->round2_date)
                                            {{$GetCandidateRoundsDetails->round1_date}}
                                            @endif</td>

                                    <td>@if($GetCandidateRoundsDetails->feedback2)
                                            {{$GetCandidateRoundsDetails->feedback1}}
                                            @endif</td>
                                </tr>

                                <tr>
                                        <td>@if($GetCandidateRoundsDetails->round3 == 1)
                                                Round 3
                                                @endif</td>
                                        <td>@if($GetCandidateRoundsDetails->interview_stage_3 == 1)
                                                Rejected
                                                @elseif($GetCandidateRoundsDetails->interview_stage_3 == 2)
                                                Shortlisted
                                                @elseif($GetCandidateRoundsDetails->interview_stage_3 == 3)
                                                On Hold
                                                @elseif($GetCandidateRoundsDetails->interview_stage_3 == 4)
                                                Selected
                                                @endif</td>

                                            <td>
                                                    @if($GetCandidateRoundsDetails->interview_mode3 == 1)
                                                    Face To Face
                                                @elseif($GetCandidateRoundsDetails->interview_mode3 == 2)
                                                Telephonic
                                                @elseif($GetCandidateRoundsDetails->interview_mode3 == 3)
                                                Skype
                                                @elseif($GetCandidateRoundsDetails->interview_mode3 == 4)
                                                Online Test
                                                @endif</td>

                                        <td>@if($GetCandidateRoundsDetails->round3_date)
                                                {{$GetCandidateRoundsDetails->round3_date}}
                                                @endif</td>

                                        <td>@if($GetCandidateRoundsDetails->feedback3)
                                                {{$GetCandidateRoundsDetails->feedback3}}
                                                @endif</td>
                                    </tr>

                                    <tr>
                                            <td>@if($GetCandidateRoundsDetails->round4 == 1)
                                                    Round 4
                                                    @endif</td>
                                            <td>@if($GetCandidateRoundsDetails->interview_stage_4 == 1)
                                                    Rejected
                                                    @elseif($GetCandidateRoundsDetails->interview_stage_4 == 2)
                                                    Shortlisted
                                                    @elseif($GetCandidateRoundsDetails->interview_stage_4 == 3)
                                                    On Hold
                                                    @elseif($GetCandidateRoundsDetails->interview_stage_4 == 4)
                                                    Selected
                                                    @endif</td>
    
                                        <td>
                                                @if($GetCandidateRoundsDetails->interview_mode4 == 1)
                                                Face To Face
                                            @elseif($GetCandidateRoundsDetails->interview_mode4 == 2)
                                            Telephonic
                                            @elseif($GetCandidateRoundsDetails->interview_mode4 == 3)
                                            Skype
                                            @elseif($GetCandidateRoundsDetails->interview_mode4 == 4)
                                            Online Test
                                            @endif</td>
                                            
                                            <td>@if($GetCandidateRoundsDetails->round4_date)
                                                    {{$GetCandidateRoundsDetails->round4_date}}
                                                    @endif</td>
    
                                            <td>@if($GetCandidateRoundsDetails->feedback4)
                                                    {{$GetCandidateRoundsDetails->feedback4}}
                                                    @endif</td>
                                        </tr>
                                </tbody>
                            </table>
                            @endif
                            <br>
                        
                        


                        {{--  <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="button" id="AddCandidateDetails" name="action">Submit
                                </button>
                            </div>
                        </div>  --}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection