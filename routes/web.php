<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'UI\AuthendicationController@login');

Route::get('/Admin', 'UI\AuthendicationController@Adminlogin');

Route::post('/AdminLogin', 'UI\AdminController@PostAdminLogin');

Route::get('/dashboard', 'UI\AdminController@dashboard');

Route::get('/customers', 'UI\AdminController@customers');

Route::get('/add_customers', 'UI\AdminController@add_customers');



Route::get('/edit_customers/{id}', 'UI\AdminController@edit_customers');

Route::post('/update_customers', 'UI\AdminController@update_customers');

Route::get('/DeleteCustomers/{id}', 'UI\AdminController@DeleteCustomers');

Route::post('/post_customers', 'UI\AdminController@post_customers');

Route::get('/jobs', 'UI\AdminController@jobs');

Route::get('/add_jobs', 'UI\AdminController@add_jobs');

Route::get('/edit_jobs/{id}', 'UI\AdminController@edit_jobs');

Route::post('/post_jobs', 'UI\AdminController@post_jobs');

Route::post('/update_jobs', 'UI\AdminController@update_jobs');

Route::get('/vendor', 'UI\AdminController@vendor');

Route::get('/add_vendor', 'UI\AdminController@add_vendor');

Route::get('/edit_vendor/{id}', 'UI\AdminController@edit_vendor');

Route::post('/post_vendors', 'UI\AdminController@post_vendors');

Route::post('/update_vendors', 'UI\AdminController@update_vendors');

Route::get('/admin_list', 'UI\AdminController@Admin');

Route::get('/MasterAdmin', 'UI\AdminController@MasterAdmin');

Route::get('/add_admin', 'UI\AdminController@add_admin');

Route::get('/add_master_admin', 'UI\AdminController@add_master_admin');

Route::get('/edit_master_admin/{id}', 'UI\AdminController@edit_master_admin');

Route::get('/edit_admin/{id}', 'UI\AdminController@edit_admin');

Route::post('/post_admin', 'UI\AdminController@post_admin');

Route::post('/post_masteradmin', 'UI\AdminController@post_masteradmin');

Route::post('/Update_admin', 'UI\AdminController@Update_admin');

Route::post('/Update_master_admin', 'UI\AdminController@Update_master_admin');

Route::get('/jobs_list', 'UI\VendorController@jobs');

Route::get('/Adminjobs', 'UI\AdminController@Adminjobs');

Route::get('/Adminlogout', 'UI\AuthendicationController@Adminlogout');

Route::get('/SuperAdminlogout', 'UI\AuthendicationController@SuperAdminlogout');

Route::get('/SubAdminlogout', 'UI\AuthendicationController@SubAdminlogout');

Route::get('/Vendorlogout', 'UI\AuthendicationController@Vendorlogout');

Route::get('/candidate_list', 'UI\VendorController@CandiateList');

Route::get('/AdminCandidateList', 'UI\VendorController@AdminCandidateList');

Route::get('/MasterAdminShortlistedCandidateList/{id}', 'UI\VendorController@MasterAdminShortlistedCandidateList');

Route::get('/AdminShortlistedCandidateList', 'UI\VendorController@AdminShortlistedCandidateList');

Route::get('/AdminRejectedCandidateList', 'UI\VendorController@AdminRejectedCandidateList');

Route::get('/AdminCandiateList/{id}', 'UI\AdminController@AdminCandiateList');

Route::post('/AddJobsToCandidates', 'UI\VendorController@AddJobsToCandidates');

Route::get('/Assigninterviews', 'UI\VendorController@Assigninterviews');

Route::get('/ModifyCandidateDetails/{id}', 'UI\VendorController@ModifyCandidateDetails');

Route::get('/CandidateDetails/{id}', 'UI\AdminController@CandidateDetails');

Route::post('/UpdateCandidateDetails', 'UI\VendorController@UpdateCandidateDetails');


Route::get('/add_candidate', 'UI\VendorController@add_candidate');

Route::get('/change_password', 'UI\VendorController@change_password');

Route::get('/Master_admin_changepassword', 'UI\AdminController@Master_admin_changepassword');

Route::get('/edit_candidate/{Id}', 'UI\VendorController@edit_candidate');


Route::post('/post_candidates', 'UI\VendorController@post_candidates');

Route::post('/update_candidates', 'UI\VendorController@update_candidates');

Route::post('/VendorLogin', 'UI\AuthendicationController@VendorLogin');

Route::post('/MasterAdminLogin', 'UI\AuthendicationController@MasterAdminLogin');

Route::post('/UsersList/{id}', 'UI\VendorController@UsersList');

Route::post('/ChangeStatus/{id}', "UI\AdminController@ChangeStatus");

Route::post('/ChangeShortlistedStatus/{id}', "UI\AdminController@ChangeShortlistedStatus");

Route::post('/ChangeRejectedStatus/{id}', "UI\AdminController@ChangeRejectedStatus");

Route::post('/ChangeJobStatus/{id}', "UI\AdminController@ChangeJobStatus");

Route::post('/ChangeVendorStatus/{id}', "UI\AdminController@ChangeVendorStatus");

Route::post('/ChangeAdminStatus/{id}', "UI\AdminController@ChangeAdminStatus");

Route::post('/ChangeMasterAdminStatus/{id}', "UI\AdminController@ChangeMasterAdminStatus");

Route::post('/CheckCandidateVedorFilters', "UI\VendorController@CheckCandidateVendorFilters");


Route::post('/CheckCandidateAdminFilters', "UI\AdminController@CheckCandidateFilters");

Route::post('/CheckCandidateFilters', "UI\AdminController@CheckCandidateVendorFilters");

Route::post('/CheckCandidateFinalStatusFilters', "UI\AdminController@CheckCandidateFinalStatusFilters");

Route::post('/CheckCandidateFeedbackFilters', "UI\AdminController@CheckCandidateFeedbackFilters");

Route::post('/CheckCustomerData', "UI\VendorController@CheckCustomerData");

Route::post('/CheckJobsData', "UI\VendorController@CheckJobsData");

Route::post('/CheckCustomerJobsData', "UI\VendorController@CheckCustomerJobsData");

Route::post('/CheckJobsListData', "UI\VendorController@CheckJobsListData");

Route::post('/FilterCustomerJobsData', "UI\AdminController@FilterCustomerJobsData");

Route::post('/FilterJobsListData', "UI\AdminController@FilterJobsListData");

Route::post('/UpdatePassword', "UI\VendorController@UpdatePassword");

Route::post('/MasterAdminUpdatePassword', "UI\AdminController@MasterAdminUpdatePassword");