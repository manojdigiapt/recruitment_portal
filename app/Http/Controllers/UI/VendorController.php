<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Model\UI\JobsModel;
use App\Model\UI\AppliedCandidatesModel;
use App\Model\UI\CandidatesModel;
use App\Model\UI\CustomerModel;
use App\Model\UI\RoundsModel;
use App\Model\UI\VendorModel;
use Illuminate\Support\Facades\Auth;

use DB;
use Session;

class VendorController extends Controller
{   
    public function CandiateList(){
        $title = "Master Tracker";
        

        $GetVendorId = Session::get('VendorId');
        
        $GetCandidates = CandidatesModel::where('candidates.vendor_id', $GetVendorId)->get();

        $GetJobs = JobsModel::select('customers.name', 'jobs.*')
                    ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                    ->orderBy('jobs.created_at', 'desc')
                    ->where('jobs.status', 0)
                    ->where('jobs.vendor_id', $GetVendorId)
                    ->get();

        return view("UI.vendor.candidate_list", compact('title', 'GetCandidates', 'GetJobs'));
    }

    public function change_password(){
        $title = "Change Password";
        
        return view("UI.vendor.change_password", compact('title'));
    }

    public function AdminCandidateList(){
        $title = "All Jobs";
    
        $GetCustomers = CustomerModel::get();
        $GetJobsList = JobsModel::get();

        $GetCandidates = DB::table('applied_candidates')
                ->select('candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4', 'jobs.job_title', 'applied_candidates.id AS Id', 'applied_candidates.shortlisted_status')
                ->distinct()
                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
                ->join('rounds', 'rounds.applied_candidate_id', '=', 'applied_candidates.id')
                // ->orderBy('created_at', 'desc')
                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
                // ->where('applied_candidates.shortlisted_status', 1)
                ->get();

        $GetJobs = JobsModel::get();

        return view("UI.admin.admin_checkshortlsitedbyJobs", compact('title', 'GetCustomers', 'GetJobs', 'GetJobsList', 'GetCandidates'));
    }


    public function MasterAdminShortlistedCandidateList($Id){
        $title = "Candidate Details";
        

        $GetCandidates = DB::table('applied_candidates')
                ->select('candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4', 'jobs.job_title', 'applied_candidates.id AS Id', 'applied_candidates.shortlisted_status')
                ->distinct()
                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
                ->join('rounds', 'rounds.applied_candidate_id', '=', 'applied_candidates.id')
                // ->orderBy('created_at', 'desc')
                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
                ->where('jobs.id', $Id)
                // ->where('jobs.vendor_id', $GetVendorId)
                // ->where('applied_candidates.shortlisted_status', 1)
                ->get();


        // $GetCandidates = CandidatesModel::select('candidates.*', 'jobs.job_title', 'applied_candidates.id As Id', 'applied_candidates.shortlisted_status')
        //         ->join('applied_candidates', 'applied_candidates.candidate_id', '=', 'candidates.id')
        //         ->join('jobs', 'applied_candidates.jobs_id', '=', 'jobs.id')
        //         ->where('applied_candidates.shortlisted_status', 1)
        //         ->get();

        

        $GetJobs = JobsModel::get();

        return view("UI.admin.master_admin_candidate_shortlisted", compact('title', 'GetCandidates', 'GetJobs'));
    }


    public function AdminShortlistedCandidateList(){
        $title = "Candidate Details";
        

        $GetCustomerId = Session::get('SubAdminCustomerId');

       
        $GetCandidates = DB::table('applied_candidates')
                ->select('candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4', 'jobs.job_title', 'applied_candidates.id AS Id', 'applied_candidates.shortlisted_status')
                ->distinct()
                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
                ->join('rounds', 'rounds.applied_candidate_id', '=', 'applied_candidates.id')
                // ->orderBy('created_at', 'desc')
                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
                // ->where('jobs.vendor_id', $GetVendorId)
                ->where('applied_candidates.shortlisted_status', 1)
                ->where('applied_candidates.customer_id', $GetCustomerId)
                ->get();
        
        // $GetCandidates = CandidatesModel::select('candidates.*', 'jobs.job_title', 'applied_candidates.id As Id', 'applied_candidates.shortlisted_status')
        //         ->join('applied_candidates', 'applied_candidates.candidate_id', '=', 'candidates.id')
        //         ->join('jobs', 'applied_candidates.jobs_id', '=', 'jobs.id')
        //         ->where('applied_candidates.shortlisted_status', 1)
        //         ->where('applied_candidates.customer_id', $GetCustomerId)
        //         ->get();
        

        $GetJobs = JobsModel::get();

        return view("UI.admin.sub_admin_shortlisted_candidates_list", compact('title', 'GetCandidates', 'GetJobs'));
    }


    public function AdminRejectedCandidateList(){
        $title = "Candidate Details";
        

        $GetCustomerId = Session::get('SubAdminCustomerId');

       
        $GetCandidates = DB::table('applied_candidates')
                ->select('candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4', 'jobs.job_title', 'applied_candidates.id AS Id', 'applied_candidates.shortlisted_status')
                ->distinct()
                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
                ->join('rounds', 'rounds.applied_candidate_id', '=', 'applied_candidates.id')
                // ->orderBy('created_at', 'desc')
                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
                // ->where('jobs.vendor_id', $GetVendorId)
                ->where('applied_candidates.shortlisted_status', 2)
                ->where('applied_candidates.customer_id', $GetCustomerId)
                ->get();
        
        

        $GetJobs = JobsModel::get();

        return view("UI.admin.sub_admin_shortlisted_candidates_list", compact('title', 'GetCandidates', 'GetJobs'));
    }


    public function add_candidate(){
        $title = "Add Candidate";
        
        return view("UI.vendor.add_candidate", compact('title'));
    }

    public function edit_candidate($Id){
        $title = "Edit Candidate";

        $GetCandidates = CandidatesModel::find($Id);
        
        return view("UI.vendor.edit_candidate", compact('title', 'GetCandidates'));
    }

    public function jobs(){
        $title = "Jobs";

        $GetVendorId = Session::get('VendorId');
        
        // $GetCustomers = CustomerModel::get();

        $GetCustomers = DB::table('jobs')
                        ->distinct()
                        ->select('customers.name', 'customers.id')
                        ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                        ->where('jobs.vendor_id', $GetVendorId)
                        ->get();
                        
        $GetJobsList = JobsModel::where('jobs.vendor_id', $GetVendorId)-> get();

        $GetJobs = DB::table('jobs')
                ->select('customers.name', 'jobs.*')
                ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                ->where('jobs.vendor_id', $GetVendorId)
                ->get();
        
        $GetCandidates = CandidatesModel::get();

        // echo json_encode($GetJobsList);
        // exit;
        
        return view("UI.vendor.jobs_list", compact('title', 'GetJobs', 'GetCandidates', 'GetCustomers', 'GetJobsList'));
    }

    public function Assigninterviews(){
        $title = "Candidate List";

        $GetVendorId = Session::get('VendorId');

        // $GetCustomers = CustomerModel::get();
        // $GetJobs = JobsModel::get();
        $GetCustomers = DB::table('jobs')
                        ->distinct()
                        ->select('customers.name', 'customers.id')
                        ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                        ->where('jobs.vendor_id', $GetVendorId)
                        ->get();
                        

        $GetJobs = DB::table('jobs')
                ->select('customers.name', 'jobs.*')
                ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                ->where('jobs.vendor_id', $GetVendorId)
                ->get();

        // echo $GetVendorId;
        // exit;
        if(Auth::guard('vendor')->check()){
            $GetCandidates = DB::table('applied_candidates')
                ->select('candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4', 'jobs.job_title', 'applied_candidates.id AS Id', 'applied_candidates.shortlisted_status')
                ->distinct()
                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
                ->join('rounds', 'rounds.applied_candidate_id', '=', 'applied_candidates.id')
                // ->orderBy('created_at', 'desc')
                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
                ->where('jobs.vendor_id', $GetVendorId)
                // ->where('applied_candidates.shortlisted_status', 1)
                ->get();

        }elseif(Auth::guard('admin')->check()){
            $GetCandidates = DB::table('applied_candidates')
            ->select('candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4', 'applied_candidates.shortlisted_status')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'rounds.applied_candidate_id', '=', 'applied_candidates.id')
            ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
            ->orderBy('created_at', 'desc')
            // ->join('customers', 'customers.id', '=', 'jobs.customer_id')
            ->get();
        }

        // echo json_encode($GetCandidates);
        // exit;

        return view("UI.vendor.assign_interviews", compact('title', 'GetCandidates', 'GetCustomers', 'GetJobs'));
    }


    public function ModifyCandidateDetails($Id){
        $title = "Candidate Details";

        // $GetVendorId = Session::get('VendorId');
        // echo $Id;
        // exit;

        $GetCandidateDetails = DB::table('applied_candidates')
                ->select("applied_candidates.id AS Id", "candidates.*", "jobs.job_title AS JobName", "applied_candidates.company_name", "applied_candidates.ctc", "applied_candidates.mode")
                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
                // ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
                ->where('applied_candidates.id', $Id)
                ->first();
        
        $GetCandidateRoundsDetails = DB::table('applied_candidates')
                ->select('rounds.*')
                ->join('rounds', 'applied_candidates.id', '=', 'rounds.applied_candidate_id')
                ->where('rounds.applied_candidate_id', $Id)
                ->first();

        // echo json_encode($GetCandidateRoundsDetails);
        // exit;

        return view("UI.vendor.modify_candidate_details", compact('title', 'GetCandidateDetails', 'GetCandidateRoundsDetails'));
    }


    public function UpdateCandidateDetails(Request $request){
        $Id = $request->Id;

        // $Candidates = AppliedCandidatesModel::where('id', $Id)->first();

        // echo json_encode($Id);
        // exit;

        // $Candidates->name = $request->name;
        // $Candidates->email = $request->email;
        // $Candidates->mobile = $request->mobile;
        // $Candidates->interview_schedule = $request->schedule;
        // $Candidates->feedback = $request->feedback;
        // $Candidates->final_status = $request->final_status;
        // $Candidates->interview_status = $request->interview_status;
        // $Candidates->round1 = $request->round1;
        // $Candidates->round2 = $request->round2;
        // $Candidates->round3 = $request->round3;
        // $Candidates->company_name = $request->company_name;
        // $Candidates->ctc = $request->ctc;
        // $Candidates->mode = $request->mode;

        // $InsertCandidates = $Candidates->save();

        $CheckRounds = RoundsModel::where('applied_candidate_id', $Id)->first();

        if($CheckRounds){
            // $CheckRounds->candidate_id = $Id;
            $CheckRounds->round1 = $request->round1;
            $CheckRounds->interview_stage_1 = $request->stage1;
            $CheckRounds->round1_date = $request->schedule1;
            $CheckRounds->interview_mode1 = $request->interview_mode1;
            $CheckRounds->feedback1 = $request->feedback1;

            $CheckRounds->round2 = $request->round2;
            $CheckRounds->interview_stage_2 = $request->stage2;
            $CheckRounds->round2_date = $request->schedule2;
            $CheckRounds->interview_mode2 = $request->interview_mode2;
            $CheckRounds->feedback2 = $request->feedback2;

            $CheckRounds->round3 = $request->round3;
            $CheckRounds->interview_stage_3 = $request->stage3;
            $CheckRounds->round3_date = $request->schedule3;
            $CheckRounds->interview_mode3 = $request->interview_mode3;
            $CheckRounds->feedback3 = $request->feedback3;

            $CheckRounds->round4 = $request->round4;
            $CheckRounds->interview_stage_4 = $request->stage4;
            $CheckRounds->round4_date = $request->schedule4;
            $CheckRounds->interview_mode4 = $request->interview_mode4;
            $CheckRounds->feedback4 = $request->feedback4;


            $UpdateRounds = $CheckRounds->save();
            // echo "update";
        }else{
            $Rounds = new RoundsModel();

            $Rounds->candidate_id = $Id;
            $Rounds->round1 = $request->round1;
            $Rounds->interview_stage_1 = $request->stage1;
            $Rounds->round1_date = $request->schedule1;
            $CheckRounds->interview_mode1 = $request->interview_mode1;
            $Rounds->feedback1 = $request->feedback1;

            $Rounds->round2 = $request->round2;
            $Rounds->interview_stage_2 = $request->stage2;
            $Rounds->round2_date = $request->schedule2;
            $CheckRounds->interview_mode2 = $request->interview_mode2;
            $Rounds->feedback2 = $request->feedback2;

            $Rounds->round3 = $request->round3;
            $Rounds->interview_stage_3 = $request->stage3;
            $Rounds->round3_date = $request->schedule3;
            $CheckRounds->interview_mode3 = $request->interview_mode3;
            $Rounds->feedback3 = $request->feedback3;

            $Rounds->round4 = $request->round4;
            $Rounds->interview_stage_4 = $request->stage4;
            $Rounds->round4_date = $request->schedule4;
            $CheckRounds->interview_mode4 = $request->interview_mode4;
            $Rounds->feedback4 = $request->feedback4;


            $UpdateRounds = $Rounds->save();

            // echo "insert";
        }

        // exit;

        if($UpdateRounds){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Candidate details updated"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
    }


    public function post_candidates(Request $request){
        $GetEmail = $request->email;
        $GetMobile = $request->mobile;

        $CheckEmail = CandidatesModel::where('email', $GetEmail)->first();

        $CheckMobile = CandidatesModel::where('mobile', $GetMobile)->first();

        if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Email id already exists."
            ));
        }elseif($CheckMobile){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Mobile no already exists."
            ));
        }else{
            $Candidates = new CandidatesModel();

            // echo $request->JobId;
            // exit;
            $GetVendorId = Session::get('VendorId');
    
            $Candidates->vendor_id = $GetVendorId;
            $Candidates->name = $request->name;
            $Candidates->email = $request->email;
            $Candidates->mobile = $request->mobile;
            $Candidates->skills = $request->skills;
            $Candidates->current_company = $request->current_company;
            $Candidates->current_ctc = $request->current_ctc;
            $Candidates->notice_period = $request->notice_period;
    
            $extension = $request->file('resume')->getClientOriginalExtension();
            $dir = 'resume/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('resume')->move($dir, $filename);
    
            $Candidates->resume = $filename;
    
            // $Customers->status = "1";
    
            $InsertCandidates = $Candidates->save();
    
            // $Rounds = new RoundsModel();
            
            // $Rounds->candidate_id = $Candidates->id;
            // $Rounds->save();
    
            if($InsertCandidates){
                return response()->json(array(
                        "error"=>FALSE,
                        "message" => "Candidate added"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message" => "Failed"
                ));
            }
        }
        
    }


    public function update_candidates(Request $request){
        $GetId = $request->Id;
        // echo $GetId;
        // exit;

        $GetEmail = $request->email;
        $GetMobile = $request->mobile;

        $Candidates = CandidatesModel::find($GetId);

        // echo $request->JobId;
        // exit;
        // $GetVendorId = Session::get('VendorId');

        // $Candidates->vendor_id = $GetVendorId;
        $Candidates->name = $request->name;
        $Candidates->email = $request->email;
        $Candidates->mobile = $request->mobile;
        $Candidates->skills = $request->skills;
        $Candidates->current_company = $request->current_company;
        $Candidates->current_ctc = $request->current_ctc;
        $Candidates->notice_period = $request->notice_period;

        $File = $request->resume;

        if($File == "undefined" || null){
            $filename = $Candidates->resume;
        }else{
            $extension = $request->file('resume')->getClientOriginalExtension();
            $dir = 'resume/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('resume')->move($dir, $filename);
        }
        

        $Candidates->resume = $filename;

        // $Customers->status = "1";

        $InsertCandidates = $Candidates->save();

        // $Rounds = new RoundsModel();
        
        // $Rounds->candidate_id = $Candidates->id;
        // $Rounds->save();

        if($InsertCandidates){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Candidate updated"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
        
    }



    public function UsersList($Id){
        $GetUsers = AppliedCandidatesModel::join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
                    ->where('jobs_id', $Id)->get();

        foreach($GetUsers as $Users){
            $GetData = "<tr><td>".$Users->name."</td><td>".$Users->email."</td><td>".$Users->mobile."</td><td><a href='resume/".$Users->resume."' target='_blank'>View Resume</a></td></tr>";

            echo $GetData;
        }
    }


    public function CheckCandidateVendorFilters(Request $request){
        $InterviewStatus = $request->interview_status;
        // $FinalStatus = $request->final_status;
        // $Feedback = $request->feedback;

        // echo $InterviewStatus;
        // exit;
        $GetVendorId = Session::get('VendorId');

        if($InterviewStatus == 0){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('interview_status', $InterviewStatus)
            ->where('jobs.vendor_id', $GetVendorId)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 1){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_1', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 2){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_1', 2)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 3){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_1', 3)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 4){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_1', 4)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 5){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_2', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 6){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_2', 2)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 7){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_2', 3)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 8){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_2', 4)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 9){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_3', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 10){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_3', 2)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 11){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_3', 3)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 12){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_3', 4)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 13){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_4', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 14){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_4', 2)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 15){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_3', 3)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 16){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_4', 4)
            ->orderBy('created_at', 'desc')
            ->get();
        }

        $GetLinks = "";

    foreach($GetData as $Data){

        if(Auth::guard('vendor')->check()){
            $GetLinks = " <a href='/ModifyCandidateDetails/".$Data->id."' target='_blank'>Modify Status</a></td>";
        }elseif(Auth::guard('admin')->check()){
            $GetLinks = "<a href='/CandidateDetails/".$Data->id."' target='_blank'>View Details</a></td>";
        }

            $GetRound1 = "";
            $GetRound2 = "";
            $GetRound3 = "";
            $GetRound4 = "";

            if($Data->Round1 == 1){
                $GetRound1 = "Rejected";
            }elseif($Data->Round1 == 2){
                $GetRound1 = "Shortlisted";
            }elseif($Data->Round1 == 3){
                $GetRound1 = "On Hold";
            }elseif($Data->Round1 == 4){
                $GetRound1 = "Selected";
            }

            if($Data->Round2 == 1){
                $GetRound2 = "Rejected";
            }elseif($Data->Round2 == 2){
                $GetRound2 = "Shortlisted";
            }elseif($Data->Round2 == 3){
                $GetRound2 = "On Hold";
            }elseif($Data->Round2 == 4){
                $GetRound2 = "Selected";
            }

            if($Data->Round3 == 1){
                $GetRound3 = "Rejected";
            }elseif($Data->Round3 == 2){
                $GetRound3 = "Shortlisted";
            }elseif($Data->Round3 == 3){
                $GetRound3 = "On Hold";
            }elseif($Data->Round3 == 4){
                $GetRound3 = "Selected";
            }

            if($Data->Round4 == 1){
                $GetRound4 = "Rejected";
            }elseif($Data->Round4 == 2){
                $GetRound4 = "Shortlisted";
            }elseif($Data->Round4 == 3){
                $GetRound4 = "On Hold";
            }elseif($Data->Round4 == 4){
                $GetRound4 = "Selected";
            }

            $GetResult = "<tr>
                            <td>".$Data->name."</td>
                            <td>".$Data->email."</td>
                            <td>".$Data->mobile."</td>
                            <td>".$GetRound1."</td>
                            <td>".$GetRound2."</td>
                            <td>".$GetRound3."</td>
                            <td>".$GetRound4."</td>
                            <td>".$GetLinks."</td>
                        </tr>";
            
            echo $GetResult;
    }                
            
    }
    

    public function AddJobsToCandidates(Request $request){
        $GetCandidateNames = $request->candidate;

        foreach($GetCandidateNames as $CandidateName){
            // echo json_encode($CandidateName);

            $GetJobNames = $request->Jobs;

            foreach($GetJobNames as $JobName){

                // echo json_encode($JobName);

                $CheckData = AppliedCandidatesModel::where('jobs_id', $JobName)
                                                    ->where('candidate_id', $CandidateName)
                                                    ->first();

                if($CheckData){
                    $CheckData->jobs_id = $JobName;
                    $CheckData->candidate_id = $CandidateName;
    
                    $AddCandidates = $CheckData->save();
                }else{
                    $Candidates = new AppliedCandidatesModel();

                    $Candidates->jobs_id = $JobName;
                    $Candidates->candidate_id = $CandidateName;
                    $Candidates->customer_id = $request->CustomerId;
                    
    
                    $AddCandidates = $Candidates->save();
                }

            }
        }
        if($AddCandidates){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Candidate details updated"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
        
        // exit;
    }


    public function CheckCustomerData(Request $request){
        $GetId = $request->Id;

        $GetJobId = $request->JobsId;
        
        $GetVendorId = Session::get('VendorId');

        if($GetJobId != 0){
            $GetCandidateByCustomer = DB::table('customers')
                                ->distinct()
                                  ->select('candidates.name', 'candidates.email', 'candidates.id', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4',  'applied_candidates.id AS Id', 'jobs.job_title', 'applied_candidates.shortlisted_status')

                                ->join('applied_candidates', 'applied_candidates.customer_id', '=', 'customers.id')

                                ->join('rounds', 'applied_candidates.id', '=', 'rounds.applied_candidate_id')

                                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')

                                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')

                                ->where('applied_candidates.customer_id', $GetId)
                                ->where('jobs.vendor_id', $GetVendorId)
                                // ->where('applied_candidates.shortlisted_status', 1)

                                ->where('applied_candidates.jobs_id', $GetJobId)
                                

                                ->get();
        }else{
            $GetCandidateByCustomer = DB::table('customers')
                                ->distinct()
                                  ->select('candidates.name', 'candidates.email', 'candidates.id', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4',  'applied_candidates.id AS Id', 'jobs.job_title', 'applied_candidates.shortlisted_status')

                                ->join('applied_candidates', 'applied_candidates.customer_id', '=', 'customers.id')

                                ->join('rounds', 'applied_candidates.id', '=', 'rounds.applied_candidate_id')

                                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')

                                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')

                                ->where('applied_candidates.customer_id', $GetId)
                                ->where('jobs.vendor_id', $GetVendorId)
                                // ->where('applied_candidates.shortlisted_status', 1)

                                ->get();
        }

            // $GetCandidateByCustomer = DB::select("SELECT DISTINCT candidates.name, candidates.email, candidates.id, rounds.interview_stage_1 AS Round1, rounds.interview_stage_2 AS Round2, rounds.interview_stage_3 AS Round3, rounds.interview_stage_4 AS Round4, applied_candidates.id AS Id, jobs.job_title FROM candidates, applied_candidates, rounds, customers, jobs WHERE applied_candidates.customer_id = customers.id AND applied_candidates.id = rounds.applied_candidate_id AND candidates.id = applied_candidates.candidate_id AND applied_candidates.customer_id = $GetId AND applied_candidates.shortlisted_status = 1 AND applied_candidates.jobs_id = jobs.id");

            foreach($GetCandidateByCustomer as $Data){
                if($Data->shortlisted_status == 2){
                    $GetLinks = "<span class='clr-red'>Rejected</span>";
                }elseif(Auth::guard('vendor')->check()){
                    $GetLinks = " <a href='/ModifyCandidateDetails/".$Data->id."' target='_blank'>Modify Status</a></td>";
                }elseif(Auth::guard('admin')->check()){
                    $GetLinks = "<a href='/CandidateDetails/".$Data->id."' target='_blank'>View Details</a></td>";
                }
        
                    $GetRound1 = "";
                    $GetRound2 = "";
                    $GetRound3 = "";
                    $GetRound4 = "";
        
                    if($Data->Round1 == 1){
                        $GetRound1 = "Rejected";
                    }elseif($Data->Round1 == 2){
                        $GetRound1 = "Shortlisted";
                    }elseif($Data->Round1 == 3){
                        $GetRound1 = "On Hold";
                    }elseif($Data->Round1 == 4){
                        $GetRound1 = "Selected";
                    }
        
                    if($Data->Round2 == 1){
                        $GetRound2 = "Rejected";
                    }elseif($Data->Round2 == 2){
                        $GetRound2 = "Shortlisted";
                    }elseif($Data->Round2 == 3){
                        $GetRound2 = "On Hold";
                    }elseif($Data->Round2 == 4){
                        $GetRound2 = "Selected";
                    }
        
                    if($Data->Round3 == 1){
                        $GetRound3 = "Rejected";
                    }elseif($Data->Round3 == 2){
                        $GetRound3 = "Shortlisted";
                    }elseif($Data->Round3 == 3){
                        $GetRound3 = "On Hold";
                    }elseif($Data->Round3 == 4){
                        $GetRound3 = "Selected";
                    }
        
                    if($Data->Round4 == 1){
                        $GetRound4 = "Rejected";
                    }elseif($Data->Round4 == 2){
                        $GetRound4 = "Shortlisted";
                    }elseif($Data->Round4 == 3){
                        $GetRound4 = "On Hold";
                    }elseif($Data->Round4 == 4){
                        $GetRound4 = "Selected";
                    }
        
                    $GetResult = "<tr>
                                    <td>".$Data->name."</td>
                                    <td>".$Data->email."</td>
                                    <td>".$Data->job_title."</td>
                                    <td>".$GetRound1."</td>
                                    <td>".$GetRound2."</td>
                                    <td>".$GetRound3."</td>
                                    <td>".$GetRound4."</td>
                                    <td>".$GetLinks."</td>
                                </tr>";
                    
                    echo $GetResult;
            }
    }


    public function CheckJobsData(Request $request){
        $GetId = $request->Id;

        $GetJobId = $request->JobsId;
       
        $GetVendorId = Session::get('VendorId');

        if($GetId != 0){
            $GetCandidateByCustomer = DB::table('customers')
                                ->distinct()
                                  ->select('candidates.name', 'candidates.email', 'candidates.id', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4',  'applied_candidates.id AS Id', 'jobs.job_title', 'applied_candidates.shortlisted_status')

                                ->join('applied_candidates', 'applied_candidates.customer_id', '=', 'customers.id')

                                ->join('rounds', 'applied_candidates.id', '=', 'rounds.applied_candidate_id')

                                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')

                                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')

                                ->where('applied_candidates.customer_id', $GetId)
                                
                                // ->where('applied_candidates.shortlisted_status', 1)
                                ->where('jobs.vendor_id', $GetVendorId)
                                ->where('applied_candidates.jobs_id', $GetJobId)
                                

                                ->get();
        }else{
            $GetCandidateByCustomer = DB::table('customers')
                                ->distinct()
                                  ->select('candidates.name', 'candidates.email', 'candidates.id', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4',  'applied_candidates.id AS Id', 'jobs.job_title', 'applied_candidates.shortlisted_status')

                                ->join('applied_candidates', 'applied_candidates.customer_id', '=', 'customers.id')

                                ->join('rounds', 'applied_candidates.id', '=', 'rounds.applied_candidate_id')

                                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')

                                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')

                                ->where('applied_candidates.jobs_id', $GetJobId)
                                ->where('jobs.vendor_id', $GetVendorId)
                                // ->where('applied_candidates.shortlisted_status', 1)

                                ->get();
        }

        

            // $GetCandidateByCustomer = DB::select("SELECT DISTINCT candidates.name, candidates.email, candidates.id, rounds.interview_stage_1 AS Round1, rounds.interview_stage_2 AS Round2, rounds.interview_stage_3 AS Round3, rounds.interview_stage_4 AS Round4, applied_candidates.id AS Id, jobs.job_title FROM candidates, applied_candidates, rounds, customers, jobs WHERE applied_candidates.customer_id = customers.id AND applied_candidates.id = rounds.applied_candidate_id AND candidates.id = applied_candidates.candidate_id AND applied_candidates.customer_id = $GetId AND applied_candidates.shortlisted_status = 1 AND applied_candidates.jobs_id = jobs.id");

            foreach($GetCandidateByCustomer as $Data){
                if($Data->shortlisted_status == 2){
                    $GetLinks = "<span class='clr-red'>Rejected</span>";
                }elseif(Auth::guard('vendor')->check()){
                    $GetLinks = " <a href='/ModifyCandidateDetails/".$Data->id."' target='_blank'>Modify Status</a></td>";
                }elseif(Auth::guard('admin')->check()){
                    $GetLinks = "<a href='/CandidateDetails/".$Data->id."' target='_blank'>View Details</a></td>";
                }
        
                    $GetRound1 = "";
                    $GetRound2 = "";
                    $GetRound3 = "";
                    $GetRound4 = "";
        
                    if($Data->Round1 == 1){
                        $GetRound1 = "Rejected";
                    }elseif($Data->Round1 == 2){
                        $GetRound1 = "Shortlisted";
                    }elseif($Data->Round1 == 3){
                        $GetRound1 = "On Hold";
                    }elseif($Data->Round1 == 4){
                        $GetRound1 = "Selected";
                    }
        
                    if($Data->Round2 == 1){
                        $GetRound2 = "Rejected";
                    }elseif($Data->Round2 == 2){
                        $GetRound2 = "Shortlisted";
                    }elseif($Data->Round2 == 3){
                        $GetRound2 = "On Hold";
                    }elseif($Data->Round2 == 4){
                        $GetRound2 = "Selected";
                    }
        
                    if($Data->Round3 == 1){
                        $GetRound3 = "Rejected";
                    }elseif($Data->Round3 == 2){
                        $GetRound3 = "Shortlisted";
                    }elseif($Data->Round3 == 3){
                        $GetRound3 = "On Hold";
                    }elseif($Data->Round3 == 4){
                        $GetRound3 = "Selected";
                    }
        
                    if($Data->Round4 == 1){
                        $GetRound4 = "Rejected";
                    }elseif($Data->Round4 == 2){
                        $GetRound4 = "Shortlisted";
                    }elseif($Data->Round4 == 3){
                        $GetRound4 = "On Hold";
                    }elseif($Data->Round4 == 4){
                        $GetRound4 = "Selected";
                    }
        
                    $GetResult = "<tr>
                                    <td>".$Data->name."</td>
                                    <td>".$Data->email."</td>
                                    <td>".$Data->job_title."</td>
                                    <td>".$GetRound1."</td>
                                    <td>".$GetRound2."</td>
                                    <td>".$GetRound3."</td>
                                    <td>".$GetRound4."</td>
                                    <td>".$GetLinks."</td>
                                </tr>";
                    
                    echo $GetResult;
            }
    }





    public function CheckCustomerJobsData(Request $request){
        $GetId = $request->Id;

        $GetJobId = $request->JobsId;
        
        $GetVendorId = Session::get('VendorId');

        // echo $GetId;
        // exit;

        if($GetJobId != 0){
            $GetJobs = DB::table('jobs')
                ->select('customers.name', 'jobs.*')
                ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                ->where('jobs.vendor_id', $GetVendorId)
                ->where('jobs.id', $GetId)
                ->get();
        }else{
            $GetJobs = DB::table('jobs')
                ->select('customers.name', 'jobs.*')
                ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                ->where('jobs.vendor_id', $GetVendorId)
                ->where('jobs.customer_id', $GetId)
                ->get();
        }


        foreach($GetJobs as $Jobs){

                $GetCandidatesCount = AppliedCandidatesModel::where('jobs_id', $Jobs->id)->count();

                $GetJobStatus = "";

                if($Jobs->status == 0){
                    $GetJobStatus = "<span class='clr-green'>Active</span>";
                }elseif($Jobs->status == 1){
                    $GetJobStatus = "<span class='clr-red'>In Active</span>";
                }

                $GetResult = "<div class='card blue-grey darken-1 bg-white-shadow'>
                <div class='card-content job-card-pad-bottom job_pad_bottom'>
                    <p class='pull-right job-status'>".$GetJobStatus."</p>

                    <span class='card-title'>Job Title: ".$Jobs->job_title."</span>
                    
                    <p><b>Customer Name:</b> ".$Jobs->name."</p>
                    <div class='col m4'>
                        <p>Qualification: ".$Jobs->qualification."</p>
                    </div>
                    <div class='col m4'>
                    <p>Total Experience: ".$Jobs->experience_from." Years - ".$Jobs->experience_to." Months</p>
                    </div>
                    <div class='col m4'>
                        <p>Annual CTC Budget: ".$Jobs->budget_from." </p>
                    </div>
                    <div class='col m4'>
                        <p>Primary Skills: ".$Jobs->details."</p>
                    </div>
                    <div class='col m4'>
                        <p>Job Location: ".$Jobs->location."</p>
                    </div>
                    <div class='col m4'>
                        <p>Notice period: ".$Jobs->notice_period." Days</p>
                    </div>

                    <div class='col m12 pad-top15'>
                        <p><b>Job Description:</b> ".$Jobs->job_description."</p>
                    </div>
                </div>
            </div>";
                
                echo $GetResult;
        }
    }


    public function CheckJobsListData(Request $request){
        $GetId = $request->Id;

        $GetJobId = $request->JobsId;
       
        $GetVendorId = Session::get('VendorId');

        

        if($GetId != 0){
            $GetJobs = DB::table('jobs')
                ->select('customers.name', 'jobs.*')
                ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                ->where('jobs.vendor_id', $GetVendorId)
                ->where('jobs.customer_id', $GetId)
                ->where('jobs.id', $GetJobId)
                ->get();
        }else{
            $GetJobs = DB::table('jobs')
                ->select('customers.name', 'jobs.*')
                ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                ->where('jobs.vendor_id', $GetVendorId)
                ->where('jobs.id', $GetJobId)
                ->get();
        }
        
        // echo json_encode($GetJobs);
        // exit;

            foreach($GetJobs as $Jobs){

                $GetCandidatesCount = AppliedCandidatesModel::where('jobs_id', $Jobs->id)->count();

                $GetJobStatus = "";

                if($Jobs->status == 0){
                    $GetJobStatus = "<span class='clr-green'>Active</span>";
                }elseif($Jobs->status == 1){
                    $GetJobStatus = "<span class='clr-red'>In Active</span>";
                }

                $GetResult = "<div class='card blue-grey darken-1 bg-white-shadow'>
                <div class='card-content job-card-pad-bottom job_pad_bottom'>
                    <p class='pull-right job-status'>".$GetJobStatus."</p>

                    <span class='card-title'>Job Title: ".$Jobs->job_title."</span>
                    
                    <p><b>Customer Name:</b> ".$Jobs->name."</p>
                    <div class='col m4'>
                        <p>Qualification: ".$Jobs->qualification."</p>
                    </div>
                    <div class='col m4'>
                    <p>Total Experience: ".$Jobs->experience_from." Years - ".$Jobs->experience_to." Months</p>
                    </div>
                    <div class='col m4'>
                        <p>Annual CTC Budget: ".$Jobs->budget_from." </p>
                    </div>
                    <div class='col m4'>
                        <p>Primary Skills: ".$Jobs->details."</p>
                    </div>
                    <div class='col m4'>
                        <p>Job Location: ".$Jobs->location."</p>
                    </div>
                    <div class='col m4'>
                        <p>Notice period: ".$Jobs->notice_period." Days</p>
                    </div>

                    <div class='col m12 pad-top15'>
                        <p><b>Job Description:</b> ".$Jobs->job_description."</p>
                    </div>
                </div>
            </div>";
                
                echo $GetResult;
        }
    }

    public function UpdatePassword(Request $request){
        // $OldPassword = $request->old_password;
        $NewPassword = $request->new_password;
        $ConfirmPassword = $request->confirm_password;

        // $CheckOldPassword = VendorModel::where('password', $OldPassword)->first();
        $GetVendorId = Session::get('VendorId');

        $GetPassword = VendorModel::where('id', $GetVendorId)->first();

        if($NewPassword != $ConfirmPassword){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Password does not match."
            ));
        }else{
            $GetPassword->password = Hash::make($NewPassword);
            $UpdatePassword = $GetPassword->save();

            return response()->json(array(
                "error"=>FALSE,
                "message" => "Password updated successfully."
            ));
        }
    }
}
