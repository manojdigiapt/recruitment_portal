<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Model\UI\VendorModel;
use App\Model\UI\MasterAdminModel;

use Session;


class AuthendicationController extends Controller
{   
    public function SuperAdminlogout(Request $request) {
        // Auth::logout();
        Auth::guard('super_admin')->logout();
        return redirect('/');
    }

    public function Adminlogout(Request $request) {
        // Auth::logout();
        Auth::guard('master_admin')->logout();
        return redirect('/');
    }

    public function SubAdminlogout(Request $request) {
        // Auth::logout();
        Auth::guard('sub_admin')->logout();
        return redirect('/');
    }

    public function Vendorlogout(Request $request) {
        // Auth::logout();
        Auth::guard('vendor')->logout();
        return redirect('/');
    }

    public function login(){
        $title = "Recruitment Login";

        return view("UI.login", compact('title'));
    }

    public function Adminlogin(){
        $title = "Admin Login";

        return view("UI.admin_login", compact('title'));
    }


    public function MasterAdminLogin(Request $request){
        $CheckCandidateOrTrainee = MasterAdminModel::where('email', $request->username)
                                ->first();
        
        $CheckHashPassword = Hash::check($request->password, $CheckCandidateOrTrainee["password"]);

        // echo json_encode($CheckHashPassword);
        // exit;

        if($CheckCandidateOrTrainee['status'] == 1){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Your account has been disabled. Please contact your admin."
            ));
        }elseif($CheckHashPassword == true){
            $CheckCandidateOrTraineeType = MasterAdminModel::where('email', $request->username)
                                ->first();
            
            if($CheckCandidateOrTraineeType->login_type == 1){
                
                if (Auth::guard('super_admin')->attempt(['email' => $request->username, 'password' => $request->password])) {
                    //Authentication passed...
                    $request->session()->put('SuperAdminName', Auth::guard('super_admin')->user()->name);
                    $request->session()->put('SuperAdminEmail', Auth::guard('super_admin')->user()->email);
                    $request->session()->put('SuperAdminId', Auth::guard('super_admin')->user()->id);
                    return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Login successfully"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please check your credentials..."
                    ));
                }   
            }elseif($CheckCandidateOrTraineeType->login_type == 2){
                if (Auth::guard('master_admin')->attempt(['email' => $request->username, 'password' => $request->password])) {
                    //Authentication passed...
                    $request->session()->put('MasterAdminName', Auth::guard('master_admin')->user()->name);
                    $request->session()->put('MasterAdminEmail', Auth::guard('master_admin')->user()->email);
                    $request->session()->put('MasterAdminId', Auth::guard('master_admin')->user()->id);
                    return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Login successfully"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please check your credentials..."
                    ));
                }   
            }
            
        }
        else{
            return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please check your credentials..."
                    ));
        }
    }



    public function VendorLogin(Request $request){
        $CheckCandidateOrTrainee = VendorModel::where('email', $request->username)
                                ->first();
        // echo json_encode($CheckCandidateOrTrainee);
        // exit;
        $CheckHashPassword = Hash::check($request->password, $CheckCandidateOrTrainee["password"]);

        if($CheckCandidateOrTrainee['status'] == 1){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Your account has been disabled. Please contact your admin."
            ));
        }elseif($CheckHashPassword == true){
            $CheckCandidateOrTraineeType = VendorModel::where('email', $request->username)
                                ->first();
            
            if($CheckCandidateOrTraineeType->login_type == 2){
                if (Auth::guard('vendor')->attempt(['email' => $request->username, 'password' => $request->password])) {
                    //Authentication passed...
                    $request->session()->put('VendorName', Auth::guard('vendor')->user()->name);
                    $request->session()->put('VendorEmail', Auth::guard('vendor')->user()->email);
                    $request->session()->put('VendorId', Auth::guard('vendor')->user()->id);
                    return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Login successfully"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please check your credentials..."
                    ));
                }  
            }
            
        }
        else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Please check your credentials..."
            ));
        }
    }
}
