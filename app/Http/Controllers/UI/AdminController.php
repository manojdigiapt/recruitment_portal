<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Model\UI\JobsModel;
use App\Model\UI\CustomerModel;
use App\Model\UI\VendorModel;
use App\Model\UI\DocumentsModel;
use App\Model\UI\AppliedCandidatesModel;
use App\Model\UI\AdminModel;
use App\Model\UI\CandidatesModel;
use App\Model\UI\MasterAdminModel;
use App\Model\UI\RoundsModel;

use DB;
use Session;

class AdminController extends Controller
{
    public function dashboard(){
        if(Auth::guard('super_admin')->check()){
            $title = "TechiTalents Admin Dashboard";
        }else if(Auth::guard('master_admin')->check()){
            $title = "Master Admin Dashboard";
        }

        
        $Customers = CustomerModel::get();
        $Jobs = JobsModel::get();
        $Vendor = VendorModel::get();
        $Candidates = AppliedCandidatesModel::get();

        $GetLastRegisteredCustomers = CustomerModel::take(5)->orderBy('created_at', 'desc')->get();

        $GetLastAddedJobs = DB::table('jobs')
                            ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                            ->join('vendor', 'vendor.id', '=', 'jobs.vendor_id')
                            ->select('customers.name', 'jobs.*', 'vendor.name AS VendorName')
                            ->take(5)->orderBy('jobs.created_at', 'desc')->get();

        $GetVendors = DB::table('vendor')
                            // ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                            ->take(5)->orderBy('vendor.created_at', 'desc')->get();

        // $GetCandidates = DB::table('candidates')
        //                     // ->join('customers', 'customers.id', '=', 'jobs.customer_id')
        //                     ->take(5)->orderBy('candidates.created_at', 'desc')->get();

        $GetCandidates = DB::table('applied_candidates')
                ->select('candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4', 'jobs.job_title', 'applied_candidates.id AS Id', 'applied_candidates.shortlisted_status')
                ->distinct()
                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
                ->join('rounds', 'rounds.applied_candidate_id', '=', 'applied_candidates.id')
                // ->orderBy('created_at', 'desc')
                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
                // ->where('jobs.vendor_id', $GetVendorId)
                // ->where('applied_candidates.shortlisted_status', 1)
                ->get();

        return view("UI.admin.dashboard", compact('title', 'Customers', 'Jobs', 'Vendor', 'Candidates', 'GetLastRegisteredCustomers', 'GetLastAddedJobs', 'GetVendors', 'GetCandidates'));
    }


    public function Master_admin_changepassword(){
        $title = "Change Password";
        
        return view("UI.master_admin.change_password", compact('title'));
    }


    public function customers(){
        $title = "All Customers";
        $GetCustomers = CustomerModel::orderBy('created_at', 'DESC')->get();

        return view("UI.admin.customers", compact('title', 'GetCustomers'));
    }

    public function add_customers(){
        $title = "Add Customers";
        
        return view("UI.admin.add_customers", compact('title'));
    }


    public function edit_customers($Id){
        $title = "Edit Customers";

        $GetCustomers = CustomerModel::where('id', $Id)->first();

        return view("UI.admin.edit_customer", compact('title', 'GetCustomers'));
    }


    public function post_customers(Request $request){
        $GetEmail = $request->email;
        $GetMobile = $request->mobile;

        $CheckEmail = CustomerModel::where('email', $GetEmail)->first();

        $CheckMobile = CustomerModel::where('mobile', $GetMobile)->first();

        // echo json_encode($CheckMobile);
        // exit;

        if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Email id is already exists"
            ));
        }else if($CheckMobile){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Mobile no is already exists"
            ));
        }else{
            $Customers = new CustomerModel();

        // $Jobs->customer_id = $request->cname;
        $Customers->name = $request->name;
        $Customers->address = $request->address;
        $Customers->contact = $request->contact;
        $Customers->email = $request->email;
        $Customers->mobile = $request->mobile;

        $extension = $request->file('agreement')->getClientOriginalExtension();
        $dir = 'agreement_files/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('agreement')->move($dir, $filename);

        $Customers->agreement = $filename;

        $Customers->status = "0";

        $InsertCustomers = $Customers->save();

        if($InsertCustomers){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Customers added"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
        }
        
    }


    public function update_customers(Request $request){
        $Id = $request->Id;

        $Customers = CustomerModel::where('id', $Id)->first();

        // $Jobs->customer_id = $request->cname;
        $Customers->name = $request->name;
        $Customers->address = $request->address;
        $Customers->contact = $request->contact;
        $Customers->email = $request->email;
        $Customers->mobile = $request->mobile;

        $File = $request->agreement;

        if($File == "undefined" || null){
            $filename = $Customers->agreement;
        }else{
            $extension = $request->file('agreement')->getClientOriginalExtension();
            $dir = 'agreement_files/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('agreement')->move($dir, $filename);
            
        }

        $Customers->agreement = $filename;


        $InsertCustomers = $Customers->save();

        if($InsertCustomers){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Customers updated"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
    }


    public function DeleteCustomers($Id){
        $DeleteCustomers = CustomerModel::where('id', $Id)->delete();
        if($DeleteCustomers){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Customers deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }



    public function jobs(){
        $title = "All Jobs";

        $GetJobs = DB::table('jobs')
                ->select('customers.name', 'jobs.*', 'vendor.name AS VendorName')
                ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                ->join('vendor', 'vendor.id', '=', 'jobs.vendor_id')
                ->orderBy('created_at', 'desc')
                ->get();

        return view("UI.admin.jobs", compact('title', 'GetJobs'));
    }

    public function add_jobs(){
        $title = "Add Jobs";

        $GetCustomers = CustomerModel::where('status', 0)->get();
        $GetVendors = VendorModel::where("login_type", 2)->where('status', 0)
                        ->get();

        return view("UI.admin.add_jobs", compact('title', 'GetCustomers', 'GetVendors'));
    }

    public function edit_jobs($Id){
        $title = "Edit Jobs";

        $GetJobs = JobsModel::where('id', $Id)->first();

        $GetCustomers = CustomerModel::get();
        $GetVendors = VendorModel::where("login_type", 2)->get();

        return view("UI.admin.edit_jobs", compact('title', 'GetJobs', 'GetCustomers', 'GetVendors'));
    }


    public function post_jobs(Request $request){
        $Jobs = new JobsModel();

        $Jobs->customer_id = $request->cname;
        $Jobs->vendor_id = $request->vendor_id;
        $Jobs->job_title = $request->title;
        $Jobs->qualification = $request->qualification;
        $Jobs->experience_from = $request->exp_from;
        $Jobs->experience_to = $request->exp_to;
        $Jobs->location = $request->location;
        $Jobs->notice_period = $request->notice_period;
        $Jobs->budget_from = $request->budget_from;
        // $Jobs->budget_to = $request->budget_to;
        $Jobs->gender = $request->gender;
        $Jobs->details = $request->information;
        $Jobs->job_description = $request->description;
        $Jobs->status = "0";

        $InsertJobs = $Jobs->save();

        if($InsertJobs){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Jobs posted"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
    }



    public function update_jobs(Request $request){
        $Id = $request->Id;

        $Jobs = JobsModel::where('id', $Id)->first();

        $Jobs->customer_id = $request->cname;
        $Jobs->vendor_id = $request->vendor_id;
        $Jobs->job_title = $request->title;
        $Jobs->qualification = $request->qualification;
        $Jobs->experience_from = $request->exp_from;
        $Jobs->experience_to = $request->exp_to;
        $Jobs->location = $request->location;
        $Jobs->notice_period = $request->notice_period;
        $Jobs->budget_from = $request->budget_from;
        $Jobs->budget_to = $request->budget_to;
        $Jobs->gender = $request->gender;
        $Jobs->details = $request->information;
        $Jobs->job_description = $request->description;

        $InsertJobs = $Jobs->save();

        if($InsertJobs){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Jobs updated"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
    }



    public function vendor(){
        $title = "All Vendors";
        $GetVendors = VendorModel::orderBy('id', 'desc')->get();

        // echo json_encode($GetVendors);
        // exit;

        return view("UI.admin.vendor", compact('title', 'GetVendors'));
    }

    public function add_vendor(){
        $title = "Add Vendor";

        return view("UI.admin.add_vendor", compact('title'));
    }
        

    public function edit_vendor($Id){
        $title = "Edit Vendor";

        $GetVendor = DB::table('vendor')
                    ->select('documents.id AS DocId', 'documents.pan_card', 'documents.gst_certificate', 'documents.incorporation_certificate', 'documents.agreement', 'vendor.*')
                    ->join('documents', 'documents.id', '=', 'vendor.documents_id')
                    ->where('vendor.id', $Id)
                    ->first();
        
        return view("UI.admin.edit_vendor", compact('title', 'GetVendor'));
    }


    public function post_vendors(Request $request){

        $GetEmail = $request->email;
        $GetMobile = $request->mobile;
        $GetPan = $request->pan;
        $GetGst = $request->gst;

        $CheckEmail = VendorModel::where('email', $GetEmail)->first();

        $CheckMobile = VendorModel::where('mobile', $GetMobile)->first();

        $CheckPan = VendorModel::where('pan_no', $GetPan)->first();

        $CheckGST = VendorModel::where('gst', $GetGst)->first();

        // echo json_encode($request->service_category);
        // exit;

        if($CheckEmail){
            return response()->json(array(
                    "error"=>TRUE,
                    "message" => "Email id is already exists"
            ));
        }elseif($CheckMobile){
            return response()->json(array(
                    "error"=>TRUE,
                    "message" => "Mobile no is already exists"
            ));
        }elseif($CheckPan){
            return response()->json(array(
                    "error"=>TRUE,
                    "message" => "Pan no is already exists"
            ));
        }elseif($CheckGST){
            return response()->json(array(
                    "error"=>TRUE,
                    "message" => "GST no is already exists"
            ));
        }else{
            $Documents = new DocumentsModel();

            $extension = $request->file('pan_copy')->getClientOriginalExtension();
            $dir = 'pan_copy/';
            $filename1 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pan_copy')->move($dir, $filename1);
    
            $Documents->pan_card = $filename1;
    
            $extension = $request->file('gst_certificate')->getClientOriginalExtension();
            $dir = 'gst_certificate/';
            $filename2 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('gst_certificate')->move($dir, $filename2);
    
            $Documents->gst_certificate = $filename2;
    
            $extension = $request->file('company_certificate')->getClientOriginalExtension();
            $dir = 'company_certificate/';
            $filename3 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('company_certificate')->move($dir, $filename3);
    
            $Documents->incorporation_certificate = $filename3;
    
            $extension = $request->file('agreement')->getClientOriginalExtension();
            $dir = 'agreement/';
            $filename4 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('agreement')->move($dir, $filename4);
    
            $Documents->agreement = $filename4;
    
            $InsertDocuments = $Documents->save();
            
            if($InsertDocuments){
                $Vendors = new VendorModel();
    
                // $Jobs->customer_id = $request->cname;
                $Vendors->type = $request->vendor_type;
                $Vendors->service_category = $request->service_category;
                $Vendors->name = $request->cname;
                $Vendors->password = Hash::make($request->password);
                $Vendors->address = $request->address;
                $Vendors->contact = $request->contact;
                $Vendors->email = $request->email;
                $Vendors->mobile = $request->mobile;
                $Vendors->pan_no = $request->pan;
                $Vendors->gst = $request->gst;
                $Vendors->contract_tenure = $request->tenure;
                $Vendors->payment_terms = $request->payment;
                $Vendors->documents_id = $Documents->id;
    
                $Vendors->login_type = "2";
                // $Vendors->status = "0";
                // $Vendors->status = "1";
    
                $InsertVendors = $Vendors->save();
    
                if($InsertVendors){
                    return response()->json(array(
                            "error"=>FALSE,
                            "message" => "Vendors added successfully"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message" => "Failed"
                    ));
                }
            }
        }

        
        
    }



    public function update_vendors(Request $request){
        $GetDocumentsId = $request->doc_id;
        $Documents = DocumentsModel::where('id', $GetDocumentsId)->first();

        $File1 = $request->pan_copy;

        

        if($File1 == "undefined" || null){
            $filename1 = $Documents->pan_card;
        }else{
            $extension = $request->file('pan_copy')->getClientOriginalExtension();
            $dir = 'pan_copy/';
            $filename1 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pan_copy')->move($dir, $filename1);
            
        }

        $Documents->pan_card = $filename1;


        $File2 = $request->gst_certificate;

        if($File2 == "undefined" || null){
            $filename2 = $Documents->gst_certificate;
        }else{
            $extension = $request->file('gst_certificate')->getClientOriginalExtension();
        $dir = 'gst_certificate/';
        $filename2 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('gst_certificate')->move($dir, $filename2);
            
        }

        

        $Documents->gst_certificate = $filename2;

        $File3 = $request->company_certificate;

        if($File3 == "undefined" || null){
            $filename3 = $Documents->incorporation_certificate;
        }else{
            $extension = $request->file('company_certificate')->getClientOriginalExtension();
        $dir = 'company_certificate/';
        $filename3 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('company_certificate')->move($dir, $filename3);
            
        }

        $Documents->incorporation_certificate = $filename3;

        $File4 = $request->agreement;

        if($File4 == "undefined" || null){
            $filename4 = $Documents->agreement;
        }else{
            $extension = $request->file('agreement')->getClientOriginalExtension();
        $dir = 'agreement/';
        $filename4 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('agreement')->move($dir, $filename4);
            
        }

        

        $Documents->agreement = $filename4;

        $InsertDocuments = $Documents->save();
        
        if($InsertDocuments){
            $GetId = $request->Id;

            $Vendors = VendorModel::where('id', $GetId)->first();

            

            // $Jobs->customer_id = $request->cname;
            $Vendors->type = $request->vendor_type;
            $Vendors->service_category = $request->service_category;
            $Vendors->name = $request->cname;
            $Vendors->password = Hash::make($request->password);
            $Vendors->address = $request->address;
            $Vendors->contact = $request->contact;
            $Vendors->email = $request->email;
            $Vendors->mobile = $request->mobile;
            $Vendors->pan_no = $request->pan;
            $Vendors->gst = $request->gst;
            $Vendors->contract_tenure = $request->tenure;
            $Vendors->payment_terms = $request->payment;
            $Vendors->documents_id = $Documents->id;

            if(Auth::guard('super_admin')->check()){
                if($request->password){
                    $Vendors->password = Hash::make($request->password);
                }else{
                    $Vendors->password = $Vendors->password;
                }
            }
            
            // $Vendors->status = "1";

            $InsertVendors = $Vendors->save();

            if($InsertVendors){
                return response()->json(array(
                        "error"=>FALSE,
                        "message" => "Vendors updated"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message" => "Failed"
                ));
            }
        }
        
    }


    public function ChangeStatus($Id){
        // $GetCandidateId = Session::get('CandidateId');
        // $GetStatus = $request->status;

        // $GetId = $request->id;

        $ChangeStatus = CustomerModel::where("id", $Id)
                                ->first();
        // echo json_encode($ChangeStatus);
        // exit;
        if($ChangeStatus['status'] == 0){
            $ChangeStatus['status'] = 1;
            $ChangeStatus->save();
            return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"In Active"
            ));
        }else{
            $ChangeStatus['status'] = 0;
            $ChangeStatus->save();
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Active"
            ));
        }

    }


    public function ChangeJobStatus($Id){
        // $GetCandidateId = Session::get('CandidateId');
        // $GetStatus = $request->status;

        // $GetId = $request->id;

        $ChangeStatus = JobsModel::where("id", $Id)
                                ->first();
        // echo json_encode($ChangeStatus);
        // exit;
        if($ChangeStatus['status'] == 0){
            $ChangeStatus['status'] = 1;
            $ChangeStatus->save();
            return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"In Active"
            ));
        }else{
            $ChangeStatus['status'] = 0;
            $ChangeStatus->save();
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Active"
            ));
        }

    }

    public function Admin(){
        $title = "All Admin";
        $GetAdmin = AdminModel::select('customers.name', 'admin.username', 'admin.password', 'admin.status', 'admin.id')
                            ->join('customers', 'customers.id', '=', 'admin.customer_id')
                            ->get();

        return view("UI.admin.admin", compact('title', 'GetAdmin'));
    }

    public function MasterAdmin(){
        $title = "Master Admins";
        $GetAdmin = MasterAdminModel::get();

        return view("UI.master_admin.master_admin", compact('title', 'GetAdmin'));
    }

    public function add_master_admin(){
        $title = "Add Master Admin";

        return view("UI.master_admin.add_master_admin", compact('title'));
    }

    public function edit_master_admin($Id){
        $title = "Edit Master Admin";

        $GetAdmins = MasterAdminmodel::find($Id);


        return view("UI.master_admin.edit_master_admin", compact('title', 'GetAdmins'));
    }

    public function add_admin(){
        $title = "Add Admin";

        $GetCustomers = CustomerModel::get();

        return view("UI.admin.add_admin", compact('title', 'GetCustomers'));
    }

    public function edit_admin($Id){
        $title = "Edit Admin";

        $GetAdmins = Adminmodel::find($Id);

        $GetCustomers = CustomerModel::get();

        return view("UI.admin.edit_admin", compact('title', 'GetCustomers', 'GetAdmins'));
    }


    public function ChangeVendorStatus($Id){
        // $GetCandidateId = Session::get('CandidateId');
        // $GetStatus = $request->status;

        // $GetId = $request->id;

        $ChangeStatus = VendorModel::where("id", $Id)
                                ->first();
        // echo json_encode($Id);
        // exit;
        if($ChangeStatus['status'] == 0){
            $ChangeStatus['status'] = 1;
            $ChangeStatus->save();
            return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"In Active"
            ));
        }else{
            $ChangeStatus['status'] = 0;
            $ChangeStatus->save();
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Active"
            ));
        }

    }





    public function CandidateDetails($Id){
        $title = "Candidate Details";

        // $GetVendorId = Session::get('VendorId');


        $GetCandidateDetails = $GetCandidates = DB::table('applied_candidates')
            ->select("applied_candidates.id AS Id", "candidates.*", "jobs.job_title AS JobName", "applied_candidates.company_name", "applied_candidates.ctc", "applied_candidates.mode", "applied_candidates.shortlisted_status")
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'rounds.applied_candidate_id', '=', 'applied_candidates.id')
            ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
            ->where('applied_candidates.shortlisted_status', 1)
            ->orderBy('created_at', 'desc')
            // ->join('customers', 'customers.id', '=', 'jobs.customer_id')
            ->first();

        $GetCandidateRoundsDetails = DB::table('applied_candidates')
                ->select('rounds.*')
                ->join('rounds', 'applied_candidates.id', '=', 'rounds.applied_candidate_id')
                ->where('rounds.applied_candidate_id', $Id)
                ->first();
        
        // echo json_encode($GetCandidateDetails);
        // exit;

        return view("UI.admin.candidate_details", compact('title', 'GetCandidateDetails', 'GetCandidateRoundsDetails'));
    }


    public function CheckCandidateFilters(Request $request){
        $InterviewStatus = $request->interview_status;
        // $FinalStatus = $request->final_status;
        // $Feedback = $request->feedback;

        // echo $InterviewStatus;
        // exit;
        $GetVendorId = Session::get('VendorId');

        if($InterviewStatus == 0){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('interview_status', $InterviewStatus)
            // ->where('jobs.vendor_id', $GetVendorId)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 1){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_1', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 2){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_1', 2)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 3){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_1', 3)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 4){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_1', 4)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 5){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_2', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 6){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_2', 2)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 7){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_2', 3)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 8){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_2', 4)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 9){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_3', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 10){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_3', 2)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 11){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_3', 3)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 12){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_3', 4)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 13){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_4', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 14){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_4', 2)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 15){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_3', 3)
            ->orderBy('created_at', 'desc')
            ->get();
        }else if($InterviewStatus == 16){
            $GetData = DB::table('applied_candidates')
            ->select('applied_candidates.*', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4')
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->join('rounds', 'applied_candidates.id', '=', 'rounds.candidate_id')
            // ->where('jobs.vendor_id', $GetVendorId)
            ->where('rounds.interview_stage_4', 4)
            ->orderBy('created_at', 'desc')
            ->get();
        }

        $GetLinks = "";

    foreach($GetData as $Data){

        if(Auth::guard('vendor')->check()){
            $GetLinks = " <a href='/ModifyCandidateDetails/".$Data->id."' target='_blank'>Modify Status</a></td>";
        }elseif(Auth::guard('admin')->check()){
            $GetLinks = "<a href='/CandidateDetails/".$Data->id."' target='_blank'>View Details</a></td>";
        }

            $GetRound1 = "";
            $GetRound2 = "";
            $GetRound3 = "";
            $GetRound4 = "";

            if($Data->Round1 == 1){
                $GetRound1 = "Rejected";
            }elseif($Data->Round1 == 2){
                $GetRound1 = "Shortlisted";
            }elseif($Data->Round1 == 3){
                $GetRound1 = "On Hold";
            }elseif($Data->Round1 == 4){
                $GetRound1 = "Selected";
            }

            if($Data->Round2 == 1){
                $GetRound2 = "Rejected";
            }elseif($Data->Round2 == 2){
                $GetRound2 = "Shortlisted";
            }elseif($Data->Round2 == 3){
                $GetRound2 = "On Hold";
            }elseif($Data->Round2 == 4){
                $GetRound2 = "Selected";
            }

            if($Data->Round3 == 1){
                $GetRound3 = "Rejected";
            }elseif($Data->Round3 == 2){
                $GetRound3 = "Shortlisted";
            }elseif($Data->Round3 == 3){
                $GetRound3 = "On Hold";
            }elseif($Data->Round3 == 4){
                $GetRound3 = "Selected";
            }

            if($Data->Round4 == 1){
                $GetRound4 = "Rejected";
            }elseif($Data->Round4 == 2){
                $GetRound4 = "Shortlisted";
            }elseif($Data->Round4 == 3){
                $GetRound4 = "On Hold";
            }elseif($Data->Round4 == 4){
                $GetRound4 = "Selected";
            }

            $GetResult = "<tr>
                            <td>".$Data->name."</td>
                            <td>".$Data->email."</td>
                            <td>".$Data->mobile."</td>
                            <td>".$GetRound1."</td>
                            <td>".$GetRound2."</td>
                            <td>".$GetRound3."</td>
                            <td>".$GetRound4."</td>
                            <td>".$GetLinks."</td>
                        </tr>";
            
            echo $GetResult;
    }                
            
    }


    public function CheckCandidateFinalStatusFilters(Request $request){
        // $InterviewStatus = $request->interview_status;
        $FinalStatus = $request->final_status;
        // $Feedback = $request->feedback;

        // echo $InterviewStatus;
        // exit;

        if($FinalStatus == 0){
            $GetData = DB::table('applied_candidates')
            ->select("applied_candidates.*")
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            // ->where('final_status', $FinalStatus)
            ->orderBy('created_at', 'desc')
            ->get();
        }else{
            $GetData = DB::table('applied_candidates')
            ->select("applied_candidates.*")
            ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
            ->where('final_status', $FinalStatus)
            ->orderBy('created_at', 'desc')
            ->get();
        }

        
                        
            $GetLinks = "";

        foreach($GetData as $Data){

            if(Auth::guard('vendor')->check()){
                $GetLinks = " <a href='/ModifyCandidateDetails/".$Data->id."' target='_blank'>Modify Status</a></td>";
            }elseif(Auth::guard('admin')->check()){
                $GetLinks = "<a href='/CandidateDetails/".$Data->id."' target='_blank'>View Details</a></td>";
            }

                $GetResult = "<tr>
                                <td>".$Data->name."</td>
                                <td>".$Data->email."</td>
                                <td>".$Data->mobile."</td>
                                <td>".$GetLinks."</td>
                            </tr>";
                
                echo $GetResult;
        }
    }


    public function CheckCandidateFeedbackFilters(Request $request){
        // $InterviewStatus = $request->interview_status;
        // $FinalStatus = $request->final_status;
        $Feedback = $request->feedback;

        // echo $InterviewStatus;
        // exit;

        $GetData = DB::table('applied_candidates')
                    ->select("applied_candidates.*")
                    ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')
                    // ->when($InterviewStatus, function ($query) use ($InterviewStatus) {
                    //     return $query->whereIn('interview_status', $InterviewStatus);
                    // })
                    ->where('feedback', $Feedback)


                    // ->when($FinalStatus, function ($query) use ($FinalStatus) {
                    //     return $query->whereIn('final_status', $FinalStatus);
                    // })

                    // ->when($Feedback, function ($query) use ($Feedback) {
                    //     return $query->whereIn('feedback', $Feedback);
                    // })

                    ->get();
                        
            $GetLinks = "";

        foreach($GetData as $Data){

            if(Auth::guard('vendor')->check()){
                $GetLinks = " <a href='/ModifyCandidateDetails/".$Data->id."' target='_blank'>Modify Status</a></td>";
            }elseif(Auth::guard('admin')->check()){
                $GetLinks = "<a href='/CandidateDetails/".$Data->id."' target='_blank'>View Details</a></td>";
            }

                $GetResult = "<tr>
                                <td>".$Data->name."</td>
                                <td>".$Data->email."</td>
                                <td>".$Data->mobile."</td>
                                <td>".$GetLinks."</td>
                            </tr>";
                
                echo $GetResult;
        }
    }


    public function post_masteradmin(Request $request){
        $GetUsername = $request->name;

        $CheckEmail = MasterAdminModel::where('email', $GetUsername)->first();

        // echo json_encode($CheckMobile);
        // exit;

        if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Username is already exists"
            ));
        }else{
            $Admins = new MasterAdminModel();

        $Admins->email = $request->name;
        $Admins->password = Hash::make($request->password);
        $Admins->login_type = "2";


        $InsertAdmins = $Admins->save();

        if($InsertAdmins){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Master Admin added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
        }
        
    }

    public function Update_master_admin(Request $request){
        $GetId = $request->Id;

        $GetUsername = $request->name;

        $Admins = MasterAdminModel::where('id', $GetId)->first();

        $Admins->email = $request->name;

        if($request->password){
            $Admins->password = Hash::make($request->password);
        }else{
            $Admins->password = $Admins->password;
        }


        $InsertAdmins = $Admins->save();

        if($InsertAdmins){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Master Admin added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
        
    }



    public function post_admin(Request $request){
        $GetUsername = $request->name;

        $CheckEmail = AdminModel::where('username', $GetUsername)->first();

        // echo json_encode($CheckMobile);
        // exit;

        if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Username is already exists"
            ));
        }else{
            $Admins = new AdminModel();

        $Admins->customer_id = $request->customerId;
        $Admins->username = $request->name;
        $Admins->password = Hash::make($request->password);
        // $Admins->status = "1";


        $InsertAdmins = $Admins->save();

        if($InsertAdmins){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Admin added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
        }
        
    }

    public function Update_admin(Request $request){
        $GetId = $request->Id;

        $GetUsername = $request->name;


        // echo json_encode($CheckMobile);
        // exit;

        $Admins = AdminModel::where('id', $GetId)->first();

        $Admins->customer_id = $request->customerId;
        $Admins->username = $request->name;

        if(Auth::guard('super_admin')->check()){
            if($request->password){
                $Admins->password = Hash::make($request->password);
            }else{
                $Admins->password = $Admins->password;
            }
        }
        // $Admins->password = Hash::make($request->password);


        $InsertAdmins = $Admins->save();

        if($InsertAdmins){
            return response()->json(array(
                    "error"=>FALSE,
                    "message" => "Admin added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
        
    }


    public function PostAdminLogin(Request $request){
        $UserName = $request->username;
        $Password = $request->password;

        $CheckCredentials = AdminModel::where('username', $UserName)->first();
        
        // echo json_encode($CheckCredentials);
        // exit;

        if($CheckCredentials['status'] == 1){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Your account is not activate. Please contact your admin."
            ));
        }else if($CheckCredentials){
            $CheckAdminPassword = Hash::check($Password, $CheckCredentials->password);
            
            // echo json_encode($CheckAdminPassword);
            // exit;

            if($CheckAdminPassword == true){
                if (Auth::guard('sub_admin')->attempt(['username' => $request->username, 'password' => $request->password])) {
                    //Authentication passed...
                    $request->session()->put('SubAdminName', Auth::guard('sub_admin')->user()->username);
                    $request->session()->put('SubAdminCustomerId', Auth::guard('sub_admin')->user()->customer_id);
                    $request->session()->put('SubAdminId', Auth::guard('sub_admin')->user()->id);
                    return response()->json(array(
                        "error"=>FALSE,
                        "message"=>"Login successfully"
                    ));
                }else{
                    return response()->json(array(
                        "error"=>TRUE,
                        "message"=>"Please Check your credentials..."
                    ));
                }   
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=> "Please check your credentials."
                ));
            }
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Please check your credentials."
            ));
        }
        
    }


    public function AdminCandiateList($Id){
        $title = "Candidates List";

        $GetCustomerId = Session::get('SubAdminCustomerId');
        
        
        $GetCandidates = DB::table('applied_candidates')    
                        // ->distinct()
                        ->select('candidates.*', 'jobs.job_title', 'applied_candidates.id As Id', 'applied_candidates.shortlisted_status')
                        
                        ->join('jobs', 'applied_candidates.jobs_id', '=', 'jobs.id')
                        ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')
                        // ->join('jobs', 'jobs.vendor_id', '=', 'candidates.vendor_id')

                        ->where('applied_candidates.customer_id', $GetCustomerId)
                        ->where('jobs.id', $Id)
                        ->get();

        // $GetCandidates = DB::select("SELECT DISTINCT candidates.name, jobs.job_title FROM candidates, applied_candidates, jobs WHERE applied_candidates.jobs_id = jobs.id AND applied_candidates.candidate_id = candidates.id AND applied_candidates.customer_id = '$GetCustomerId'");

        // echo json_encode($GetCandidates);
        // exit;

        return view("UI.admin.admin_candidates_list", compact('title', 'GetCandidates'));
    }


    public function ChangeShortlistedStatus($Id){
        // $GetCandidateId = Session::get('CandidateId');
        // $GetStatus = $request->status;

        // $GetId = $request->id;

        $ChangeStatus = AppliedCandidatesModel::where("id", $Id)
                                ->first();
        // // echo json_encode($Id);
        // // exit;
        // if($ChangeStatus['shortlisted_status'] == 0){
            $ChangeStatus['shortlisted_status'] = 1;
            $ChangeStatus->save();

            $CheckRounds = RoundsModel::where('applied_candidate_id', $Id)->first();

            // echo json_encode($CheckRounds);
            // exit;

            if($CheckRounds == null){
                $Rounds = new RoundsModel();
            
                $Rounds->applied_candidate_id = $Id;
                $Rounds->save();
            }

            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Shortlisted"
            ));
            
        // }else{
        //     $ChangeStatus['shortlisted_status'] = 0;
        //     $ChangeStatus->save();
        //     return response()->json(array(
        //         "error"=>TRUE,
        //         "message"=>"Un Shortlisted"
        //     ));
        // }

    }

    public function ChangeRejectedStatus($Id){
        $ChangeStatus = AppliedCandidatesModel::where("id", $Id)
                                ->first();

            $ChangeStatus['shortlisted_status'] = 2;
            $ChangeStatus->save();

            $CheckRounds = RoundsModel::where('applied_candidate_id', $Id)->first();

            // echo json_encode($CheckRounds);
            // exit;

            if($CheckRounds == null){
                $Rounds = new RoundsModel();
            
                $Rounds->applied_candidate_id = $Id;
                $Rounds->save();
            }

            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Rejected"
            ));
            

    }


    public function ChangeMasterAdminStatus($Id){

        $ChangeStatus = MasterAdminModel::where("id", $Id)
                                ->first();
        // echo json_encode($ChangeStatus);
        // exit;
        if($ChangeStatus['status'] == 1){
            $ChangeStatus['status'] = 0;
            $ChangeStatus->save();
            return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Active"
            ));
        }else{
            $ChangeStatus['status'] = 1;
            $ChangeStatus->save();
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"In Active"
            ));
        }

    }


    public function ChangeAdminStatus($Id){

        $ChangeStatus = AdminModel::where("id", $Id)
                                ->first();
        // echo json_encode($ChangeStatus);
        // exit;
        if($ChangeStatus['status'] == 1){
            $ChangeStatus['status'] = 0;
            $ChangeStatus->save();
            return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Active"
            ));
        }else{
            $ChangeStatus['status'] = 1;
            $ChangeStatus->save();
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"In Active"
            ));
        }

    }



    public function Adminjobs(){
        $title = "Jobs";

        $GetCustomerId = Session::get('SubAdminCustomerId');

        // echo $GetCustomerId;
        // exit;

        $GetJobs = DB::table('jobs')
                ->select('customers.name', 'jobs.*')
                ->join('customers', 'customers.id', '=', 'jobs.customer_id')
                ->where('jobs.customer_id', $GetCustomerId)
                ->get();
        
        $GetCandidates = CandidatesModel::get();

        return view("UI.admin.admin_jobs_list", compact('title', 'GetJobs', 'GetCandidates'));
    }



    public function FilterCustomerJobsData(Request $request){
        $GetId = $request->Id;

        $GetJobId = $request->JobsId;
        
        $GetVendorId = Session::get('VendorId');

        if($GetJobId != 0){
            $GetCandidateByCustomer = DB::table('customers')
                                ->distinct()
                                  ->select('candidates.name', 'candidates.email', 'candidates.id', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4',  'applied_candidates.id AS Id', 'jobs.job_title', 'applied_candidates.shortlisted_status')

                                ->join('applied_candidates', 'applied_candidates.customer_id', '=', 'customers.id')

                                ->join('rounds', 'applied_candidates.id', '=', 'rounds.applied_candidate_id')

                                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')

                                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')

                                ->where('applied_candidates.customer_id', $GetId)
                                // ->where('applied_candidates.shortlisted_status', 1)

                                ->where('applied_candidates.jobs_id', $GetJobId)
                                

                                ->get();
        }else{
            $GetCandidateByCustomer = DB::table('customers')
                                ->distinct()
                                  ->select('candidates.name', 'candidates.email', 'candidates.id', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4',  'applied_candidates.id AS Id', 'jobs.job_title', 'applied_candidates.shortlisted_status')

                                ->join('applied_candidates', 'applied_candidates.customer_id', '=', 'customers.id')

                                ->join('rounds', 'applied_candidates.id', '=', 'rounds.applied_candidate_id')

                                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')

                                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')

                                ->where('applied_candidates.customer_id', $GetId)
                                // ->where('applied_candidates.shortlisted_status', 1)

                                ->get();
        }

            // $GetCandidateByCustomer = DB::select("SELECT DISTINCT candidates.name, candidates.email, candidates.id, rounds.interview_stage_1 AS Round1, rounds.interview_stage_2 AS Round2, rounds.interview_stage_3 AS Round3, rounds.interview_stage_4 AS Round4, applied_candidates.id AS Id, jobs.job_title FROM candidates, applied_candidates, rounds, customers, jobs WHERE applied_candidates.customer_id = customers.id AND applied_candidates.id = rounds.applied_candidate_id AND candidates.id = applied_candidates.candidate_id AND applied_candidates.customer_id = $GetId AND applied_candidates.shortlisted_status = 1 AND applied_candidates.jobs_id = jobs.id");

            foreach($GetCandidateByCustomer as $Data){
                if($Data->shortlisted_status == 2){
                    $GetLinks = "<span class='clr-red'>Rejected</span>";
                }elseif(Auth::guard('vendor')->check()){
                    $GetLinks = " <a href='/ModifyCandidateDetails/".$Data->id."' target='_blank'>Modify Status</a></td>";
                }elseif(Auth::guard('admin')->check()){
                    $GetLinks = "<a href='/CandidateDetails/".$Data->id."' target='_blank'>View Details</a></td>";
                }
        
                    $GetRound1 = "";
                    $GetRound2 = "";
                    $GetRound3 = "";
                    $GetRound4 = "";
        
                    if($Data->Round1 == 1){
                        $GetRound1 = "Rejected";
                    }elseif($Data->Round1 == 2){
                        $GetRound1 = "Shortlisted";
                    }elseif($Data->Round1 == 3){
                        $GetRound1 = "On Hold";
                    }elseif($Data->Round1 == 4){
                        $GetRound1 = "Selected";
                    }
        
                    if($Data->Round2 == 1){
                        $GetRound2 = "Rejected";
                    }elseif($Data->Round2 == 2){
                        $GetRound2 = "Shortlisted";
                    }elseif($Data->Round2 == 3){
                        $GetRound2 = "On Hold";
                    }elseif($Data->Round2 == 4){
                        $GetRound2 = "Selected";
                    }
        
                    if($Data->Round3 == 1){
                        $GetRound3 = "Rejected";
                    }elseif($Data->Round3 == 2){
                        $GetRound3 = "Shortlisted";
                    }elseif($Data->Round3 == 3){
                        $GetRound3 = "On Hold";
                    }elseif($Data->Round3 == 4){
                        $GetRound3 = "Selected";
                    }
        
                    if($Data->Round4 == 1){
                        $GetRound4 = "Rejected";
                    }elseif($Data->Round4 == 2){
                        $GetRound4 = "Shortlisted";
                    }elseif($Data->Round4 == 3){
                        $GetRound4 = "On Hold";
                    }elseif($Data->Round4 == 4){
                        $GetRound4 = "Selected";
                    }
        
                    $GetResult = "<tr>
                                    <td>".$Data->name."</td>
                                    <td>".$Data->email."</td>
                                    <td>".$Data->job_title."</td>
                                    <td>".$GetRound1."</td>
                                    <td>".$GetRound2."</td>
                                    <td>".$GetRound3."</td>
                                    <td>".$GetRound4."</td>
                                    <td>".$GetLinks."</td>
                                </tr>";
                    
                    echo $GetResult;
            }
    }


    public function FilterJobsListData(Request $request){
        $GetId = $request->Id;

        $GetJobId = $request->JobsId;
       
        $GetVendorId = Session::get('VendorId');

        if($GetId != 0){
            $GetCandidateByCustomer = DB::table('customers')
                                ->distinct()
                                  ->select('candidates.name', 'candidates.email', 'candidates.id', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4',  'applied_candidates.id AS Id', 'jobs.job_title', 'applied_candidates.shortlisted_status')

                                ->join('applied_candidates', 'applied_candidates.customer_id', '=', 'customers.id')

                                ->join('rounds', 'applied_candidates.id', '=', 'rounds.applied_candidate_id')

                                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')

                                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')

                                ->where('applied_candidates.customer_id', $GetId)
                                
                                // ->where('applied_candidates.shortlisted_status', 1)
                                ->where('applied_candidates.jobs_id', $GetJobId)
                                

                                ->get();
        }else{
            $GetCandidateByCustomer = DB::table('customers')
                                ->distinct()
                                  ->select('candidates.name', 'candidates.email', 'candidates.id', 'rounds.interview_stage_1 AS Round1', 'rounds.interview_stage_2 AS Round2', 'rounds.interview_stage_3 AS Round3', 'rounds.interview_stage_4 AS Round4',  'applied_candidates.id AS Id', 'jobs.job_title', 'applied_candidates.shortlisted_status')

                                ->join('applied_candidates', 'applied_candidates.customer_id', '=', 'customers.id')

                                ->join('rounds', 'applied_candidates.id', '=', 'rounds.applied_candidate_id')

                                ->join('candidates', 'candidates.id', '=', 'applied_candidates.candidate_id')

                                ->join('jobs', 'jobs.id', '=', 'applied_candidates.jobs_id')

                                ->where('applied_candidates.jobs_id', $GetJobId)
                                // ->where('applied_candidates.shortlisted_status', 1)

                                ->get();
        }

        

            foreach($GetCandidateByCustomer as $Data){
                if($Data->shortlisted_status == 2){
                    $GetLinks = "<span class='clr-red'>Rejected</span>";
                }elseif(Auth::guard('vendor')->check()){
                    $GetLinks = " <a href='/ModifyCandidateDetails/".$Data->id."' target='_blank'>Modify Status</a></td>";
                }elseif(Auth::guard('admin')->check()){
                    $GetLinks = "<a href='/CandidateDetails/".$Data->id."' target='_blank'>View Details</a></td>";
                }
        
                    $GetRound1 = "";
                    $GetRound2 = "";
                    $GetRound3 = "";
                    $GetRound4 = "";
        
                    if($Data->Round1 == 1){
                        $GetRound1 = "Rejected";
                    }elseif($Data->Round1 == 2){
                        $GetRound1 = "Shortlisted";
                    }elseif($Data->Round1 == 3){
                        $GetRound1 = "On Hold";
                    }elseif($Data->Round1 == 4){
                        $GetRound1 = "Selected";
                    }
        
                    if($Data->Round2 == 1){
                        $GetRound2 = "Rejected";
                    }elseif($Data->Round2 == 2){
                        $GetRound2 = "Shortlisted";
                    }elseif($Data->Round2 == 3){
                        $GetRound2 = "On Hold";
                    }elseif($Data->Round2 == 4){
                        $GetRound2 = "Selected";
                    }
        
                    if($Data->Round3 == 1){
                        $GetRound3 = "Rejected";
                    }elseif($Data->Round3 == 2){
                        $GetRound3 = "Shortlisted";
                    }elseif($Data->Round3 == 3){
                        $GetRound3 = "On Hold";
                    }elseif($Data->Round3 == 4){
                        $GetRound3 = "Selected";
                    }
        
                    if($Data->Round4 == 1){
                        $GetRound4 = "Rejected";
                    }elseif($Data->Round4 == 2){
                        $GetRound4 = "Shortlisted";
                    }elseif($Data->Round4 == 3){
                        $GetRound4 = "On Hold";
                    }elseif($Data->Round4 == 4){
                        $GetRound4 = "Selected";
                    }
        
                    $GetResult = "<tr>
                                    <td>".$Data->name."</td>
                                    <td>".$Data->email."</td>
                                    <td>".$Data->job_title."</td>
                                    <td>".$GetRound1."</td>
                                    <td>".$GetRound2."</td>
                                    <td>".$GetRound3."</td>
                                    <td>".$GetRound4."</td>
                                    <td>".$GetLinks."</td>
                                </tr>";
                    
                    echo $GetResult;
            }
    }


    public function MasterAdminUpdatePassword(Request $request){
        // $OldPassword = $request->old_password;
        $NewPassword = $request->new_password;
        $ConfirmPassword = $request->confirm_password;

        // $CheckOldPassword = VendorModel::where('password', $OldPassword)->first();
        $GetMasterAdminId = Session::get('MasterAdminId');

        $GetPassword = MasterAdminModel::where('id', $GetMasterAdminId)->first();

        if($NewPassword != $ConfirmPassword){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Password does not match."
            ));
        }else{
            $GetPassword->password = Hash::make($NewPassword);
            $UpdatePassword = $GetPassword->save();

            return response()->json(array(
                "error"=>FALSE,
                "message" => "Password updated successfully."
            ));
        }
    }
}



