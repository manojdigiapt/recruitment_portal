<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class VendorModel extends Authenticatable
{
    protected $guard = 'vendor';

    protected $table = 'vendor';

    protected $fillable = ['id', 'type', 'service_category', 'name', 'password', 'address', 'contact', 'email', 'mobile', 'pan_no', 'gst', 'contract_tenure', 'payment_terms', 'documents_id', 'payment_id'];
}
