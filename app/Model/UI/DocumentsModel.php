<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class DocumentsModel extends Model
{
    protected $table = 'documents';

    protected $fillable = ['id', 'pan_card', 'gst_certificate', 'incorporation_certificate', 'agreement'];
}
