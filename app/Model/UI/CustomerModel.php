<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model
{
    protected $table = 'customers';

    protected $fillable = ['id', 'name', 'address', 'contact', 'email', 'mobile', 'agreement', 'status'];
}
