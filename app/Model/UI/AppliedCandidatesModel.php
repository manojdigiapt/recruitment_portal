<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class AppliedCandidatesModel extends Model
{
    protected $table = 'applied_candidates';

    protected $fillable = ['id', 'jobs_id', 'candidate_id', 'selected_date', 'shortlisted_status', 'company_name', 'ctc', 'mode'];
}
