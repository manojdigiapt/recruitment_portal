<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class JobsModel extends Model
{
    protected $table = 'jobs';

    protected $fillable = ['id', 'customer_id', 'job_title', 'qualification', 'experience_from', 'experience_to', 'location', 'notice_period', 'budget_from', 'budget_to', 'gender', 'details', 'job_description', 'status'];
}
