<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MasterAdminModel extends Authenticatable
{
    protected $guard = 'master_admin';

    protected $table = 'master_admin';

    protected $fillable = ['id', 'email', 'password', 'login_type', 'status'];
}
