<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminModel extends Authenticatable
{
    protected $guard = 'sub_admin';

    protected $table = 'admin';

    protected $fillable = ['id', 'customer_id', 'username', 'password', 'status'];
}
