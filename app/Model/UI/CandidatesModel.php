<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class CandidatesModel extends Model
{
    protected $table = 'candidates';

    protected $fillable = ['id', 'vendor_id', 'name', 'email', 'mobile', 'skills', 'resume'];
}
