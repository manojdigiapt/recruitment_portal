<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class RoundsModel extends Model
{
    protected $table = 'rounds';

    protected $fillable = ['id', 'candidate_id', 'round1', 'interview_stage_1', 'round1_date', 'feedback1', 'round2', 'interview_stage_2', 'round2_date', 'feedback2', 'round3', 'interview_stage_3', 'round3_date', 'feedback3', 'round4', 'interview_stage_4', 'round4_date', 'feedback4'];
}
