$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$('#username').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        var username = $("#username").val();
        var password = $("#password").val();
    
        if(username == ""){
            danger_toast_msg("Please type email address");
            $("#username").focus();
            return false;
        }else if(IsEmail(username)==false){
            danger_toast_msg("Please check your email address");
            $('#username').focus();
            return false;
        }
    
        if(password == ""){
            danger_toast_msg("Please type password");
            $("#password").focus();
            return false;
        }
    
        var VendorLogin = {
            username: username,
            password: password
        }
    
        $.ajax({
            type: "POST",
            url: "/VendorLogin",
            data: VendorLogin,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    // return false;
                    if(data.type == 1){
                        window.location.href="/dashboard";
                        return false;
                    }else if(data.type == 2){
                        window.location.href="/jobs_list";
                        return false;
                    }
                    
                }
            }
        });   
    }
});

$('#password').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        var username = $("#username").val();
        var password = $("#password").val();
    
        if(username == ""){
            danger_toast_msg("Please type email address");
            $("#username").focus();
            return false;
        }else if(IsEmail(username)==false){
            danger_toast_msg("Please check your email address");
            $('#username').focus();
            return false;
        }
    
        if(password == ""){
            danger_toast_msg("Please type password");
            $("#password").focus();
            return false;
        }
    
        var VendorLogin = {
            username: username,
            password: password
        }
    
        $.ajax({
            type: "POST",
            url: "/VendorLogin",
            data: VendorLogin,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    // return false;
                    if(data.type == 1){
                        window.location.href="/dashboard";
                        return false;
                    }else if(data.type == 2){
                        window.location.href="/jobs_list";
                        return false;
                    }
                    
                }
            }
        });   
    }
});


$('#admin_username').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        var username = $("#admin_username").val();
        var password = $("#admin_password").val();
    
        if(username == ""){
            danger_toast_msg("Please type username");
            $("#admin_username").focus();
            return false;
        }
    
        if(password == ""){
            danger_toast_msg("Please type password");
            $("#admin_password").focus();
            return false;
        }
    
        var AdminLogin = {
            username: username,
            password: password
        }
    
        $.ajax({
            type: "POST",
            url: "/AdminLogin",
            data: AdminLogin,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    window.location.href="/Adminjobs";
                    return false;
                }
            }
        });  
    }
});


$('#admin_password').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        var username = $("#admin_username").val();
        var password = $("#admin_password").val();
    
        if(username == ""){
            danger_toast_msg("Please type username");
            $("#admin_username").focus();
            return false;
        }
    
        if(password == ""){
            danger_toast_msg("Please type password");
            $("#admin_password").focus();
            return false;
        }
    
        var AdminLogin = {
            username: username,
            password: password
        }
    
        $.ajax({
            type: "POST",
            url: "/AdminLogin",
            data: AdminLogin,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    window.location.href="/Adminjobs";
                    return false;
                }
            }
        });  
    }
});



$(document).on("click", "#MasterAdminLoginBtn", function(){
    var username = $("#username").val();
    var password = $("#password").val();

    // if(username == ""){
    //     danger_toast_msg("Please type email address");
    //     $("#username").focus();
    //     return false;
    // }

    // if(password == ""){
    //     danger_toast_msg("Please type password");
    //     $("#password").focus();
    //     return false;
    // }

    var MasterAdminLogin = {
        username: username,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/MasterAdminLogin",
        data: MasterAdminLogin,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                // return false;
                // if(data.type == 1){
                //     window.location.href="/dashboard";
                //     return false;
                // }else if(data.type == 2){
                //     window.location.href="/jobs_list";
                //     return false;
                // }
                window.location.href="/dashboard";
                return false;
                
            }
        }
    });
});


$(document).on("click", "#LoginBtn", function(){
    var username = $("#username").val();
    var password = $("#password").val();
    var select_type = $("#select_type").val();

    if(username == ""){
        danger_toast_msg("Please type email address");
        $("#username").focus();
        return false;
    }
    // }else if(IsEmail(username)==false){
    //     danger_toast_msg("Please check your email address");
    //     $('#username').focus();
    //     return false;
    // }

    if(password == ""){
        danger_toast_msg("Please type password");
        $("#password").focus();
        return false;
    }

    if(select_type == "Select Type"){
        danger_toast_msg("Please select type");
        $("#select_type").focus();
        return false;
    }

    var VendorLogin = {
        username: username,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/VendorLogin",
        data: VendorLogin,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                window.location.href="/jobs_list";
                return false;
            }
        }
    });
});



$(document).on("click", "#AdminLoginBtn", function(){
    var username = $("#admin_username").val();
    var password = $("#admin_password").val();

    if(username == ""){
        danger_toast_msg("Please type username");
        $("#admin_username").focus();
        return false;
    }

    if(password == ""){
        danger_toast_msg("Please type password");
        $("#admin_password").focus();
        return false;
    }

    var AdminLogin = {
        username: username,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/AdminLogin",
        data: AdminLogin,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                window.location.href="/Adminjobs";
                return false;
            }
        }
    });
});



function UsersList(id){

    $.ajax({
        type: "POST",
        url: "/UsersList/"+id,
        data: {id: id},
        dataType: "html",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message)
                return false;
            }else{
                $("#UserDetails").html("");
                $("#UserDetails").append(data);
                $("#UserList").modal('open');
                return false;
            }
        }
    });

    
}



$(document).on("click", "#AddJobs", function(){
    var cname = $("#cname :selected").val();
    var vendor_id = $("#vendor_id").val();
    var title = $("#title").val();
    var qualification = $("#qualification").val();
    var exp_from = $("#exp_from").val();
    var exp_to = $("#exp_to").val();
    var location = $("#location").val();
    var notice_period = $("#notice_period").val();
    var budget_from = $("#budget_from").val();
    // var budget_to = $("#budget_to").val();
    var gender = $("#gender :selected").val();
    var information = $("#information").val();
    var description = $("#description").val();


    if(cname == "Select customer"){
        danger_toast_msg("Please select customer name");
        $("#cname").focus();
        return false;
    }

    if(vendor_id == "Select vendor"){
        danger_toast_msg("Please select vendor");
        $("#vendor_id").focus();
        return false;
    }

    if(title == ""){
        danger_toast_msg("Please type title");
        $("#title").focus();
        return false;
    }

    if(qualification == ""){
        danger_toast_msg("Please type qualification");
        $("#qualification").focus();
        return false;
    }

    if(exp_from == ""){
        danger_toast_msg("Please type experience from");
        $("#exp_from").focus();
        return false;
    }

    // if(exp_to == ""){
    //     danger_toast_msg("Please type experience to");
    //     $("#exp_to").focus();
    //     return false;
    // }

    if(location == ""){
        danger_toast_msg("Please type location");
        $("#location").focus();
        return false;
    }

    if(notice_period == ""){
        danger_toast_msg("Please type notice period");
        $("#notice_period").focus();
        return false;
    }else if(notice_period == "0"){
        danger_toast_msg("Please check notice period");
        $("#notice_period").focus();
        return false;
    }else if(notice_period == "00"){
        danger_toast_msg("Please check notice period");
        $("#notice_period").focus();    
        return false;
    }

    if(budget_from == ""){
        danger_toast_msg("Please type budget from");
        $("#budget_from").focus();
        return false;
    }else if(budget_from == "0"){
        danger_toast_msg("Please check ctc budget");
        $("#budget_from").focus();
        return false;
    }else if(budget_from == "00"){
        danger_toast_msg("Please check ctc budget");
        $("#budget_from").focus();    
        return false;
    }

    // if(budget_to == ""){
    //     danger_toast_msg("Please type budget to");
    //     $("#budget_to").focus();
    //     return false;
    // }else if(budget_to == "0"){
    //     danger_toast_msg("Please check ctc budget");
    //     $("#budget_to").focus();
    //     return false;
    // }else if(budget_to == "00"){
    //     danger_toast_msg("Please check ctc budget");
    //     $("#budget_to").focus();    
    //     return false;
    // }

    if(gender == "Select gender"){
        danger_toast_msg("Please select gender");
        $("#gender").focus();
        return false;
    }

    if(information == ""){
        danger_toast_msg("Please type information");
        $("#information").focus();
        return false;
    }

    if(description == ""){
        danger_toast_msg("Please type description");
        $("#description").focus();
        return false;
    }

    // if(parseInt(budget_to) < parseInt(budget_from)){
    //     danger_toast_msg("it can't be less than Budget");
    //     $("#budget_from").focus();
    //     return false;
    // }

    // if(parseInt(budget_to) < parseInt(budget_from)){
    //     danger_toast_msg("it can't be less than Budget");
    //     $("#budget_from").focus();
    //     return false;
    // }

    

    // return false;

    var JobsData = {
        cname: cname,
        vendor_id: vendor_id,
        title: title,
        qualification: qualification,
        exp_from: exp_from,
        exp_to: exp_to,
        location: location,
        notice_period: notice_period,
        budget_from: budget_from,
        // budget_to: budget_to,
        gender: gender,
        information: information,
        description: description
    }

    $.ajax({
        type: "POST",
        url: "/post_jobs",
        data: JobsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                $("#AddJobs").attr('disabled', true);
                // location.reload();
                window.location.href = "/add_jobs";
                return false;
            }
        }
    });
});



$(document).on("click", "#UpdateJobs", function(){
    var Id = $("#Id").val();
    var cname = $("#cname").val();
    var vendor_id = $("#vendor_id").val();
    var title = $("#title").val();
    var qualification = $("#qualification").val();
    var exp_from = $("#exp_from").val();
    var exp_to = $("#exp_to").val();
    var location = $("#location").val();
    var notice_period = $("#notice_period").val();
    var budget_from = $("#budget_from").val();
    var budget_to = $("#budget_to").val();
    var gender = $("#gender").val();
    var information = $("#information").val();
    var description = $("#description").val();


    if(cname == "Select customer"){
        danger_toast_msg("Please select customer name");
        $("#cname").focus();
        return false;
    }

    if(vendor_id == "Select vendor"){
        danger_toast_msg("Please select vendor");
        $("#vendor_id").focus();
        return false;
    }

    if(title == ""){
        danger_toast_msg("Please type title");
        $("#title").focus();
        return false;
    }

    if(qualification == ""){
        danger_toast_msg("Please type qualification");
        $("#qualification").focus();
        return false;
    }

    if(exp_from == ""){
        danger_toast_msg("Please type experience from");
        $("#exp_from").focus();
        return false;
    }

    if(exp_to == ""){
        danger_toast_msg("Please type experience to");
        $("#exp_to").focus();
        return false;
    }

    if(location == ""){
        danger_toast_msg("Please type location");
        $("#location").focus();
        return false;
    }

    if(notice_period == ""){
        danger_toast_msg("Please type notice period");
        $("#notice_period").focus();
        return false;
    }else if(notice_period == "0"){
        danger_toast_msg("Please check notice period");
        $("#notice_period").focus();
        return false;
    }else if(notice_period == "00"){
        danger_toast_msg("Please check notice period");
        $("#notice_period").focus();    
        return false;
    }

    if(budget_from == ""){
        danger_toast_msg("Please type budget from");
        $("#budget_from").focus();
        return false;
    }

    if(budget_to == ""){
        danger_toast_msg("Please type budget to");
        $("#budget_to").focus();
        return false;
    }

    if(gender == "Select gender"){
        danger_toast_msg("Please select gender");
        $("#gender").focus();
        return false;
    }

    if(information == ""){
        danger_toast_msg("Please type information");
        $("#information").focus();
        return false;
    }

    if(description == ""){
        danger_toast_msg("Please type description");
        $("#description").focus();
        return false;
    }

    if(parseInt(budget_to) < parseInt(budget_from)){
        danger_toast_msg("it can't be less than Budget");
        $("#budget_from").focus();
        return false;
    }


    var JobsData = {
        Id: Id,
        cname: cname,
        vendor_id: vendor_id,
        title: title,
        qualification: qualification,
        exp_from: exp_from,
        exp_to: exp_to,
        location: location,
        notice_period: notice_period,
        budget_from: budget_from,
        budget_to: budget_to,
        gender: gender,
        information: information,
        description: description
    }

    $.ajax({
        type: "POST",
        url: "/update_jobs",
        data: JobsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                $("#UpdateJobs").attr('disabled', true);
                // location.reload(true);
                window.location = window.location.href;
                return false;
            }
        }
    });
});





$(document).on("click", "#AddCustomers", function(){
    var name = $("#customer_name").val();
    var email = $("#email").val();
    var contact = $("#contact_name").val();
    var mobile = $("#mobile").val();
    var address = $("#address").val();
    var agreement = $("#agreement").val();

    if(name == ""){
        danger_toast_msg("Name should not empty");
        $("#customer_name").focus();
        return false;
    }

    if(email == ""){
        danger_toast_msg("Please type email address");
        $("#email").focus();
        return false;
    }else if(IsEmail(email)==false){
        danger_toast_msg("Please check your email address");
        $('#email').focus();
        return false;
    }

    if(contact == ""){
        danger_toast_msg("Contact should not empty");
        $("#contact_name").focus();
        return false;
    }

    if(mobile == ""){
        danger_toast_msg("Mobile should not empty");
        $("#mobile").focus();
        return false;
    }

    if(address == ""){
        danger_toast_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    if(agreement == ""){
        danger_toast_msg("Agreement should not empty");
        $("#agreement").focus();
        return false;
    }

    var CustomersData = new FormData();

    CustomersData.append('name', name);
    CustomersData.append('email', email);
    CustomersData.append('contact', contact);
    CustomersData.append('mobile', mobile);
    CustomersData.append('address', address);
    CustomersData.append('agreement', $('#agreement')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/post_customers",
        data: CustomersData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                $("#AddCustomers").attr('disabled', true);
                // location.reload();
                window.location.href = "/add_customers";
                return false;
            }
        }
    });
});



$(document).on("click", "#UpdateCustomers", function(){
    var Id = $("#Id").val();
    var name = $("#customer_name").val();
    var email = $("#email").val();
    var contact = $("#contact_name").val();
    var mobile = $("#mobile").val();
    var address = $("#address").val();
    var agreement = $("#agreement").val();

    if(name == ""){
        danger_toast_msg("Name should not empty");
        $("#customer_name").focus();
        return false;
    }

    if(email == ""){
        danger_toast_msg("Please type email address");
        $("#email").focus();
        return false;
    }else if(IsEmail(email)==false){
        danger_toast_msg("Please check your email address");
        $('#email').focus();
        return false;
    }

    if(contact == ""){
        danger_toast_msg("Contact should not empty");
        $("#contact_name").focus();
        return false;
    }

    if(mobile == ""){
        danger_toast_msg("Mobile should not empty");
        $("#mobile").focus();
        return false;
    }

    if(address == ""){
        danger_toast_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    // if(agreement == ""){
    //     danger_toast_msg("Agreement should not empty");
    //     $("#agreement").focus();
    //     return false;
    // }

    var CustomersData = new FormData();

    CustomersData.append('Id', Id);
    CustomersData.append('name', name);
    CustomersData.append('email', email);
    CustomersData.append('contact', contact);
    CustomersData.append('mobile', mobile);
    CustomersData.append('address', address);
    CustomersData.append('agreement', $('#agreement')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/update_customers",
        data: CustomersData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                $("#UpdateCustomers").attr('disabled', true);
                // location.reload(true);
                window.location = window.location.href;
                return false;
            }
        }
    });
});


function DeleteCustomers(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/DeleteCustomers/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload(true);
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)
}







$(document).on("click", "#AddVendor", function(){
    var vendor_type = $("#vendor_type :selected").val();
    var service_category = $("#service_category :selected").val();
    var cname = $("#cname").val();
    var password = $("#password").val();
    var address = $("#address").val();
    var contact = $("#contact").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var pan = $("#pan").val();
    var gst = $("#gst").val();
    var tenure = $("#tenure").val();
    var payment = $("#payment").val();
    var pan_copy = $("#pan_copy").val();
    var gst_certificate = $("#gst_certificate").val();
    var company_certificate = $("#company_certificate").val();
    var agreement = $("#agreement").val();


    if(vendor_type == "Select Vendor Type"){
        danger_toast_msg("Vendor type should not empty");
        $("#vendor_type").focus();
        return false;
    }

    if(service_category == "Select Category"){
        danger_toast_msg("Category should not empty");
        $("#service_category").focus();
        return false;
    }

    if(cname == ""){
        danger_toast_msg("Customer name should not empty");
        $("#cname").focus();
        return false;
    }

    if(password == ""){
        danger_toast_msg("Password should not empty");
        $("#password").focus();
        return false;
    }

    if(address == ""){
        danger_toast_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    if(contact == ""){
        danger_toast_msg("Contact should not empty");
        $("#contact").focus();
        return false;
    }

    if(email == ""){
        danger_toast_msg("Please type email address");
        $("#email").focus();
        return false;
    }else if(IsEmail(email)==false){
        danger_toast_msg("Please check your email address");
        $('#email').focus();
        return false;
    }

    if(mobile == ""){
        danger_toast_msg("Mobile should not empty");
        $("#mobile").focus();
        return false;
    }

    if(pan == ""){
        danger_toast_msg("Pan should not empty");
        $("#pan").focus();
        return false;
    }

    var regExp = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/; 
    var txtpan = $("#pan").val(); 
    if (txtpan.length == 10 ) { 
     if( txtpan.match(regExp) ){ 
        // danger_toast_msg('PAN match found');
     }
     else {
        danger_toast_msg("Not a valid PAN number");
      event.preventDefault(); 
      $("#pan").focus();
          return false;
     } 
    } 
    else { 
        danger_toast_msg('Please enter 10 digits for a valid PAN number');
          event.preventDefault(); 
          $("#pan").focus();
          return false;
    } 

    if(gst == ""){
        danger_toast_msg("GST should not empty");
        $("#gst").focus();
        return false;
    }

    var inputvalues = $("#gst").val(); 
    var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');

    if (gstinformat.test(inputvalues)) {
    //  return true;
    } else {
        danger_toast_msg('Please enter valid GSTIN number');
        $("#gst").focus();
        return false;
    }


    if(tenure == ""){
        danger_toast_msg("Tenure should not empty");
        $("#tenure").focus();
        return false;
    }

    if(payment == ""){
        danger_toast_msg("Payment should not empty");
        $("#payment").focus();
        return false;
    }


    


    if(pan_copy == ""){
        danger_toast_msg("Pan copy should not empty");
        $("#pan_copy").focus();
        return false;
    }

    if(gst_certificate == ""){
        danger_toast_msg("Gst certificate should not empty");
        $("#gst_certificate").focus();
        return false;
    }

    if(company_certificate == ""){
        danger_toast_msg("Company certificate should not empty");
        $("#company_certificate").focus();
        return false;
    }

    if(agreement == ""){
        danger_toast_msg("Agreement should not empty");
        $("#agreement").focus();
        return false;
    }

    var VendorsData = new FormData();

    VendorsData.append('vendor_type', vendor_type);
    VendorsData.append('service_category', service_category);
    VendorsData.append('cname', cname);
    VendorsData.append('password', password);
    VendorsData.append('address', address);
    VendorsData.append('contact', contact);
    VendorsData.append('email', email);
    VendorsData.append('mobile', mobile);
    VendorsData.append('pan', pan);
    VendorsData.append('gst', gst);
    VendorsData.append('tenure', tenure);
    VendorsData.append('payment', payment);
    VendorsData.append('pan_copy', $('#pan_copy')[0].files[0]);
    VendorsData.append('gst_certificate', $('#gst_certificate')[0].files[0]);
    VendorsData.append('company_certificate', $('#company_certificate')[0].files[0]);
    VendorsData.append('agreement', $('#agreement')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/post_vendors",
        data: VendorsData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        beforeSend: function(){
            $("#AddVendor").attr('disabled', true);
        },
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                $("#AddVendor").attr('disabled', true);
                window.location.href = "/add_vendor";
                return false;
            }
        },
        complete:function(data){
            $("#AddVendor").attr('disabled', false);
        }
        
    });
});



$(document).on("click", "#UpdateVendor", function(){
    var doc_id = $("#doc_id").val();
    var Id = $("#Id").val();

    var vendor_type = $("#vendor_type :selected").val();
    var service_category = $("#service_category :selected").val();

    // var vendor_type = $("#vendor_type").val();
    var cname = $("#cname").val();
    var password = $("#password").val();
    var address = $("#address").val();
    var contact = $("#contact").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var pan = $("#pan").val();
    var gst = $("#gst").val();
    var tenure = $("#tenure").val();
    var payment = $("#payment").val();
    var pan_copy = $("#pan_copy").val();
    var gst_certificate = $("#gst_certificate").val();
    var company_certificate = $("#company_certificate").val();
    var agreement = $("#agreement").val();

    if(vendor_type == "Vendor Type"){
        danger_toast_msg("Vendor type should not empty");
        $("#vendor_type").focus();
        return false;
    }

    if(service_category == "Select Category"){
        danger_toast_msg("Category should not empty");
        $("#service_category").focus();
        return false;
    }

    if(cname == ""){
        danger_toast_msg("Customer name should not empty");
        $("#cname").focus();
        return false;
    }


    if(address == ""){
        danger_toast_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    if(contact == ""){
        danger_toast_msg("Contact should not empty");
        $("#contact").focus();
        return false;
    }

    if(email == ""){
        danger_toast_msg("Please type email address");
        $("#email").focus();
        return false;
    }else if(IsEmail(email)==false){
        danger_toast_msg("Please check your email address");
        $('#email').focus();
        return false;
    }

    if(mobile == ""){
        danger_toast_msg("Mobile should not empty");
        $("#mobile").focus();
        return false;
    }

    if(pan == ""){
        danger_toast_msg("Pan should not empty");
        $("#pan").focus();
        return false;
    }

    if(gst == ""){
        danger_toast_msg("GST should not empty");
        $("#gst").focus();
        return false;
    }

    if(tenure == ""){
        danger_toast_msg("Tenure should not empty");
        $("#tenure").focus();
        return false;
    }

    if(payment == ""){
        danger_toast_msg("Payment should not empty");
        $("#payment").focus();
        return false;
    }


    var regExp = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/; 
    var txtpan = $("#pan").val(); 
    if (txtpan.length == 10 ) { 
     if( txtpan.match(regExp) ){ 
        // danger_toast_msg('PAN match found');
     }
     else {
        danger_toast_msg("Not a valid PAN number");
      event.preventDefault(); 
      $("#pan").focus();
          return false;
     } 
    } 
    else { 
        danger_toast_msg('Please enter 10 digits for a valid PAN number');
          event.preventDefault(); 
          $("#pan").focus();
          return false;
    } 


    // if(pan_copy == ""){
    //     danger_toast_msg("Pan copy should not empty");
    //     $("#pan_copy").focus();
    //     return false;
    // }

    // if(gst_certificate == ""){
    //     danger_toast_msg("Gst certificate should not empty");
    //     $("#gst_certificate").focus();
    //     return false;
    // }

    // if(company_certificate == ""){
    //     danger_toast_msg("Company certificate should not empty");
    //     $("#company_certificate").focus();
    //     return false;
    // }

    // if(agreement == ""){
    //     danger_toast_msg("Agreement should not empty");
    //     $("#agreement").focus();
    //     return false;
    // }


    var VendorsData = new FormData();

    VendorsData.append('doc_id', doc_id);
    VendorsData.append('Id', Id);
    
    VendorsData.append('vendor_type', vendor_type);
    VendorsData.append('service_category', service_category);
    VendorsData.append('cname', cname);
    VendorsData.append('password', password);
    VendorsData.append('address', address);
    VendorsData.append('contact', contact);
    VendorsData.append('email', email);
    VendorsData.append('mobile', mobile);
    VendorsData.append('pan', pan);
    VendorsData.append('gst', gst);
    VendorsData.append('tenure', tenure);
    VendorsData.append('payment', payment);
    VendorsData.append('pan_copy', $('#pan_copy')[0].files[0]);
    VendorsData.append('gst_certificate', $('#gst_certificate')[0].files[0]);
    VendorsData.append('company_certificate', $('#company_certificate')[0].files[0]);
    VendorsData.append('agreement', $('#agreement')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/update_vendors",
        data: VendorsData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                $("#UpdateVendor").attr('disabled', true);
                // location.reload(true);
                window.location = window.location.href;
                return false;
            }
        }
    });
});



function ViewDetails(id){
    $("#JobId").val(id);

}


$(document).on("click", "#AddCandidates", function(){

    var status = navigator.onLine;
    if (status) {
        // var CustomerId = $("#CustomerId").val();
        var name = $("#name").val();
        var email = $("#email").val();
        var mobile = $("#mobile").val();
        var skills = $("#skills").val();
        var current_company = $("#current_company").val();
        var current_ctc = $("#current_ctc").val();
        var notice_period = $("#notice_period").val();
        var resume = $("#resume").val();

        // if(CustomerId == ""){
        //     danger_toast_msg("Customer id should not empty");
        //     $("#CustomerId").focus();
        //     return false;
        // }

        if(name == ""){
            danger_toast_msg("Candidate name should not empty");
            $("#name").focus();
            return false;
        }

        if(email == ""){
            danger_toast_msg("Please type email address");
            $("#email").focus();
            return false;
        }else if(IsEmail(email)==false){
            danger_toast_msg("Please check your email address");
            $('#email').focus();
            return false;
        }

        if(mobile == ""){
            danger_toast_msg("Mobile number should not empty");
            $("#mobile").focus();
            return false;
        }

        if(skills == ""){
            danger_toast_msg("Skills should not empty");
            $("#skills").focus();
            return false;
        }

        if(current_company == ""){
            danger_toast_msg("Current Company should not empty");
            $("#current_company").focus();
            return false;
        }

        if(current_ctc == ""){
            danger_toast_msg("Current CTC should not empty");
            $("#current_ctc").focus();
            return false;
        }

        if(notice_period == ""){
            danger_toast_msg("Notice period should not empty");
            $("#notice_period").focus();
            return false;
        }

        if(resume == ""){
            danger_toast_msg("Resume should not empty");
            $("#resume").focus();
            return false;
        }


        var CandidatesData = new FormData();

        // CandidatesData.append('CustomerId', CustomerId);
        CandidatesData.append('name', name);
        CandidatesData.append('email', email);
        CandidatesData.append('mobile', mobile);
        CandidatesData.append('skills', skills);
        CandidatesData.append('current_company', current_company);
        CandidatesData.append('current_ctc', current_ctc);
        CandidatesData.append('notice_period', notice_period);
        CandidatesData.append('resume', $('#resume')[0].files[0]);

        $.ajax({
            type: "POST",
            url: "/post_candidates",
            data: CandidatesData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    location.reload(true);
                    return false;
                }
            }
        });
    } else{
        danger_toast_msg("Please check your internet connection.");
        // return false;
    }

});


$(document).on("click", "#UpdateCandidates", function(){
    var Id = $("#Id").val();
    var name = $("#name").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var skills = $("#skills").val();
    var current_company = $("#current_company").val();
    var current_ctc = $("#current_ctc").val();
    var notice_period = $("#notice_period").val();
    var resume = $("#resume").val();

    // if(CustomerId == ""){
    //     danger_toast_msg("Customer id should not empty");
    //     $("#CustomerId").focus();
    //     return false;
    // }

    if(name == ""){
        danger_toast_msg("Candidate name should not empty");
        $("#name").focus();
        return false;
    }

    if(email == ""){
        danger_toast_msg("Please type email address");
        $("#email").focus();
        return false;
    }else if(IsEmail(email)==false){
        danger_toast_msg("Please check your email address");
        $('#email').focus();
        return false;
    }

    if(mobile == ""){
        danger_toast_msg("Mobile number should not empty");
        $("#mobile").focus();
        return false;
    }

    if(skills == ""){
        danger_toast_msg("Skills should not empty");
        $("#skills").focus();
        return false;
    }

    if(current_company == ""){
        danger_toast_msg("Current Company should not empty");
        $("#current_company").focus();
        return false;
    }

    if(current_ctc == ""){
        danger_toast_msg("Current CTC should not empty");
        $("#current_ctc").focus();
        return false;
    }

    if(notice_period == ""){
        danger_toast_msg("Notice period should not empty");
        $("#notice_period").focus();
        return false;
    }




    var CandidatesData = new FormData();

    CandidatesData.append('Id', Id);
    CandidatesData.append('name', name);
    CandidatesData.append('email', email);
    CandidatesData.append('mobile', mobile);
    CandidatesData.append('skills', skills);
    CandidatesData.append('current_company', current_company);
    CandidatesData.append('current_ctc', current_ctc);
    CandidatesData.append('notice_period', notice_period);
    CandidatesData.append('resume', $('#resume')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/update_candidates",
        data: CandidatesData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload(true);
                return false;
            }
        }
    });
});






$(document).on("click", "#AddCandidateDetails", function(){
    var Id = $("#Id").val();
    var job_name = $("#job_title").val();
    var name = $("#name").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var schedule = $("#schedule").val();
    var feedback = $("#feedback").val();
    // var round1 = $("#round1").val();
    // var round2 = $("#round2").val();
    // var round3 = $("#round3").val();
    // var stage = $("#stage").val();
    // var final_status = $("#final_status").val();
    var company_name = $("#company_name").val();
    var ctc = $("#ctc").val();
    var mode = $("#mode").val();

    if($('#round1').is(':checked')){
        var round1 = "1";
    }else{
        var round1 = "0";
    }

    if($('#stage1').is(':checked')){
        var stage1 = "1";
    }else if($('#stage2').is(':checked')){
        var stage1 = "2";
    }else if($('#stage3').is(':checked')){
        var stage1 = "3";
    }else if($('#stage4').is(':checked')){
        var stage1 = "4";
    }

    var schedule1 = $("#schedule1").val();
    var interview_mode1 = $("#interview_mode1").val();
    var feedback1 = $("#feedback1").val();

    if($('#round2').is(':checked')){
        var round2 = "1";
    }else{
        var round2 = "0";
    }

    if($('#stage5').is(':checked')){
        var stage2 = "1";
    }else if($('#stage6').is(':checked')){
        var stage2 = "2";
    }else if($('#stage7').is(':checked')){
        var stage2 = "3";
    }else if($('#stage8').is(':checked')){
        var stage2 = "4";
    }

    var schedule2 = $("#schedule2").val();
    var interview_mode2 = $("#interview_mode2").val();
    var feedback2 = $("#feedback2").val();

    if($('#round3').is(':checked')){
        var round3 = "1";
    }else{
        var round3 = "0";
    }

    if($('#stage9').is(':checked')){
        var stage3 = "1";
    }else if($('#stage10').is(':checked')){
        var stage3 = "2";
    }else if($('#stage11').is(':checked')){
        var stage3 = "3";
    }else if($('#stage12').is(':checked')){
        var stage3 = "4";
    }

    var schedule3 = $("#schedule3").val();
    var interview_mode3 = $("#interview_mode3").val();
    var feedback3 = $("#feedback3").val();

    if($('#round4').is(':checked')){
        var round4 = "1";
    }else{
        var round4 = "0";
    }

    if($('#stage13').is(':checked')){
        var stage4 = "1";
    }else if($('#stage14').is(':checked')){
        var stage4 = "2";
    }else if($('#stage15').is(':checked')){
        var stage4 = "3";
    }else if($('#stage16').is(':checked')){
        var stage4 = "4";
    }

    var schedule4 = $("#schedule4").val();
    var interview_mode4 = $("#interview_mode4").val();
    var feedback4 = $("#feedback4").val();
    
    // if(ctc == ""){
    //     danger_toast_msg("Please check ctc");
    //     $("#ctc").focus();
    //     return false;
    // }else if(ctc == "0"){
    //     danger_toast_msg("Please check ctc");
    //     $("#ctc").focus();
    //     return false;
    // }else if(ctc == "00"){
    //     danger_toast_msg("Please check ctc");
    //     $("#ctc").focus();    
    //     return false;
    // }
    // alert(stage1);
    // return false;
    // return false;
    // var arr = $('.interview_stages:checked').map(function () {
    //     return this.value;
    // }).get();
    // console.log(arr);

    var CandidateData = {
        Id: Id,
        name: name,
        email: email,
        mobile: mobile,
        // schedule: schedule,
        // feedback: feedback,
        round1: round1,
        stage1: stage1,
        schedule1: schedule1,
        interview_mode1: interview_mode1,
        feedback1: feedback1,
        round2: round2,
        stage2: stage2,
        schedule2: schedule2,
        interview_mode2: interview_mode2,
        feedback2: feedback2,
        round3: round3,
        stage3: stage3,
        schedule3: schedule3,
        interview_mode3: interview_mode3,
        feedback3: feedback3,
        round4: round4,
        stage4: stage4,
        schedule4: schedule4,
        interview_mode4: interview_mode4,
        feedback4: feedback4,
        // stage: stage,
        // final_status: final_status,
        company_name: company_name,
        ctc: ctc,
        mode: mode
    }

    $.ajax({
        type: "POST",
        url: "/UpdateCandidateDetails",
        data: CandidateData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload(true);
                return false;
            }
        }
    });
});



function ChangeStatus(id){
    
    $.ajax({
        type: "POST",
        url: "/ChangeStatus/"+id,
        // data: {status: status},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                success_toast_msg(data.message);
                return false;
            }else{
                danger_toast_msg(data.message);
                return false;
            }
        }
    });
}


function ChangeJobStatus(id){
    
    $.ajax({
        type: "POST",
        url: "/ChangeJobStatus/"+id,
        // data: {status: status},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                success_toast_msg(data.message);
                return false;
            }else{
                danger_toast_msg(data.message);
                return false;
            }
        }
    });
}


function ChangeVendorStatus(id){
    
    $.ajax({
        type: "POST",
        url: "/ChangeVendorStatus/"+id,
        // data: {status: status},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                success_toast_msg(data.message);
                return false;
            }else{
                danger_toast_msg(data.message);
                return false;
            }
        }
    });
}


// $("input[name='round1']").click(function () {
                                   
//     var radio_value = $(this).val();
//     if(radio_value=='1') {
//         $("#Round2Row").addClass('round-row-disable');
//         $("#Round3Row").addClass('round-row-disable');
//         $("#Round4Row").addClass('round-row-disable');
//     } else if(radio_value=='2') {
//         $("#Round2Row").removeClass('round-row-disable');
//         $("#Round3Row").removeClass('round-row-disable');
//         $("#Round4Row").removeClass('round-row-disable');
//     } else if(radio_value=='3') {
//         $("#Round2Row").removeClass('round-row-disable');
//         $("#Round3Row").removeClass('round-row-disable');
//         $("#Round4Row").removeClass('round-row-disable');
//     } else if(radio_value=='4') {
//         $("#Round2Row").addClass('round-row-disable');
//         $("#Round3Row").addClass('round-row-disable');
//         $("#Round4Row").addClass('round-row-disable');
//     }
   
// });

$("input[name='round1']").click(function () {
                                   
    var radio_value = $(this).val();
    if(radio_value=='1') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule1").val();
            var InterviewMode = $("#interview_mode1 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule1").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode1").val();
                return false;
            }

            if(!$("input:radio[name='round1']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }
            // $("#Round1Row").addClass('round-row-disable');
            // $("#Round2Row").removeClass('round-row-disable');
            $("#round1").attr('disabled', true); 
        }
        
    } else if(radio_value=='2') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule1").val();
            var InterviewMode = $("#interview_mode1 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule1").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode1").val();
                return false;
            }

            if(!$("input:radio[name='round1']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }
            // $("#Round1Row").addClass('round-row-disable');
            // $("#Round2Row").removeClass('round-row-disable');
            $("#round1").removeAttr('disabled', true); 
        }
        
    } else if(radio_value=='3') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule1").val();
            var InterviewMode = $("#interview_mode1 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule1").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode1").val();
                return false;
            }

            if(!$("input:radio[name='round1']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }
            // $("#Round1Row").addClass('round-row-disable');
            // $("#Round2Row").removeClass('round-row-disable');
            $("#round1").removeAttr('disabled', true);
        }
         
    } else if(radio_value=='4') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule1").val();
            var InterviewMode = $("#interview_mode1 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule1").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode1").val();
                return false;
            }

            if(!$("input:radio[name='round1']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }
            // $("#Round1Row").addClass('round-row-disable');
            // $("#Round2Row").removeClass('round-row-disable');
            $("#round1").attr('disabled', true); 
        }
        
    }
   
});


$("input[name='round2']").click(function () {
                                   
    var radio_value = $(this).val();
    if(radio_value=='1') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule2").val();
            var InterviewMode = $("#interview_mode2 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule2").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode2").val();
                return false;
            }

            if(!$("input:radio[name='round2']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }

            $("#round2").attr('disabled', true); 
        }
        
    } else if(radio_value=='2') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule2").val();
            var InterviewMode = $("#interview_mode2 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule2").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode2").val();
                return false;
            }

            if(!$("input:radio[name='round2']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }

            $("#round2").removeAttr('disabled', true);
        }
         
    } else if(radio_value=='3') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule2").val();
            var InterviewMode = $("#interview_mode2 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule2").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode2").val();
                return false;
            }

            if(!$("input:radio[name='round2']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }

            $("#round2").removeAttr('disabled', true); 
        }
        
    } else if(radio_value=='4') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule2").val();
            var InterviewMode = $("#interview_mode2 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule2").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode2").val();
                return false;
            }

            if(!$("input:radio[name='round2']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }

            $("#round2").attr('disabled', true); 
        }
        
    }
   
});

$("input[name='round3']").click(function () {
                                   
    var radio_value = $(this).val();
    if(radio_value=='1') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule3").val();
            var InterviewMode = $("#interview_mode3 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule3").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode3").val();
                return false;
            }

            if(!$("input:radio[name='round3']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }

            $("#round3").attr('disabled', true); 
        }
        
    } else if(radio_value=='2') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule3").val();
            var InterviewMode = $("#interview_mode3 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule3").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode3").val();
                return false;
            }

            if(!$("input:radio[name='round3']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }

            $("#round3").removeAttr('disabled', true); 
        }
        
    } else if(radio_value=='3') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule3").val();
            var InterviewMode = $("#interview_mode3 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule3").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode3").val();
                return false;
            }

            if(!$("input:radio[name='round3']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }

            $("#round3").removeAttr('disabled', true); 
        }
        
    } else if(radio_value=='4') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule3").val();
            var InterviewMode = $("#interview_mode3 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule3").val();
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode3").val();
                return false;
            }

            if(!$("input:radio[name='round3']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }

            $("#round3").attr('disabled', true); 
        }
        
    }
   
});

$("input[name='round4']").click(function () {
                                   
    var radio_value = $(this).val();
    if(radio_value=='1') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule4").val();
            var InterviewMode = $("#interview_mode4 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule4").val();  
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode4").val();
                return false;
            }

            if(!$("input:radio[name='round4']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }
            $("#round4").attr('disabled', true); 
        }
        
    } else if(radio_value=='2') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule4").val();
            var InterviewMode = $("#interview_mode4 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule4").val();  
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode4").val();
                return false;
            }

            if(!$("input:radio[name='round4']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }
            $("#round4").removeAttr('disabled', true); 
        }
        
    } else if(radio_value=='3') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule4").val();
            var InterviewMode = $("#interview_mode4 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule4").val();  
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode4").val();
                return false;
            }

            if(!$("input:radio[name='round4']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }
            $("#round4").removeAttr('disabled', true); 
        }
        
    } else if(radio_value=='4') {
        if($(this).prop("checked") == true){
            var InterviewDate = $("#schedule4").val();
            var InterviewMode = $("#interview_mode4 :selected").val();
            
            if(InterviewDate == ""){
                danger_toast_msg("Please select interview date");
                $("#schedule4").val();  
                return false;
            }

            if(InterviewMode == "Interview Mode"){
                danger_toast_msg("Please select interview mode");
                $("#interview_mode4").val();
                return false;
            }

            if(!$("input:radio[name='round4']").is(":checked")) {
                danger_toast_msg("Please select interview status");
                // $("#interview_mode1").val();
                return false;
            }
            $("#round4").attr('disabled', true);  
        }
        
    }
   
});




// $("input[name='round2']").click(function () {
                                   
//     var radio_value = $(this).val();
//     if(radio_value=='1') {
//         $("#Round3Row").addClass('round-row-disable');
//         $("#Round4Row").addClass('round-row-disable');
//     } else if(radio_value=='2') {
//         $("#Round3Row").removeClass('round-row-disable');
//         $("#Round4Row").removeClass('round-row-disable');
//     } else if(radio_value=='3') {
//         $("#Round3Row").removeClass('round-row-disable');
//         $("#Round4Row").removeClass('round-row-disable');
//     } else if(radio_value=='4') {
//         $("#Round3Row").addClass('round-row-disable');
//         $("#Round4Row").addClass('round-row-disable');
//     }
   
// });

// $("input[name='round3']").click(function () {
                                   
//     var radio_value = $(this).val();
//     if(radio_value=='1') {
//         $("#Round4Row").addClass('round-row-disable');
//     } else if(radio_value=='2') {
//         $("#Round4Row").removeClass('round-row-disable');
//     } else if(radio_value=='3') {
//         $("#Round4Row").removeClass('round-row-disable');
//     } else if(radio_value=='4') {
//         $("#Round4Row").addClass('round-row-disable');
//     }
   
// });








function CheckCandidateInterviewVendorFilters(){
    var interview_status = $("#interview_status").val();
    // var final_status = $("#final_status").val();
    // var feedback = $("#feedback").val();


    var CandidateData = {
        interview_status: interview_status
        // final_status: final_status,
        // feedback: feedback
    }

    $.ajax({
        type: "POST",
        url: "/CheckCandidateVedorFilters",
        data: CandidateData,
        dataType: "html",
        success: function (data) {
            $("#CandidateDetails").html("");
            $("#CandidateDetails").append(data);
            return false;
        }
    });
}


function CheckCandidateInterviewAdminFilters(){
    var interview_status = $("#interview_status").val();
    // var final_status = $("#final_status").val();
    // var feedback = $("#feedback").val();


    var CandidateData = {
        interview_status: interview_status
        // final_status: final_status,
        // feedback: feedback
    }

    $.ajax({
        type: "POST",
        url: "/CheckCandidateAdminFilters",
        data: CandidateData,
        dataType: "html",
        success: function (data) {
            $("#CandidateDetails").html("");
            $("#CandidateDetails").append(data);
            return false;
        }
    });
}


function CheckCandidateFinalStatusFilters(){
    // var interview_status = $("#interview_status").val();
    var final_status = $("#final_status").val();
    // var feedback = $("#feedback").val();


    var CandidateData = {
        // interview_status: interview_status,
        final_status: final_status
        // feedback: feedback
    }

    $.ajax({
        type: "POST",
        url: "/CheckCandidateFinalStatusFilters",
        data: CandidateData,
        dataType: "html",
        success: function (data) {
            $("#CandidateDetails").html("");
            $("#CandidateDetails").append(data);
            return false;
        }
    });
}


function CheckCandidateFeedbackFilters(){
    // var interview_status = $("#interview_status").val();
    // var final_status = $("#final_status").val();
    var feedback = $("#feedback").val();


    var CandidateData = {
        // interview_status: interview_status,
        // final_status: final_status
        feedback: feedback
    }

    $.ajax({
        type: "POST",
        url: "/CheckCandidateFeedbackFilters",
        data: CandidateData,
        dataType: "html",
        success: function (data) {
            $("#CandidateDetails").html("");
            $("#CandidateDetails").append(data);
            return false;
        }
    });
}



$(document).ready(function(){
    // Check
    if($("input#round1").prop("checked") == true){
        
        $("#Round2Row").removeClass('round-row-disable');
    }
    else if($("input#round1").prop("checked") == false){
        $("#Round2Row").addClass('round-row-disable');
    }

    if($("input#round2").prop("checked") == true){
        $("#Round3Row").removeClass('round-row-disable');
    }
    else if($("input#round2").prop("checked") == false){
        $("#Round3Row").addClass('round-row-disable');
    }

    if($("input#round3").prop("checked") == true){
        $("#Round4Row").removeClass('round-row-disable');
    }
    else if($("input#round3").prop("checked") == false){
        $("#Round4Row").addClass('round-row-disable');
    }


    // Click
    // $('input#round1').click(function(){
    //     if($(this).prop("checked") == true){
    //         var InterviewDate = $("#schedule1").val();
    //         var InterviewMode = $("#interview_mode1 :selected").val();
            
    //         if(InterviewDate == ""){
    //             danger_toast_msg("Please select interview date");
    //             $("#schedule1").val();
    //             return false;
    //         }

    //         if(InterviewMode == "Interview Mode"){
    //             danger_toast_msg("Please select interview mode");
    //             $("#interview_mode1").val();
    //             return false;
    //         }

    //         if(!$("input:radio[name='round1']").is(":checked")) {
    //             danger_toast_msg("Please select interview status");
    //             // $("#interview_mode1").val();
    //             return false;
    //         }
    //         // $("#Round1Row").addClass('round-row-disable');
    //         $("#Round2Row").removeClass('round-row-disable');
    //     }
    //     else if($(this).prop("checked") == false){
    //         $("#Round2Row").addClass('round-row-disable');
    //     }
    // });

    // $('input#round2').click(function(){
    //     if($(this).prop("checked") == true){
    //         var InterviewDate = $("#schedule2").val();
    //         var InterviewMode = $("#interview_mode2 :selected").val();
            
    //         if(InterviewDate == ""){
    //             danger_toast_msg("Please select interview date");
    //             $("#schedule2").val();
    //             return false;
    //         }

    //         if(InterviewMode == "Interview Mode"){
    //             danger_toast_msg("Please select interview mode");
    //             $("#interview_mode2").val();
    //             return false;
    //         }

    //         if(!$("input:radio[name='round2']").is(":checked")) {
    //             danger_toast_msg("Please select interview status");
    //             // $("#interview_mode1").val();
    //             return false;
    //         }

    //         $("#Round3Row").removeClass('round-row-disable');
    //     }
    //     else if($(this).prop("checked") == false){
    //         $("#Round3Row").addClass('round-row-disable');
    //     }
    // });

    // $('input#round3').click(function(){
    //     if($(this).prop("checked") == true){
    //         var InterviewDate = $("#schedule3").val();
    //         var InterviewMode = $("#interview_mode3 :selected").val();
            
    //         if(InterviewDate == ""){
    //             danger_toast_msg("Please select interview date");
    //             $("#schedule3").val();
    //             return false;
    //         }

    //         if(InterviewMode == "Interview Mode"){
    //             danger_toast_msg("Please select interview mode");
    //             $("#interview_mode3").val();
    //             return false;
    //         }

    //         if(!$("input:radio[name='round3']").is(":checked")) {
    //             danger_toast_msg("Please select interview status");
    //             // $("#interview_mode1").val();
    //             return false;
    //         }

    //         $("#Round4Row").removeClass('round-row-disable');
    //     }
    //     else if($(this).prop("checked") == false){
    //         $("#Round4Row").addClass('round-row-disable');
    //     }
    // });


    // $('input#round4').click(function(){
    //     if($(this).prop("checked") == true){
    //         var InterviewDate = $("#schedule4").val();
    //         var InterviewMode = $("#interview_mode4 :selected").val();
            
    //         if(InterviewDate == ""){
    //             danger_toast_msg("Please select interview date");
    //             $("#schedule4").val();  
    //             return false;
    //         }

    //         if(InterviewMode == "Interview Mode"){
    //             danger_toast_msg("Please select interview mode");
    //             $("#interview_mode4").val();
    //             return false;
    //         }

    //         if(!$("input:radio[name='round4']").is(":checked")) {
    //             danger_toast_msg("Please select interview status");
    //             // $("#interview_mode1").val();
    //             return false;
    //         }

    //     }
    // });
});




function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
       return false;
    }else{
       return true;
    }
  }

  function validatePhone(txtPhone) {
    var a = document.getElementById(txtPhone).value;
    var filter = /[1-9]{1}[0-9]{9}/;
    if (filter.test(a)) {
        return true;
    }
    else {
        return false;
    }
}



function success_toast_msg(data){
    var toast = new iqwerty.toast.Toast();
toast
.setText(data)
.stylize({
  background: '#37BFA7',
  color: 'white',
  'box-shadow': '0 0 50px rgba(0, 0, 0, .7)'
})
.show();
}

function danger_toast_msg(data){
    var toast = new iqwerty.toast.Toast();
toast
.setText(data)
.stylize({
  background: 'red',
  color: 'white',
  'box-shadow': '0 0 50px rgba(0, 0, 0, .7)'
})
.show();
}


$('#mobile').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});

$('#exp_from').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});

$('#exp_to').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});

$('#notice_period').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});

$('#budget_from').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});

$('#budget_to').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});



// $("#cname").keypress(function(event){
//     var inputValue = event.charCode;
//     if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
//         event.preventDefault();
//     }
// });


$(document).ready(function(){
    // $('#contact_name').bind('keypress',function(evt){
    //     var key = String.fromCharCode(evt.which || evt.charCode);
    //     if(/[a-z]/i.test(key) === false) evt.preventDefault();
    //   })
      
      $("#contact_name").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });

      $("#name").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });

    // $("#contact").keypress(function(event){
    //     var inputValue = event.charCode;
    //     if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
    //         event.preventDefault();
    //     }
    // });

    $("#cname").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });

    // $('#contact').bind('keypress',function(evt){
    //     var key = String.fromCharCode(evt.which || evt.charCode);
    //     if(/^[a-zA-Z\s]+$/i.test(key) === false) evt.preventDefault();
    //   })

    // $("#qualification").keypress(function(event){
    //     var inputValue = event.charCode;
    //     if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
    //         event.preventDefault();
    //     }
    // });

    //   $('#qualification').bind('keypress',function(evt){
    //     var key = String.fromCharCode(evt.which || evt.charCode);
    //     if(/[a-z]/i.test(key) === false) evt.preventDefault();
    //   })
  });



$('#budget_from').keyup(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;
  
    // format number
    $(this).val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      ;
    });
  });

  $('#budget_to').keyup(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;
  
    // format number
    $(this).val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      ;
    });
  });


  $('#current_ctc').keyup(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;
  
    // format number
    $(this).val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      ;
    });
  });

  $(document).on('click', "#ClearRound1", function(){
    $('input#round1').removeAttr('checked');
    $('input[name="round1"]').prop('checked', false);
    $("#feedback1").val("");
    $('input#schedule1')[0].value = 0;
    return false;
  });


  $(document).on('click', "#ClearRound2", function(){
    $('input#round2').removeAttr('checked');
    $('input[name="round2"]').prop('checked', false);
    $("#feedback2").val("");
    $('input#schedule2')[0].value = 0;
    return false;
  });

  $(document).on('click', "#ClearRound3", function(){
    $('input#round3').removeAttr('checked');
    $('input[name="round3"]').prop('checked', false);
    $("#feedback3").val("");
    $('input#schedule3')[0].value = 0;
    return false;
  });

  $(document).on('click', "#ClearRound4", function(){
    $('input#round4').removeAttr('checked');
    $('input[name="round4"]').prop('checked', false);
    $("#feedback4").val("");
    $('input#schedule4')[0].value = 0;
    return false;
  });



  $(document).ready(function () {
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    $('#schedule1').attr('max', maxDate);

    $('#schedule2').attr('max', maxDate);

    $('#schedule3').attr('max', maxDate);

    $('#schedule4').attr('max', maxDate);
  });



  $('input#pan_copy').change(function () {
    var ext = this.value.match(/\.(.+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'pdf':
            // $('#uploadButton').attr('disabled', false);
            break;
        default:
            danger_toast_msg('This is not an allowed file type.');
            this.value = '';
    }
    });

$('input#gst_certificate').change(function () {
    var ext = this.value.match(/\.(.+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'pdf':
            // $('#uploadButton').attr('disabled', false);
            break;
        default:
            danger_toast_msg('This is not an allowed file type.');
            this.value = '';
    }
    });

    $('input#company_certificate').change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'pdf':
                // $('#uploadButton').attr('disabled', false);
                break;
            default:
                danger_toast_msg('This is not an allowed file type.');
                this.value = '';
        }
        });

        $('input#agreement').change(function () {
            var ext = this.value.match(/\.(.+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'jpeg':
                case 'png':
                case 'pdf':
                    // $('#uploadButton').attr('disabled', false);
                    break;
                default:
                    danger_toast_msg('This is not an allowed file type.');
                    this.value = '';
            }
            });

    function PanURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#pan_img').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $("#click_pan_copy").click(function() {
        $("#pan_copy").click();
    });

    $("#click_gst_certificate").click(function() {
        $("#gst_certificate").click();
    });

    $("#click_company_certificate").click(function() {
        $("#company_certificate").click();
    });

    $("#click_agreement").click(function() {
        $("#agreement").click();
    });


    $("#pan_copy").change(function(){
        CheckFileType = this.files[0].type;
        
        if(CheckFileType == "image/jpeg"){
            $("#pan_img").show();
            $("#Pan_viewer").hide();
        }else if(CheckFileType == "image/png"){
            $("#pan_img").show();
            $("#Pan_viewer").hide();
        }else if(CheckFileType == "application/pdf"){
            $("#Pan_viewer").show();
            $("#pan_img").hide();
            pdffile=document.getElementById("pan_copy").files[0];
                pdffile_url=URL.createObjectURL(pdffile);
                $('#Pan_viewer').attr('src',pdffile_url);
        }

        PanURL(this);
    });


    function GstURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#gst_img').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#gst_certificate").change(function(){

        CheckFileType = this.files[0].type;
        
        if(CheckFileType == "image/jpeg"){
            $("#gst_img").show();
            $("#Gst_viewer").hide();
        }else if(CheckFileType == "image/png"){
            $("#gst_img").show();
            $("#Gst_viewer").hide();
        }else if(CheckFileType == "application/pdf"){
            $("#Gst_viewer").show();
            $("#gst_img").hide();
            pdffile=document.getElementById("gst_certificate").files[0];
                pdffile_url=URL.createObjectURL(pdffile);
                $('#Gst_viewer').attr('src',pdffile_url);
        }
        GstURL(this);
    });

    function CompanyURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#company_img').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#company_certificate").change(function(){

        CheckFileType = this.files[0].type;
        
        if(CheckFileType == "image/jpeg"){
            $("#company_img").show();
            $("#Company_viewer").hide();
        }else if(CheckFileType == "image/png"){
            $("#company_img").show();
            $("#Company_viewer").hide();
        }else if(CheckFileType == "application/pdf"){
            $("#Company_viewer").show();
            $("#company_img").hide();
            pdffile=document.getElementById("company_certificate").files[0];
                pdffile_url=URL.createObjectURL(pdffile);
                $('#Company_viewer').attr('src',pdffile_url);
        }

        CompanyURL(this);
    });


    function AgreementURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $('#agreement_img').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#agreement").change(function(){
        CheckFileType = this.files[0].type;
        
        if(CheckFileType == "image/jpeg"){
            $("#agreement_img").show();
            $("#Agreement_viewer").hide();
        }else if(CheckFileType == "image/png"){
            $("#agreement_img").show();
            $("#Agreement_viewer").hide();
        }else if(CheckFileType == "application/pdf"){
            $("#Agreement_viewer").show();
            $("#agreement_img").hide();
            pdffile=document.getElementById("agreement").files[0];
                pdffile_url=URL.createObjectURL(pdffile);
                $('#Agreement_viewer').attr('src',pdffile_url);
        }

        AgreementURL(this);
    });

    $(document).ready(function() {
        $("#SubmitJobsList").click(function(){
            // if ($(".CheckJobs").prop("checked", false)){
            //     danger_toast_msg("Please select jobs");
            //     return false;
            // }

            var CustomerId = $("#CustomerId").val();

            var candidate = [];
            $.each($("input[name='Candidates[]']:checked"), function(){
                candidate.push($(this).val());
            });
            // alert("Candidate Id: " + candidate.join(", "));

            var Jobs = [];
            $.each($("input[name='Jobs[]']:checked"), function(){
                Jobs.push($(this).val());
            });
            // alert("Jobs Id: " + Jobs.join(", "));

            $.ajax({
                type: "POST",
                url: "/AddJobsToCandidates",
                data: {candidate: candidate, Jobs: Jobs, CustomerId: CustomerId},
                dataType: "JSON",
                beforeSend: function(){
                    $("#ShortlistBtn").attr('disabled', true);
                },
                success: function (data) {
                    if(data.error){
                        danger_toast_msg(data.message);
                        // window.location.href = "/candidate_list";
                        return false;
                    }else{
                        success_toast_msg(data.message);
                        // window.location.href = "/candidate_list";
                        return false;
                    }
                },
                complete:function(data){
                    $("#ShortlistBtn").attr('disabled', false);
                }
            });
        });
    });


    $(document).on("click", "#AddMasterAdmin", function(){
        var name = $("#usernname").val();
        var password = $("#password").val();
    
        if(name == ""){
            danger_toast_msg("Username should not empty");
            $("#usernname").focus();
            return false;
        }
    
        if(password == ""){
            danger_toast_msg("Password should not empty");
            $("#password").focus();
            return false;
        }
    
    
        var AdminData = {
            name: name,
            password: password

        }
    
        $.ajax({
            type: "POST",
            url: "/post_masteradmin",
            data: AdminData,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    window.location = window.location.href;
                    return false;
                }
            }
        });
    });


    $(document).on("click", "#UpdateMasterAdmin", function(){
        var Id = $("#Id").val();
        var name = $("#master_admin_name").val();
        var password = $("#password").val();
    
    
        if(name == ""){
            danger_toast_msg("Username should not empty");
            $("#master_admin_name").focus();
            return false;
        }
    
        // if(password == ""){
        //     danger_toast_msg("Password should not empty");
        //     $("#password").focus();
        //     return false;
        // }
    
    
        var AdminData = {
            Id: Id,
            name: name,
            password: password

        }
    
        $.ajax({
            type: "POST",
            url: "/Update_master_admin",
            data: AdminData,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    $("#UpdateMasterAdmin").attr('disabled', true);
                    // location.reload(true);
                    window.location = window.location.href;
                    return false;
                }
            }
        });
    });

    
    $(document).on("click", "#AddAdmin", function(){
        var customerId = $("#customerid :selected").val();
        var name = $("#name").val();
        var password = $("#password").val();
    
        if(customerId == "Select Customer"){
            danger_toast_msg("Name should not empty");
            $("#customerid").focus();
            return false;
        }
    
        if(name == ""){
            danger_toast_msg("Username should not empty");
            $("#name").focus();
            return false;
        }
    
        if(password == ""){
            danger_toast_msg("Password should not empty");
            $("#password").focus();
            return false;
        }
    
    
        var AdminData = {
            customerId: customerId,
            name: name,
            password: password

        }
    
        $.ajax({
            type: "POST",
            url: "/post_admin",
            data: AdminData,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    window.location = window.location.href;
                    return false;
                }
            }
        });
    });

    $(document).on("click", "#UpdateAdmin", function(){
        var Id = $("#Id").val();
        var customerId = $("#customerid :selected").val();
        var name = $("#admin_name").val();
        var password = $("#password").val();
    
       
    
        if(name == ""){
            danger_toast_msg("Username should not empty");
            $("#admin_name").focus();
            return false;
        }
    
        // if(password == ""){
        //     danger_toast_msg("Password should not empty");
        //     $("#password").focus();
        //     return false;
        // }
    
    
        var AdminData = {
            Id: Id,
            customerId: customerId,
            name: name,
            password: password

        }
    
        $.ajax({
            type: "POST",
            url: "/Update_admin",
            data: AdminData,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    $("#UpdateAdmin").attr('disabled', true);
                    // location.reload(true);
                    window.location = window.location.href;
                    return false;
                }
            }
        });
    });



    function ChangeShortlistedStatus(id){
    
        $.ajax({
            type: "POST",
            url: "/ChangeShortlistedStatus/"+id,
            // data: {status: status},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    window.location = window.location.href;
                    return false;
                }else{
                    success_toast_msg(data.message);
                    window.location = window.location.href;
                    return false;
                }
            }
        });
    }

    function ChangeRejectedStatus(id){
    
        $.ajax({
            type: "POST",
            url: "/ChangeRejectedStatus/"+id,
            // data: {status: status},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    window.location = window.location.href;
                    return false;
                }else{
                    success_toast_msg(data.message);
                    window.location = window.location.href;
                    return false;
                }
            }
        });
    }



    function ChangeMasterAdminStatus(id){

        $.ajax({
            type: "POST",
            url: "/ChangeMasterAdminStatus/"+id,
            // data: {status: status},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    return false;
                }
            }
        });
    }


    function ChangeAdminStatus(id){

        $.ajax({
            type: "POST",
            url: "/ChangeAdminStatus/"+id,
            // data: {status: status},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    return false;
                }
            }
        });
    }



    $(document).ready(function() {
        $("#CheckAll").change(function(){
          if(this.checked){
            $(".CheckCandidate").each(function(){
              this.checked=true;
            })              
          }else{
            $(".CheckCandidate").each(function(){
              this.checked=false;
            })              
          }
        });
      
        $(".CheckCandidate").click(function () {
          if ($(this).is(":checked")){
            var isAllChecked = 0;
            $(".CheckCandidate").each(function(){
              if(!this.checked)
                 isAllChecked = 1;
            })              
            if(isAllChecked == 0){ $("#CheckAll").prop("checked", true); }     
          }else {
            $("#CheckAll").prop("checked", false);
          }
        });
      });



      $(document).ready(function() {
        $("#CheckAllJobs").change(function(){
          if(this.checked){
            $(".CheckJobs").each(function(){
              this.checked=true;
            })              
          }else{
            $(".CheckJobs").each(function(){
              this.checked=false;
            })              
          }
        });
      
        $(".CheckJobs").click(function () {
          if ($(this).is(":checked")){
            var isAllChecked = 0;
            $(".CheckJobs").each(function(){
              if(!this.checked)
                 isAllChecked = 1;
            })              
            if(isAllChecked == 0){ $("#CheckAllJobs").prop("checked", true); }     
          }else {
            $("#CheckAllJobs").prop("checked", false);
          }
        });
      });


    //   function ChooseCandidate(){
    //     // if ($(".CheckCandidate").prop("checked", false)){
    //     //     danger_toast_msg("Please select candidate");
    //     //     return false;
    //     // }

    //     // $('#JobDetails').modal('show'); 

    //   }


    function CheckCustomerData(){
        var Id = $("#customer_name").val();
        var JobsId = $("#job_name").val();

        $.ajax({
            type: "POST",
            url: "/CheckCustomerData",
            data: {Id: Id, JobsId: JobsId},
            dataType: "html",
            success: function (data) {
                $("#CandidateDetails").html("");
                $("#CandidateDetails").append(data);
                return false;
            }
        });
    }

    function CheckJobsData(){
        var Id = $("#customer_name").val();
        var JobsId = $("#job_name").val();

        $.ajax({
            type: "POST",
            url: "/CheckJobsData",
            data: {Id: Id, JobsId: JobsId},
            dataType: "html",
            success: function (data) {
                $("#CandidateDetails").html("");
                $("#CandidateDetails").append(data);
                return false;
            }
        });
    }

    function CheckCustomerJobsData(){
        var Id = $("#customer_name").val();
        var JobsId = $("#job_name").val();

        $.ajax({
            type: "POST",
            url: "/CheckCustomerJobsData",
            data: {Id: Id, JobsId: JobsId},
            dataType: "html",
            success: function (data) {
                $("#JobsData").html("");
                $("#JobsData").append(data);
                return false;
            }
        });
    }

    
    function CheckJobsListData(){
        var Id = $("#customer_name").val();
        var JobsId = $("#job_name").val();

        $.ajax({
            type: "POST",
            url: "/CheckJobsListData",
            data: {Id: Id, JobsId: JobsId},
            dataType: "html",
            success: function (data) {
                $("#JobsData").html("");
                $("#JobsData").append(data);
                return false;
            }
        });
    }
    


    function FilterCustomerJobsData(){
        var Id = $("#customer_name").val();
        var JobsId = $("#job_name").val();

        $.ajax({
            type: "POST",
            url: "/FilterCustomerJobsData",
            data: {Id: Id, JobsId: JobsId},
            dataType: "html",
            success: function (data) {
                $("#CandidateDetails").html("");
                $("#CandidateDetails").append(data);
                return false;
            }
        });
    }

    function FilterJobsListData(){
        var Id = $("#customer_name").val();
        var JobsId = $("#job_name").val();

        $.ajax({
            type: "POST",
            url: "/FilterJobsListData",
            data: {Id: Id, JobsId: JobsId},
            dataType: "html",
            success: function (data) {
                $("#CandidateDetails").html("");
                $("#CandidateDetails").append(data);
                return false;
            }
        });
        
    }


    $(document).on('click', '#UpdatePassword', function(){

        // var old_password = $("#old_password").val();
        var new_password = $("#new_password").val();
        var confirm_password = $("#confirm_password").val();

        // if(old_password == ""){
        //     danger_toast_msg("Please type old password");
        //     $("#old_password").focus();
        //     return false;
        // }

        if(new_password == ""){
            danger_toast_msg("Please type new password");
            $("#new_password").focus();
            return false;
        }

        if(confirm_password == ""){
            danger_toast_msg("Please type confirm password");
            $("#confirm_password").focus();
            return false;
        }
        
        var Passwords = {
            // old_password: old_password, 
            new_password: new_password,
            confirm_password: confirm_password    
        }

        $.ajax({
            type: "POST",
            url: "/UpdatePassword",
            data: Passwords,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    window.location = window.location.href;
                    return false;
                }
            }
        });
    });



    $(document).on('click', '#UpdateMasterAdminPassword', function(){

        // var old_password = $("#old_password").val();
        var new_password = $("#new_password").val();
        var confirm_password = $("#confirm_password").val();

        // if(old_password == ""){
        //     danger_toast_msg("Please type old password");
        //     $("#old_password").focus();
        //     return false;
        // }

        if(new_password == ""){
            danger_toast_msg("Please type new password");
            $("#new_password").focus();
            return false;
        }

        if(confirm_password == ""){
            danger_toast_msg("Please type confirm password");
            $("#confirm_password").focus();
            return false;
        }
        
        var Passwords = {
            // old_password: old_password, 
            new_password: new_password,
            confirm_password: confirm_password    
        }

        $.ajax({
            type: "POST",
            url: "/MasterAdminUpdatePassword",
            data: Passwords,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    window.location = window.location.href;
                    return false;
                }
            }
        });
    });




    function CheckAdminType(){
        var GetAdminType = $("#select_type").val();
        
        if(GetAdminType == 1){
            $(".UserName").attr("id", "username");
            $(".Password").attr("id", "password");
            $(".GetLoginId").attr("id","MasterAdminLoginBtn");
        }else if(GetAdminType == 2){
            $(".UserName").attr("id", "username");
            $(".Password").attr("id", "password");
            $(".GetLoginId").attr("id","LoginBtn");
        }else if(GetAdminType == 3){
            $(".UserName").attr("id", "admin_username");
            $(".Password").attr("id", "admin_password");
            $(".GetLoginId").attr("id","AdminLoginBtn");
        }

    }




    
// Or with jQuery
// $(document).on("click", "#CloseCandidateList", function(){
//     $("#JobDetails").modal("hide");
// });